<?php

class AdminRegistration extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	var $table = 'eventregistration,events';
  	var $select_fields = 'eventregistration.registrationNumber,events.eventCategory,events.eventName,eventregistration.commonRegister,eventregistration.email,eventregistration.status,eventregistration.registerDate';
   	var $where_condition = "events.id=eventregistration.eventId";
    var $column_order = array('eventregistration.commonRegister','eventregistration.commonRegister','events.eventName','events.eventCategory','eventregistration.email','eventregistration.registerDate','eventregistration.status',null); //set column field database for datatable orderable
    var $column_search = array('eventregistration.commonRegister','eventregistration.commonRegister','events.eventName','events.eventCategory','eventregistration.email','eventregistration.registerDate','eventregistration.status',null); //set column field database for datatable searchable just firstname , lastname , address are searchable
	var $order = array('eventregistration.registerDate' => 'DESC'); // default order 
	var $group = 'eventregistration.commonRegister';
	
	private function get_datatables_query()
		{
			$this->db->from($this->table);
			$this->db->select($this->select_fields);
			$this->db->where($this->where_condition);
			$i = 0;   
			
			foreach ($this->column_search as $item) // loop column 
			{
				
				if($_POST['search']['value']) // if datatable send POST for search
				{                 
					if($i===0) // first loop
					{
						$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
						$this->db->like($item, $_POST['search']['value']);
					}
					else
					{
						$this->db->or_like($item, $_POST['search']['value']);
					}
	 
					if(count($this->column_search) - 1 == $i) //last loop
						$this->db->group_end(); //close bracket
				}
				$i++;
			}
			 
			if(isset($_POST['order'])) // here order processing
			{
				$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			} 
			if(isset($_POST['eventName']) and $_POST['eventName']!='')
			{
				$this->db->like('events.eventName',$_POST['eventName']);
			}
			if(isset($_POST['eventCategory']) and $_POST['eventCategory']!='')
			{
				$this->db->like('events.eventCategory',$_POST['eventCategory']);
				
			}
			if(isset($_POST['commonRegister']) and $_POST['commonRegister']!='')
			{
				$this->db->like('eventregistration.commonRegister',$_POST['commonRegister']);
			}
			if(isset($_POST['email']) and $_POST['email']!='')
			{
				$this->db->like('eventregistration.email',$_POST['email']);
			}
			
			if((!empty($_POST['order_date_from']) and !empty($_POST['order_date_to'])))
			{
				$from_date = explode('/', $_POST['order_date_from']);
				$from_date_ymd = strtotime($from_date[2].'-'.$from_date[1].'-'.$from_date[0]);
				$to_date = explode('/', $_POST['order_date_to']);;
				$to_date_ymd = strtotime($to_date[2].'-'.$to_date[1].'-'.$to_date[0]);
				if($from_date_ymd<=$to_date_ymd)
				{
					$this->db->where('eventregistration.registerDate >=', $from_date_ymd);
					$this->db->where('eventregistration.registerDate <=', $to_date_ymd);
				}	
			}
			if(isset($_POST['status']) and $_POST['status']!='')
			{
				$this->db->where('eventregistration.status="'.$_POST['status'].'"');
			}
			else if(isset($this->order))
			{
				$order = $this->order;
				$this->db->order_by(key($order), $order[key($order)]);
			}
			$this->db->group_by('eventregistration.commonRegister'); 
		}
	 public function get_datatables()
		{
			$this->get_datatables_query();
			if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
			$query = $this->db->get();
			return $query->result();
		}
    public function count_filtered()
    {
        $this->get_datatables_query();
		$query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
		$this->db->from($this->table);
	    return $this->db->count_all_results();
    }	
	
	public function viewRegistration($eventId)
	{
		$query = $this->db->select('eventregistration.*,events.eventName,events.eventCategory,events.eventStartTime,events.eventEndTime,events.eventDate,events.direction,events.location')
		->from('events,eventregistration')
		->where('eventregistration.commonRegister=',$eventId)
		->where('eventregistration.eventId=events.id')
		->get();		
		return $query->result();	
	}
	
	public function deleteEvent($eventId)
	{
		$where = array("commonRegister"=>$eventId);
		$this->db->where($where);
		$this->db->delete('eventregistration');
		return $this->db->affected_rows();
	}
	
	public function updateStatus($id = array(), $status)
	{
		$affected_rows = '';
		foreach($id as $id)
		{
			$wrong = false;
			$where = array("commonRegister"=>$id);
			$this->db->where($where);
			$data = array("status"=>$status);
			$this->db->update('eventregistration',$data);
			$affected_rows = $this->db->affected_rows();	
			if($affected_rows<1)
			{
				$wrong = true;
			}
		}
		if($wrong == true)
		{
			return array("status"=>"NOT OK","message"=>"Selected Event Registration Status Not Updated Sucessfully");
		}
		else
		{
			return array("status"=>"OK","message"=>"Selected Event Registration Status Updated Successfully");
		}
	}
}

?>
