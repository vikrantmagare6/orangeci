<?php

class CategoryPage extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function categoryDetail($category_url='')
	{
		$this->db->select("id,name,url,image,description,parent_category");
        $this->db->from("category");
		$this->db->where("status='Active'");
		$this->db->where("id!=0");
		if($category_url!='')
		{
			$this->db->where("url='".$category_url."'");
		}
		return $this->db->get()->result();
	}	
	
	public function parentCategoryDetail($sub_category)
	{
		$this->db->select("id,name,parent_category");
        $this->db->from("category");
		$this->db->where("status='Active'");
		$this->db->where("id=".$sub_category);
		return $this->db->get()->row();
	}
	
	public function subCategoryDetail($parent)
	{
		$this->db->select("id,name");
        $this->db->from("category");
		$this->db->where("status='Active'");
		$this->db->where("parent_category=".$parent);
		return $this->db->get()->result();
	}
	
	public function products($category)
	{
		$this->db->select("id,name,description,image");
        $this->db->from("products");
		$this->db->where("status='Active'");
		$this->db->where("category=".$category);
		return $this->db->get()->result();		
	}
	
}

?>
