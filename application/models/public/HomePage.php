<?php

class HomePage extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	public function getAllBanner()
	{
		$where = array('status='=>'Active');
		$query = $this->db->select('id,image,caption,url,description')->from('home_banner')->where($where)->order_by('id','DESC')->get();
		return json_encode($query->result());
	}
	
	public function getAllBrands()
	{
		$where = array('status='=>'Active','display_home'=>'Yes');
		$query = $this->db->select('name,image')->from('brands')->where($where)->order_by('id','ASC')->get();
		return json_encode($query->result());	
	}
	
	public function getAllParentCategory()
	{
		$where = array('status='=>'Active','id!='=>'0','parent_category='=>'0');
		$query = $this->db->select('id,name,url')->from('category')->where($where)->order_by('name','ASC')->get();
		return json_encode($query->result());		
	}
	
	public function getAllHomeProduct()
	{
		$query = $this->db->select('p.id,p.name,p.image,p.description,p.status,c.id as category_id,c.name as category,p.display_home')
		->from('products as p,category as c')
		->where("p.display_home='Yes'")
		->where('c.id=p.category')
		->get();		
		return json_encode($query->result());
	}
}

?>
