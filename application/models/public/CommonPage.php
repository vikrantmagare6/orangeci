<?php
class CommonPage extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->allHeaderCategoryWithHTMLFunction = '';
		$this->category_tree='';
	}

	public function parentCategory()
	{
		$this->db->select("name,url");
		$this->db->from("category");
		$this->db->where("parent_category='0'");
		$this->db->where("status='Active'");
		$this->db->where("id!='0'");
		$this->db->order_by('name','ASC');
       	return json_encode($this->db->get()->result());	
	}
	
	public function parent_child_categoryHTML($category = array())
	{
			$record = $this->get_parent_child_category();
			/*echo '<pre>';
			print_r($record);
			exit;*/
			return json_encode($this->get_category($record,$category));
		}
	
	public function get_parent_child_category()
		{   
				$where = array('status!='=>'Delete',"id!="=>"0","parent_category"=>"0");
				$query = $this->db->select('id,name,parent_category,url')->from('category')->where($where)->order_by('name','ASC')->get();
				//$query = $this->db->query("select * from category where parent_category = '0' and id!='0'");
				$result = $query->result();
				$roles = array();
				foreach($result as $key=>$value)
					{   
						$role = array();
						$role['name'] = $result[$key]->name;
						$role['id'] = $result[$key]->id;
						$role['url'] = $result[$key]->url;
						$children = $this->build_child($result[$key]->id);              
								if( !empty($children) ) {
								$role['children'] = $children;
								}
						$roles[$key] = $role;			
			} 
			return $roles;      
}

		public function build_child($parent)
	{
			$where = array('status!='=>'Delete',"id!="=>"0","parent_category"=>$parent);
			$query = $this->db->select('id,name,parent_category,url')->from('category')->where($where)->order_by('name','ASC')->get();
			$result = $query->result();
			$roles = array();       
			foreach($result as $key => $val) {
				if($result[$key]->parent_category == $parent) {
					$role = array();
					$role['name'] = $result[$key]->name;
					$role['id'] = $result[$key]->id;
					$role['url'] = $result[$key]->url;
					$children = $this->build_child($result[$key]->id);
					if( !empty($children) ) {                   
						$role['children'] = $children;
					}
					$roles[] = $role;
				}
			}
		return $roles;
}

public function get_category($array) {
	/*echo '<pre>';
	print_r($array);
	exit;*/
   	$this->category_tree .='<ul class="dropdown-menu">';
	foreach($array as $category) {
		if(isset($category['children'])) {

			$this->category_tree.='<li class="dropdown dropdown-submenu">';
			$this->category_tree .= '<a title="'.$category['name'].'" href="'.base_url('category/'.$category['url']).'" class="dropdown-toggle" data-toggle="dropdown">'.$category['name'].'</a>';
            $this->get_category($category['children']);
        }
		else
		{
			$this->category_tree .= '<li>';	
			$this->category_tree .= '<a title="'.$category['name'].'" href="'.base_url('category/'.$category['url']).'" >'.$category['name'].'</a>';
		}
        $this->category_tree .='</li>';
    }
    $this->category_tree .='</ul>';
	return $this->category_tree;
}
public function getContactDetail()
{
		$this->db->select("contact_address,contact_email_address,contact_phone");
		$this->db->from("settings");
		return $this->db->get()->row();	
}
}
?>