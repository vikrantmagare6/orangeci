<?php

class EventPage extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	public function get_all_count()
    {
        
        $this->db->from('events')->where('isFarmerEvents','No');
        return $this->db->count_all_results();
    }


    public function get_all_content($start,$limit,$eventDate='')
    {
        switch ($eventDate) {
            case "16":
                $whereCondition =  "eventDate='16th December'";
                break;
            case "17":
                $whereCondition =  "eventDate='17th December'";
                break;
            case "18":
                $whereCondition =  "eventDate='18th December'";
                break;
            default:
                $whereCondition =  "eventDate!=''";
        }
        $query = $this->db->select('*')
		->from('events')
        ->where('status','Active')
        ->where($whereCondition)
        ->where('isFarmerEvents!="Yes"')
        ->limit($limit,$start)
        ->order_by("updatedDate", "DESC")
	->get();		
	return $query->result();	
    }

    public function get_all_count_farmers()
    {
        
        $this->db->from('events')->where('isFarmerEvents','Yes');
        return $this->db->count_all_results();
    }


    public function get_all_content_farmers($start,$limit,$eventDate='')
    {
        switch ($eventDate) {
            case "16":
                $whereCondition =  "eventDate='16th December'";
                break;
            case "17":
                $whereCondition =  "eventDate='17th December'";
                break;
            case "18":
                $whereCondition =  "eventDate='18th December'";
                break;
            default:
                $whereCondition =  "eventDate!=''";
        }
        $query = $this->db->select('*')
		->from('events')
        ->where('status','Active')
        ->where($whereCondition)
        ->where('isFarmerEvents','Yes')
        ->limit($limit,$start)
		->get();		
		return $query->result();
    }

public function registerCount($eventId)
{
    $this->db->select('*');
    $this->db->where('eventId',$eventId);
    $query = $this->db->get('eventregistration');
    return $query->num_rows();
}
public function capacityEvent($eventId)
{
    $this->db->select('eventCapacity');
    $this->db->where('id',$eventId);
    $query = $this->db->get('events');
    return $query->row();
}

public function getRegistrationDetail($commonRegister)
{
    $this->db->select('eventregistration.registrationNumber,events.id,events.direction,events.eventCategory,events.eventName,events.eventDate,events.eventStartTime,events.eventEndTime,events.location,eventregistration.fullName,eventregistration.mobileNumber,eventregistration.email');
    $this->db->where('eventregistration.commonRegister',$commonRegister);
    $this->db->where('events.id = eventregistration.eventId');
    $query = $this->db->get('events,eventregistration');
    return $query->result();
}


}

?>
