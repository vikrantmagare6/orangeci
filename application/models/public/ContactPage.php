<?php

class ContactPage extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	
	public function contactPageDetails()
	{
		$this->db->cache_on();
		$where = array('id='=>'1');
		$query = $this->db->select('contact_address,contact_email_address,contact_phone,contact_longitude,contact_latitude')->from('settings')->where($where)->get();
		$returnData = $query->row();	
		$this->db->cache_off();
		return json_encode($returnData);
	}
	public function submitContact($data)
	{
		$this->db->insert('contact_request',$data);
		return $this->db->affected_rows();
	}
}

?>
