<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
Custom Constant Define
*/
define('SITE_DISPNAME','World Orange Festival');
define('WELCOME_MESSAGE','Welcome To '.SITE_DISPNAME);
define('ADMIN_DIR','my_admin');
define('PUBLIC_DIR','public');
define('SITE_URL','https://www.worldorangefestival.com/');
define('ADMIN_URL',SITE_URL.ADMIN_DIR.'/');
define('URL_EXTENSION','.html');


/*Admin Common Section Constant Message Start*/
define('ADMIN_VERSION','version1/'); 
define('ADMIN_HEADER_VERSION','version1');
define('ADMIN_CONTENT_VERSION','version1');
define('ADMIN_SIDEBAR_VERSION','version');
define('ADMIN_LEFT_SIDEBAR_VERSION','version1');
define('ADMIN_RIGHT_SIDEBAR_VERSION','version1');
define('ADMIN_FOOTER_VERSION','version1');
define('ADMIN_404','version1');
/*Admin Common Section Constant Message End*/

/*Admin Modules Section Constant Message Start*/
define('ADMIN_PROFILE_VERSION','version1');
define('ADMIN_DASHBOARD_VERSION','version1');
define('ADMIN_CUSTOMER_DASHBOARD_VERSION','version1');
define('ADMIN_PAGE_DASHBOARD_VERSION','version1');
define('ADMIN_PRODUCT_DASHBOARD_VERSION','version1');
define('ADMIN_SETTINGS_VERSION','version1');
/*Admin Modules Section Constant Message End*/

/*Admin Section Page Title, Descrption, Author, Keywords Section Start*/
define('ADMIN_PAGE_TITLE',SITE_DISPNAME.' Admin');
define('ADMIN_META_KEYWORDS',SITE_DISPNAME);
define('ADMIN_META_DESCRIPTION',SITE_DISPNAME);
define('ADMIN_META_AUTHOR',SITE_DISPNAME);
/*Admin Section Page Title, Descrption, Author, Keywords Section End*/



/*Front Module Constant Message Section Start*/
define('PUBLIC_VERSION','version1');
define('PUBLIC_PAGE_TITLE',WELCOME_MESSAGE);
define('PUBLIC_META_KEYWORDS',SITE_DISPNAME);
define('PUBLIC_META_DESCRIPTION',SITE_DISPNAME);
define('PUBLIC_META_AUTHOR',SITE_DISPNAME);

/*Category Page */
define('CATEGORY_VERSION','version1');

/*Front Module Constant Message Section End*/


/*Other Constant Message Section Start*/
define('DATE_FORMAT','d M Y h:i:sa');

/*Front Success And Failure Error Message Start*/
define('FAIL','has-error');
define('SUCCESS','has-success');
/*Front Success And Failure Error Message End*/

/*Admin Success And Failure Error Message Start*/
define('A_FAIL','alert alert-danger');
define('A_SUCCESS','alert alert-success');
define('A_JQUERY_VALIDATION_SUCCESS','border-green-meadow');
define('A_JQUERY_VALIDATION_ERROR_CLASS_FULL','border-red-thunderbird font-red-thunderbird');
define('A_JQUERY_VALIDATION_ERROR_CLASS1','border-red-thunderbird');
define('A_JQUERY_VALIDATION_ERROR_CLASS2','font-red-thunderbird');
/*Admin Success And Failure Error Message End*/



/*Other Constant Message Section End*/
