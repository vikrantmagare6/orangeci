<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {
	
	public function __Construct()
	{
	   	 parent::__Construct();
	   	 $this->load->model(PUBLIC_DIR.'/homePage','home');
		 $this->load->model(PUBLIC_DIR.'/commonPage','common');
	}	

	public function index()
	{
		$get_param = $this->uri->segment(2);
		$dec_param = base64_decode($get_param);
		$dec_param_arr = explode(' & ',$dec_param);
		$redirectURL =  base_url(uri_string());
		/*echo '<pre>';
		print_r($dec_param_arr);
		exit;*/

		$event_name = $dec_param_arr[0];
		$event_date = $dec_param_arr[1];
		$event_id = $dec_param_arr[2];
		
		$header = array();
		if($event_name!='' and $event_date!='')
		{
			$header['module_name'] = $event_name.' Event Registration';
			$content = array();
			$sidebar = array();
			$footer = array();
			$footer['module_name'] = 'Event Registration';
			$content['eventName'] = $event_name;
			$content['eventId'] = $event_id;
			$content['redirectURL'] = $redirectURL;
			$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/header',$header);		
			$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/sidebar',$sidebar);		
			$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/registration',$content);		
			$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/footer',$footer);
		}
		else{
			redirect(base_url('event_schedule'));
		}
	}

}
