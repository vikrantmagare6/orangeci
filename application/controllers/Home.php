<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __Construct()
	{
	   	 parent::__Construct();
	   	 $this->load->model(PUBLIC_DIR.'/homePage','home');
		 $this->load->model(PUBLIC_DIR.'/commonPage','common');
		 $this->load->model(PUBLIC_DIR.'/eventPage','events');
	}	
	
	public function index()
	{
		$header = array();
		$header['module_name'] = 'Home';
		$content = array();
		$sidebar = array();
		$footer = array();
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/header',$header);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/sidebar',$sidebar);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/home',$content);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/footer',$footer);
	}
	public function get_events()
	{
		$allEvents = $this->events->get_all_content('0','100');
		echo '<pre>';
		print_r($allEvents);
		exit;
	}
	public function email_test()
	{

		$registrationDetail = $this->events->getRegistrationDetail('e586f12a2a5dbb976882f84815349e68');
		/*echo '<pre>';
		print_r($registrationDetail);
		exit;*/
		if(!empty($registrationDetail))
		{
				$this->load->library('email');
				
				$config = array (
						'mailtype' => 'html',
						'charset'  => 'utf-8',
						'priority' => '1'
						);
				$this->email->initialize($config);

				$this->email->from('no-reply@worldorangefestival.com', 'Orange Festival');
				$this->email->to('aliasgar.vanak@gmail.com');
				
				$this->email->subject('Registration Sucessfull');
				$email_data = array("userData"=>$registrationDetail);
				
				$email_content = $this->load->view('thank-you',$email_data,true);	
				
				$this->email->message($email_content);
				
				//Send mail 
				if($this->email->send()) 
				{
					echo 'email send';
					exit;							 
				}
				else
				{
					echo 'email not send';
					exit;
				}
		}
		else{
				exit('coming here...');
		}
	}

	public function db_backup()
	{
		// Load the DB utility class
		$this->load->dbutil();

		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup();

		// Load the file helper and write the file to your server
		$this->load->helper('file');
		write_file('/path/to/mybackup.gz', $backup);

		// Load the download helper and send the file to your desktop
		$this->load->helper('download');
		force_download('mybackup.gz', $backup);
		
	}

}
