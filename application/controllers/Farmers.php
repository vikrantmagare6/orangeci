<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Farmers extends CI_Controller {
	
	public function __Construct()
	{
	   	 parent::__Construct();
		 $this->load->model(PUBLIC_DIR.'/eventPage','events');
	}	
	
	public function index()
	{
		$header = array();
		$header['module_name'] = 'Farmers First';
		$content = array();
		$footer = array();
		$content['event_limit'] = '9';
		$content['eventDate'] = $eventDate = (isset($_GET['eventDate']) and $_GET['eventDate']!='')?$_GET['eventDate']:'';
		$total_data = $this->events->get_all_content_farmers('0',$content['event_limit'],$eventDate);

		/*echo '<pre>';
		print_r($total_data);
		exit;*/
		$content['events'] = $total_data;
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/header',$header);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/farmers_first',$content);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/footer',$footer);
	}

	public function load_more()
    {
		if(isset($_POST["limit"], $_POST["start"]))
		{
			$all_content = $this->events->get_all_content_farmers($_POST['start'],$_POST['limit'],$_POST['eventDate']);
			if(isset($all_content) && is_array($all_content) && count($all_content)) : 
				foreach ($all_content as $key => $content) :
					$event_registration_count = $this->events->registerCount($content->id);
					$registration_url = base64_encode($content->eventName.' & '.$content->eventDate);
					if($content->id>0)
					//if($content->eventCapacity>0 and $event_registration_count>=$content->eventCapacity)
					{
						$href = "javascript:void(0);";
						$label = 'Registration Closed';
						/*$registrationLink = '<a href="javascript:void(0);" >Registration Closed</a>';
						echo $registrationLink;*/
					}
					else{
						$registration_url = base64_encode($content->eventName.' & '.$content->eventDate.' & '.$content->id);
						$href = base_url('registration/'.$registration_url);
						/*$registrationLink = '<a href="'.base_url('registration/'.$registration_url).'>register now</a>';
						echo ' <a href="javascript:void(0);" >Registration Closed</a>';*/
						$label = 'Register Now';
					}

					echo '<div class="item col-md-4 col-sm-6">
					<div class="item-inner">
						<div class="liv">
							<span>'.$content->eventName.'</span>
							<img src="'.base_url('uploads/event/farmers.png').'">
						</div>
						<ul>
							<li>
								<i class="fa fa-calendar" aria-hidden="true"></i> '.$content->eventDate.' 2017
							</li>
							<li>
								<i class="fa fa-clock-o" aria-hidden="true"></i> '.$content->eventStartTime.' - '.$content->eventEndTime.'
							</li>
							<li>
								<i class="fa fa-map-marker" aria-hidden="true"></i> '.$content->location.'
							</li>
							<li>
								<i class="fa fa-map-o" aria-hidden="true"></i> <a class="getdir" href="'.$content->direction.'" target="_blank">Get Direction</a>
							</li>
						</ul>
						<a href="'.$href.'" class="regis">'.$label.'</a>
					</div>
				</div>';              
				endforeach;                                
			endif; 
			// echo '<pre>'; print_r($this->data['labels_message']); exit;
		}
	}
}
