<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Registered extends CI_Controller {

	public function __Construct()
	{
	   	 parent::__Construct();
	   	 $this->load->model(ADMIN_DIR.'/adminLogin','login');
		 $this->login->check_isvalidated();
		 $this->adminProfileInfo = $this->login->adminProfileInfo();
		 $this->load->library('form_validation');
 	     $this->load->model(ADMIN_DIR.'/AdminRegistration','registration');
		
		
	}
	
	public function lists()
	{
		$data = array();
		$data['module_heading'] = 'Registered Events List';
		$data['adminProfileInfo'] = $this->adminProfileInfo;
		$this->load->view(ADMIN_DIR.'/'.ADMIN_HEADER_VERSION.'/common/header',$data);		
		$this->load->view(ADMIN_DIR.'/'.ADMIN_LEFT_SIDEBAR_VERSION.'/common/sidebar',$data);		
		$this->load->view(ADMIN_DIR.'/'.ADMIN_PAGE_DASHBOARD_VERSION.'/registration_list',$data);		
		$this->load->view(ADMIN_DIR.'/'.ADMIN_FOOTER_VERSION.'/common/footer',$data);
	}
	
	public function get_all_list()
	{
			 if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
				 
					$response = $this->registration->updateStatus($_POST['id'],$_REQUEST['customActionName']);
				 	$status = $response['status'];
					$message = $response['message'];
				
		   }
		   $list = $this->registration->get_datatables();
       	   $data = array();
           $no = $_POST['start'];
		   $data = array();
        	$no = $_POST['start'];
		foreach ($list as $reg) {
			$no++;
            $possible_status_changes = '';
			$row = array();
			$row[] = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$reg->commonRegister.'"/><span></span></label>';
			$row[] = $reg->eventName;
			$row[] = $reg->eventCategory;
			$row[] = $reg->email; 
			$row[] = $reg->registrationNumber;
			$row[] = date(DATE_FORMAT,$reg->registerDate);
			if($reg->status=='Success')
			{
				$row[] = '<button class="btn btn-success">'.$reg->status.'</button>';
			}
			else if($reg->status=='Pending')
			{
				$row[] = '<button class="btn btn-info">'.$reg->status.'</button>';
			}
			else
			{
				$row[] = '<button class="btn btn-danger">'.$reg->status.'</button>';
			}
			//add html for action
			
			$row[] = '<div class="btn-group">
                                                                                    <a data-toggle="dropdown" href="javascript:;" class="btn purple" aria-expanded="true">
                                                                                        <i class="fa fa-user"></i> Settings
                                                                                        <i class="fa fa-angle-down"></i>
                                                                                    </a>
                                                                                    <ul class="dropdown-menu">
                                                                                        <li>
                                                                                            <a href="'.base_url(ADMIN_DIR.'/registered/view/'.$reg->commonRegister).'"><i class="fa fa-eye"></i> View</a>
                                                                                        </li>
																					    <li>
                                                                                            <a  href="'.base_url(ADMIN_DIR.'/registered/delete/'.$reg->commonRegister).'"><i class="fa fa-trash"></i> Delete</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>';
            $data[] = $row;
        }
 
 		$output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->registration->count_all(),
                        "recordsFiltered" => $this->registration->count_filtered(),
                        "data" => $data,
						
                );
        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
				 
				$output["customActionStatus"] = $status; // OK for success and NOt OK for fail. pass custom message(useful for getting status of group actions)
				$output["customActionMessage"] = $message; // pass custom message(useful for getting status of group actions)
		   }
		//output to json format
		  echo json_encode($output);
	}
	
	
	public function exportRecord()
	{
		/*echo '<pre>';
		print_r($_POST);
		exit();*/
		$eventCategory = $this->input->post('eventCategory');
		if($this->input->post('eventCategory') =='' or  $this->input->post('eventCategory') =='all')
		{
			$where = 'events.id!=""';
		}
		else
		{
		     $where = 'events.eventCategory="'.$this->input->post('eventCategory').'"';
		}
		header('Content-Type: text/csv; charset=utf-8');  
		header('Content-Disposition: attachment; filename=data.csv');  
		$output = fopen("php://output", "w");  
		$query = $this->db->select('eventregistration.registrationNumber,events.eventDate,events.eventStartTime,events.eventEndTime,eventregistration.id,eventregistration.commonRegister,events.eventName,events.eventCategory,eventregistration.fullName,eventregistration.mobileNumber,eventregistration.email,eventregistration.registerDate')
		->from('events,eventregistration')
		->where('eventregistration.eventId=events.id')
		->where($where)
		->get();
		$result = $query->result_array();
		$delimiter = ",";
		fputcsv($output, array('ID', 'Common Register ID', 'Event Name', 'Event Date','Event Start Time','Event End Time' ,'Event Category','Full Name','Mobile Number','Registration Number','Email Address','Registration Date'));
		foreach($result as $row)
		{
			/*fputcsv($output, $res);*/
			$lineData = array($row['id'], $row['commonRegister'], $row['eventName'],$row['eventDate'],$row['eventStartTime'],$row['eventEndTime'], $row['eventCategory'], $row['fullName'], $row['mobileNumber'],$row['registrationNumber'],$row['email'],date(DATE_FORMAT,$row['registerDate']));
			fputcsv($output, $lineData, $delimiter); 
		}
		fclose($output); 
	 //redirect(ADMIN_DIR.'/registered/lists');

	}
	
	
	public function delete()
	{
		$array = $this->uri->uri_to_assoc();
		$reg_id = $array['delete'];
		$response = $this->registration->deleteEvent($reg_id);	
		if($response>0)
			{
				$this->session->set_flashdata('message_notification','Event Registration Deleted Successfully');
				$this->session->set_flashdata('class',A_SUCCESS);
				redirect(ADMIN_DIR.'/registered/lists');
			}
		else
			{
				$this->session->set_flashdata('message_notification','Event Registration Not Deleted Successfully');
				$this->session->set_flashdata('class',A_FAIL);
				redirect(ADMIN_DIR.'/registered/lists');
			}
	}
	
	public function view()
	{
		$data['module_heading'] = 'View Registration';
		$data['adminProfileInfo'] = $this->adminProfileInfo;
		$reg_id = $this->uri->segment('4');
		$data['registrationInfo'] = $this->registration->viewRegistration($reg_id);
		/*echo '<pre>';
		print_r($data['registrationInfo']);
		exit;*/
		if(!empty($data['registrationInfo']))
		{
			$this->load->view(ADMIN_DIR.'/'.ADMIN_HEADER_VERSION.'/common/header',$data);		
			$this->load->view(ADMIN_DIR.'/'.ADMIN_LEFT_SIDEBAR_VERSION.'/common/sidebar',$data);		
			$this->load->view(ADMIN_DIR.'/'.ADMIN_PAGE_DASHBOARD_VERSION.'/registration_view',$data);		
			$this->load->view(ADMIN_DIR.'/'.ADMIN_FOOTER_VERSION.'/common/footer',$data);
		}
		else
		{
			$this->lists();
		}
	}
}
?>