<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	
	public function __Construct()
	{
		parent::__Construct();
		$this->load->model(ADMIN_DIR.'/adminLogin','login');
		$this->login->check_isvalidated();
		$this->adminProfileInfo = $this->login->adminProfileInfo();
		$this->load->model(ADMIN_DIR.'/AdminDashboard','dashboard');
		$this->load->model(ADMIN_DIR.'/AdminCommon','common_model');
	}	
	public function index()
	{
		redirect(base_url(ADMIN_DIR.'/registered/lists'));
		$data = array();
		$data['module_heading'] = 'Dashboard';
		$data['adminProfileInfo'] = $this->adminProfileInfo;
		/*echo '<pre>';
		print_r($data['recentProducts']);
		exit;*/
		$this->load->view(ADMIN_DIR.'/'.ADMIN_HEADER_VERSION.'/common/header',$data);		
		$this->load->view(ADMIN_DIR.'/'.ADMIN_LEFT_SIDEBAR_VERSION.'/common/sidebar',$data);		
		$this->load->view(ADMIN_DIR.'/'.ADMIN_DASHBOARD_VERSION.'/dashboard',$data);		
		$this->load->view(ADMIN_DIR.'/'.ADMIN_FOOTER_VERSION.'/common/footer',$data);	
	}
	
}

?>
