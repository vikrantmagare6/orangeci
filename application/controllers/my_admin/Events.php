<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events extends CI_Controller {

	
	public function __Construct()
	{
	   	 parent::__Construct();
	   	 $this->load->model(ADMIN_DIR.'/adminLogin','login');
		 $this->login->check_isvalidated();
		 $this->adminProfileInfo = $this->login->adminProfileInfo();
		 $this->load->library('form_validation');
 	     $this->image_path = realpath(APPPATH . '../uploads/page');
		 $this->load->model(ADMIN_DIR.'/AdminEvents','events');
		
		
	}
	
	public function lists()
	{
		$data = array();
		$data['module_heading'] = 'Events List';
		$data['adminProfileInfo'] = $this->adminProfileInfo;
		$this->load->view(ADMIN_DIR.'/'.ADMIN_HEADER_VERSION.'/common/header',$data);		
		$this->load->view(ADMIN_DIR.'/'.ADMIN_LEFT_SIDEBAR_VERSION.'/common/sidebar',$data);		
		$this->load->view(ADMIN_DIR.'/'.ADMIN_PAGE_DASHBOARD_VERSION.'/event_list',$data);		
		$this->load->view(ADMIN_DIR.'/'.ADMIN_FOOTER_VERSION.'/common/footer',$data);
	} 
	
	public function get_all_list()
	{
			 if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
				 
					$response = $this->events->updateStatus($_POST['id'],$_REQUEST['customActionName']);
				 	$status = $response['status'];
					$message = $response['message'];
				
		   }
		   $list = $this->events->get_datatables();
       	   $data = array();
           $no = $_POST['start'];
		   $data = array();
        	$no = $_POST['start'];
		foreach ($list as $event) {
			$no++;
            $possible_status_changes = '';
			$row = array();
			$row[] = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$event->id.'"/><span></span></label>';
			$row[] = ucfirst($event->eventName);
			$row[] = ucfirst($event->eventCategory);
			$row[] = ucfirst($event->eventDate); 
			$row[] = ucfirst($event->eventStartTime);
			$row[] = ucfirst($event->eventEndTime);
            $row[] = date(DATE_FORMAT,$event->createdDate);
			$row[] = date(DATE_FORMAT,$event->updatedDate);
			if($event->status=='Active')
			{
				$row[] = '<button class="btn btn-success">'.$event->status.'</button>';
			}
			else if($event->status=='Completed')
			{
				$row[] = '<button class="btn btn-info">'.$event->status.'</button>';
			}
			else
			{
				$row[] = '<button class="btn btn-danger">'.$event->status.'</button>';
			}
			//add html for action
			
			$row[] = '<div class="btn-group">
                                                                                    <a data-toggle="dropdown" href="javascript:;" class="btn purple" aria-expanded="true">
                                                                                        <i class="fa fa-user"></i> Settings
                                                                                        <i class="fa fa-angle-down"></i>
                                                                                    </a>
                                                                                    <ul class="dropdown-menu">
                                                                                        <li>
                                                                                            <a href="'.base_url(ADMIN_DIR.'/events/view/'.$event->id).'"><i class="fa fa-eye"></i> View</a>
                                                                                        </li>
																					    <li>
                                                                                            <a  href="'.base_url(ADMIN_DIR.'/events/edit/'.$event->id).'"><i class="fa fa-pencil"></i> Edit</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a  href="'.base_url(ADMIN_DIR.'/events/delete/'.$event->id).'"><i class="fa fa-trash"></i> Delete</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>';
            $data[] = $row;
        }
 
 		$output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->events->count_all(),
                        "recordsFiltered" => $this->events->count_filtered(),
                        "data" => $data,
						
                );
        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
				 
				$output["customActionStatus"] = $status; // OK for success and NOt OK for fail. pass custom message(useful for getting status of group actions)
				$output["customActionMessage"] = $message; // pass custom message(useful for getting status of group actions)
		   }
		//output to json format
		  echo json_encode($output);
	}
	
	
	
	
	
	public function delete()
	{
		$array = $this->uri->uri_to_assoc();
		$event_id = $array['delete'];
		$response = $this->events->deleteEvent($event_id);	
		if($response>0)
			{
				$this->session->set_flashdata('message_notification','Event Deleted Successfully');
				$this->session->set_flashdata('class',A_SUCCESS);
				redirect(ADMIN_DIR.'/events/lists');
			}
		else
			{
				$this->session->set_flashdata('message_notification','Event Not Deleted Successfully');
				$this->session->set_flashdata('class',A_FAIL);
				redirect(ADMIN_DIR.'/events/lists');
			}
	}
	
	public function add()
	{
		$data['module_heading'] = 'Add Event';
		$data['adminProfileInfo'] = $this->adminProfileInfo;
		$this->load->view(ADMIN_DIR.'/'.ADMIN_HEADER_VERSION.'/common/header',$data);		
		$this->load->view(ADMIN_DIR.'/'.ADMIN_LEFT_SIDEBAR_VERSION.'/common/sidebar',$data);		
		$this->load->view(ADMIN_DIR.'/'.ADMIN_PAGE_DASHBOARD_VERSION.'/event_add',$data);		
		$this->load->view(ADMIN_DIR.'/'.ADMIN_FOOTER_VERSION.'/common/footer',$data);
	}
	public function view()
	{
		$data['module_heading'] = 'View Event';
		$data['adminProfileInfo'] = $this->adminProfileInfo;
		$event_id = $this->uri->segment('4');
		$data['eventInfo'] = $this->events->viewEvent($event_id);
		if(!empty($data['eventInfo']))
		{
			$this->load->view(ADMIN_DIR.'/'.ADMIN_HEADER_VERSION.'/common/header',$data);		
			$this->load->view(ADMIN_DIR.'/'.ADMIN_LEFT_SIDEBAR_VERSION.'/common/sidebar',$data);		
			$this->load->view(ADMIN_DIR.'/'.ADMIN_PAGE_DASHBOARD_VERSION.'/event_view',$data);		
			$this->load->view(ADMIN_DIR.'/'.ADMIN_FOOTER_VERSION.'/common/footer',$data);
		}
		else
		{
			$this->lists();
		}
	}
	
	public function edit()
	{
		$data['module_heading'] = 'Edit Event';
		$data['adminProfileInfo'] = $this->adminProfileInfo;
		$event_id = $this->uri->segment('4');
		$data['eventInfo'] = $this->events->viewEvent($event_id);
		if(!empty($data['eventInfo']))
		{
			$this->load->view(ADMIN_DIR.'/'.ADMIN_HEADER_VERSION.'/common/header',$data);		
			$this->load->view(ADMIN_DIR.'/'.ADMIN_LEFT_SIDEBAR_VERSION.'/common/sidebar',$data);		
			$this->load->view(ADMIN_DIR.'/'.ADMIN_PAGE_DASHBOARD_VERSION.'/event_edit',$data);		
			$this->load->view(ADMIN_DIR.'/'.ADMIN_FOOTER_VERSION.'/common/footer',$data);
		}
		else
		{
			$this->lists();
		}
	}
	
	public function add_event()
	{
		
		/*echo '<pre>';
		print_r($_POST);
		exit;*/
		
		$config = array(
						array(
								'field' => 'eventName',
								'label' => 'Event Name',
								'rules' => 'required',
								'errors' => array(
										'required' => 'Please Enter The Event Name'
								),
						),
						array(
								'field' => 'eventCategory',
								'label' => 'Event Category',
								'rules' => 'required',
								'errors' => array(
										'required' => 'Please Enter The Event Category'
								),
						),
						array(
								'field' => 'eventDate',
								'label' => 'Event Date',
								'rules' => 'required',
								'errors' => array(
										'required' => 'Please Enter The Event Date'
								),
						),
						array(
							'field' => 'eventStartTime',
							'label' => 'Event Start Time',
							'rules' => 'required',
							'errors' => array(
									'required' => 'Please Enter The Event Start Time'
							),
						),
						array(
							'field' => 'eventEndTime',
							'label' => 'Event End Time',
							'rules' => 'required',
							'errors' => array(
									'required' => 'Please Enter The Event End Time'
							),
						),
						array(
								'field' => 'location',
								'label' => 'Location',
								'rules' => 'required',
								'errors' => array(
										'required' => 'Please Enter The Event Location'
								),
						),
						array(
								'field' => 'direction',
								'label' => 'Direction',
								'rules' => 'required',
								'errors' => array(
										'required' => 'Please Enter The Event Direction'
								),
						),
						array(
							'field' => 'eventCapacity',
							'label' => 'Event Capacity',
							'rules' => 'required|numeric',
							'errors' => array(
									'required' => 'Please Enter The Page Content',
									'numeric'   => 'Please Enter Valid Event Capacity'
							),
						),
						array(
							'field'=>'bookMyShowLink',
							'label'=>'Book My Show Link',
							'rules'=>'valid_url|callback_check_link['.$this->input->post('isBookMyShow').']',
							'errors'=>array(
									'valid_url'=>'Please Enter Valid Book My Show Link',
									'check_link'=>'Please Enter The Book My Show Link'
									),
						),
						/*array(
							'field' => 'remarks',
							'label' => 'Remarks',
							'rules' => 'required',
							'errors' => array(
									'required' => 'Please Enter The Remarks Of The Event'
									),
						),*/
						array(
								'field' => 'status',
								'label' => 'Status',
								'rules' => 'required',
								'errors' => array(
										'required' => 'Please Enter The Event Status'
										),
						)
				);
		
		$this->form_validation->set_rules($config);
		
	
		if($this->form_validation->run()== FALSE)
		{
				$this->session->set_flashdata('message_notification',validation_errors());
				$this->session->set_flashdata('class',A_FAIL);
				redirect(ADMIN_DIR.'/events/add');
		}
		else
		{	
			$eventData = array(
									"eventName"			=>	$this->input->post('eventName'),
									"eventCategory"   	=>  $this->input->post('eventCategory'),
									"eventDate"			=> 	$this->input->post('eventDate'),
									"location" 			=> 	$this->input->post('location'),
									"direction"			=> 	$this->input->post('direction'),
									"eventStartTime"	=>	$this->input->post('eventStartTime'),
									"eventEndTime"		=>	$this->input->post('eventEndTime'),
									"appearance"		=>	$this->input->post('appearance'),
									"eventCapacity"		=> 	$this->input->post('eventCapacity'),
									"status"			=>	$this->input->post('status'),
									"remarks"			=>	$this->input->post('remarks'),
									"isFarmerEvents"	=>  $this->input->post('isFarmerEvents'),
									"isBookMyShow"		=>	$this->input->post('isBookMyShow'),
									"bookMyShowLink"	=>	$this->input->post('bookMyShowLink'),
									"createdDate"		=>  strtotime(date('Y-m-d H:i:s')),
									"updatedDate"		=>  strtotime(date('Y-m-d H:i:s')),
									"ipAddress" 		=>  $this->input->ip_address()
								);
			$response = $this->events->addEvent($eventData);
			if($response>0)
			{
				$this->session->set_flashdata('message_notification','Event Added Successfully');
				$this->session->set_flashdata('class',A_SUCCESS);
				redirect(ADMIN_DIR.'/events/lists');
			}
			else
			{
				$this->session->set_flashdata('message_notification','Event Not Added Successfully');
				$this->session->set_flashdata('class',A_FAIL);
				 redirect(ADMIN_DIR.'/events/add');
			}			
			
		}
	
	}
	
	public function update_event()
	{
		
		/*echo '<pre>';
		print_r($_POST);
		exit;*/
		$config = array(
			array(
					'field' => 'eventName',
					'label' => 'Event Name',
					'rules' => 'required',
					'errors' => array(
							'required' => 'Please Enter The Event Name'
					),
			),
			array(
					'field' => 'eventCategory',
					'label' => 'Event Category',
					'rules' => 'required',
					'errors' => array(
							'required' => 'Please Enter The Event Category'
					),
			),
			array(
					'field' => 'eventDate',
					'label' => 'Event Date',
					'rules' => 'required',
					'errors' => array(
							'required' => 'Please Enter The Event Date'
					),
			),
			array(
				'field' => 'eventStartTime',
				'label' => 'Event Start Time',
				'rules' => 'required',
				'errors' => array(
						'required' => 'Please Enter The Event Start Time'
				),
			),
			array(
				'field' => 'eventEndTime',
				'label' => 'Event End Time',
				'rules' => 'required',
				'errors' => array(
						'required' => 'Please Enter The Event End Time'
				),
			),
			array(
					'field' => 'location',
					'label' => 'Location',
					'rules' => 'required',
					'errors' => array(
							'required' => 'Please Enter The Event Location'
					),
			),
			array(
					'field' => 'direction',
					'label' => 'Direction',
					'rules' => 'required',
					'errors' => array(
							'required' => 'Please Enter The Event Direction'
					),
			),
			array(
				'field' => 'eventCapacity',
				'label' => 'Event Capacity',
				'rules' => 'required|numeric',
				'errors' => array(
						'required' => 'Please Enter The Page Content',
						'numeric'   => 'Please Enter Valid Event Capacity'
				),
			),
			array(
				'field'=>'bookMyShowLink',
				'label'=>'Book My Show Link',
				'rules'=>'valid_url|callback_check_link['.$this->input->post('isBookMyShow').']',
				'errors'=>array(
						'valid_url'=>'Please Enter Valid Book My Show Link',
						'check_link'=>'Please Enter The Book My Show Link'
						),
			),
			/*array(
				'field' => 'remarks',
				'label' => 'Remarks',
				'rules' => 'required',
				'errors' => array(
						'required' => 'Please Enter The Remarks Of The Event'
						),
			),*/
			array(
					'field' => 'status',
					'label' => 'Status',
					'rules' => 'required',
					'errors' => array(
							'required' => 'Please Enter The Event Status'
							),
			)
	);

		$this->form_validation->set_rules($config);
			
		if($this->form_validation->run()== FALSE)
		{
				$this->session->set_flashdata('message_notification',validation_errors());
				$this->session->set_flashdata('class',A_FAIL);
				redirect(ADMIN_DIR.'/events/edit/'.$this->input->post('id'));
		}
		else
		{
			$eventData = array(
									"eventName"			=>	$this->input->post('eventName'),
									"eventCategory"   	=>  $this->input->post('eventCategory'),
									"eventDate"			=> 	$this->input->post('eventDate'),
									"location" 			=> 	$this->input->post('location'),
									"direction"			=> 	$this->input->post('direction'),
									"eventStartTime"	=>	$this->input->post('eventStartTime'),
									"eventEndTime"		=>	$this->input->post('eventEndTime'),
									"appearance"		=>	$this->input->post('appearance'),
									"eventCapacity"		=> 	$this->input->post('eventCapacity'),
									"remarks"			=>	$this->input->post('remarks'),
									"isFarmerEvents"	=>  $this->input->post('isFarmerEvents'),
									"isBookMyShow"		=>	$this->input->post('isBookMyShow'),
									"bookMyShowLink"	=>	$this->input->post('bookMyShowLink'),
									"status"			=>	$this->input->post('status'),
									"updatedDate"		=>  strtotime(date('Y-m-d H:i:s')),
									"ipAddress" 		=>  $this->input->ip_address()
								);
			$response = $this->events->editEvent($eventData,$this->input->post('id'));
			if($response>0)
			{
				$this->session->set_flashdata('message_notification','Event Updated Successfully');
				$this->session->set_flashdata('class',A_SUCCESS);
				redirect(ADMIN_DIR.'/events/lists');
			}
			else
			{
				$this->session->set_flashdata('message_notification','Event Not Updated Successfully');
				$this->session->set_flashdata('class',A_FAIL);
				redirect(ADMIN_DIR.'/events/edit/'.$this->input->post('id'));
			}			
			
		}
	
	}

	public function check_link($link,$isBookMyShow='No')
	{
		//exit($isBookMyShow);
		if($isBookMyShow=='Yes' and $link=='')
		{
			return FALSE;
		}
		else{
			return TRUE;
		}
	}
	
}
?>
