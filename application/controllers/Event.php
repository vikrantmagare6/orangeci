<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {
	private $perPage = 9;


	public function __Construct()
	{
			parent::__Construct();
			$this->load->model(PUBLIC_DIR.'/eventPage','events');
	}

	public function test22(){

		// $this->load->library('twilio');

		// $client = new TwilioRestClient('AC0806e1f057deb5bf51e847fbb59476cb', '62ac3ec42f13e63d1aa784fb3f606a98');
		// $client->messages->create(
		// 	    "+919769667358",
		// 	    array(
		// 	        "from" => "+14158256453",
		// 	        "body" => "letest test send sms and redirect to page quckly "
		// 	    )
		// 	);

		// return var_dump($client);
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/test');		
		
	}
	public function test23() {
		$this->load->library('EmailSender.php');
		
        $testa = $this->emailsender->send('vikrant@togglehead.in','subject change','body change');
        echo var_dump($testa);

	}	
	
	public function schedule()
	{
		$header = array();
		$header['module_name'] = 'Event Schedule';
		$content = array();
		$footer = array();
		$content['event_limit'] = '9';
		$content['eventDate'] = $eventDate = (isset($_GET['eventDate']) and $_GET['eventDate']!='')?$_GET['eventDate']:'';
		$total_data = $this->events->get_all_content('0',$content['event_limit'],$eventDate);
		/*echo '<pre>';
		print_r($total_data);
		exit;*/
		
        $content['events'] = $total_data;
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/header',$header);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/event_schedule',$content);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/footer',$footer);
	}

	public function load_more()
    {
		if(isset($_POST["limit"], $_POST["start"]))
		{
			$all_content = $this->events->get_all_content($_POST['start'],$_POST['limit'],$_POST['eventDate']);
			if(isset($all_content) && is_array($all_content) && count($all_content)) : 
				foreach ($all_content as $key => $content) :
					$event_registration_count = $this->events->registerCount($content->id);
					$registration_url = base64_encode($content->eventName.' & '.$content->eventDate);
					if($content->id>0)
					//if(($content->eventCapacity>0 and $event_registration_count>=$content->eventCapacity) or ($content->id!=22 and $content->id!=20))
					{
						$href = "javascript:void(0);";
						$label = 'Registration Closed';
						/*$registrationLink = '<a href="javascript:void(0);" >Registration Closed</a>';
						echo $registrationLink;*/
					}
					else{
						$registration_url = base64_encode($content->eventName.' & '.$content->eventDate.' & '.$content->id);
						$href = base_url('registration/'.$registration_url);
						/*$registrationLink = '<a href="'.base_url('registration/'.$registration_url).'>register now</a>';
						echo ' <a href="javascript:void(0);" >Registration Closed</a>';*/
						$label = 'Register Now';
					}
					switch ($content->eventCategory) {
                        case "Art & Culture":
                            $icon =  "art-and-culture.png";
                            break;
                        case "Games":
                            $icon =  "games.png";
                            break;
                        case "Entertainment":
                            $icon =  "music.png";
                            break;
                        case "Food":
                            $icon = "food.png";
                            break;
                        default:
                            $icon =  "other-icon.png";
                    }    

					echo '<div class="item col-md-4 col-sm-6">
					<div class="item-inner">
						<div class="liv">
							<span>'.$content->eventName.'</span>
							<img src="'.base_url('uploads/event/'.$icon).'">
						</div>
						<ul>
							<li>
								<i class="fa fa-calendar" aria-hidden="true"></i> '.$content->eventDate.' 2017
							</li>
							<li>
								<i class="fa fa-clock-o" aria-hidden="true"></i> '.$content->eventStartTime.' - '.$content->eventEndTime.'
							</li>
							<li>
								<i class="fa fa-map-marker" aria-hidden="true"></i> '.$content->location.'
							</li>
							<li>
								<i class="fa fa-map-o" aria-hidden="true"></i> <a class="getdir" href="'.$content->direction.'" target="_blank">Get Direction</a>
							</li>
						</ul>
						<a href="'.$href.'" class="regis">'.$label.'</a>
					</div>
				</div>';              
				endforeach;                                
			endif; 
			// echo '<pre>'; print_r($this->data['labels_message']); exit;
		}
	}
	
	public function submit()
	{
		$commonRegister =  uniqid();
		/*echo '<pre>';
		print_r($_POST);
		exit;*/
		/*$already_register = $this->events->registerCount($this->input->post('eventId'));
		$capacity_register = $this->events->capacityEvent($this->input->post('eventId'));
		//$already_register = $this->events->registerCount(3);
		//$capacity_register = 50;
		if($capacity_register->eventCapacity>0 and $already_register>=$capacity_register->eventCapacity)
		{
			// It means registration is already full.
			redirect(base_url('closed'));
		}*/
		$already_register = $this->events->registerCount($this->input->post('eventId'));
		$capacity_register = $this->events->capacityEvent($this->input->post('eventId'));
		$remaining_register = intval($capacity_register->eventCapacity)-intval($already_register);

		if($capacity_register->eventCapacity>0 and $already_register>=$capacity_register->eventCapacity)
		{
			// It means registration is already full.
			redirect(base_url('closed'));
		}
		else if($capacity_register->eventCapacity>0 and $remaining_register < count($this->input->post('name')))
		{
			//It means that, the number of registration you are doing is more than number of registration is available. So, it will give you an error
			$this->session->set_flashdata('message_notification','Sorry, Only '.$remaining_register.' seats available for this event.');
			$this->session->set_flashdata('class','danger');
			redirect($this->input->post('redirectURL'));	
		}
		else{
			$count = count($this->input->post('name'));
			for($i=0; $i<$count; $i++)
			{
				$registration_id['id'] = $already_register+$i+1;
				$registration_id['name'] = $this->input->post('name')[$i];
				$registration_id['name'] = $this->input->post('name')[$i];
				$data[] = array(
					'fullName'=>$this->input->post('name')[$i],
					'mobileNumber'=>$this->input->post('phone')[$i],
					'commonRegister'=>$commonRegister,
					'email'=>$this->input->post('email'),
					'eventId'=>$this->input->post('eventId'),
					'registrationNumber'=>$already_register+$i+1,
					'registerDate'=>strtotime(date('Y-m-d H:i:s')),
					'status'=>'Success',
					'updatedDate'=>strtotime(date('Y-m-d H:i:s')),
					'ipAddress'=>$this->input->ip_address(),
				);
			}
			$this->db->insert_batch('eventregistration',$data);
			$this->sendEmail($commonRegister,$this->input->post('redirectURL'));
		}

		/*echo '<pre>';
		print_r($data);
		exit;*/

	}

	public function sendEmail($commonRegister,$redirectURL)
	{
		$registrationDetail = $this->events->getRegistrationDetail($commonRegister);
		/*echo '<pre>';
		print_r($registrationDetail);
		exit;*/
		if(!empty($registrationDetail))
		{
				$this->load->library('email');
				
				$config = array (
						'mailtype' => 'html',
						'charset'  => 'utf-8',
						'priority' => '1'
						);
				$this->email->initialize($config);
				$registrationId = '';
				foreach($registrationDetail as $v)
				{
					$registrationId .= $v->registrationNumber.',';
				}
				$registrationId = rtrim($registrationId,',');
				$this->email->from('no-reply@worldorangefestival.com', 'Orange Festival');
				$this->email->to($registrationDetail[0]->email);
				if($registrationDetail[0]->id==22)
				{
				   $bennyPass = 'Collect your passes from the Lokmat office by showing this email.';
				}
				else {
				   $bennyPass = '';
				}
				$this->email->subject('Registration Sucessfull');
				$email_data = array("eventName"=>$registrationDetail[0]->eventName,
									"fullName"=>$registrationDetail[0]->fullName,
									"eventDate"=>$registrationDetail[0]->eventDate,
									"eventTime"=>$registrationDetail[0]->eventStartTime.' - '.$registrationDetail[0]->eventEndTime,
									'eventLocation'=>$registrationDetail[0]->location,
									'eventDirection'=>$registrationDetail[0]->direction,
									'registrationID'=> $registrationId,
									'bennyPass'=> $bennyPass
							 );
				
				$email_content = $this->load->view('thank-you',$email_data,true);	
				
				$this->email->message($email_content);
				
				//Send mail 
				if($this->email->send()) 
				{
					redirect(base_url('thank-you/'.$commonRegister));							 
				}
				else
				{
					redirect(base_url('thank-you/'.$commonRegister));
				}
		}
		else{
			$this->session->set_flashdata('message_notification','Your Registration Is Not Completed Successfully, Please Try Again.');
			$this->session->set_flashdata('class','danger');
			redirect($redirectURL);	
		}
	}


}
