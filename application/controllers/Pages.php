<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
	
	public function __Construct()
	{
			parent::__Construct();
			$this->load->model(PUBLIC_DIR.'/eventPage','events');
	}	
	
	public function activities()
	{
		$header = array();
		$header['module_name'] = 'Activities';
		$content = array();
		$footer = array();
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/header',$header);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/activities',$content);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/footer',$footer);
	}

	public function partners()
	{
		$header = array();
		$header['module_name'] = 'Partners';
		$content = array();
		$footer = array();
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/header',$header);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/partners',$content);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/footer',$footer);
	}

	public function venue()
	{
		$header = array();
		$content = array();
		$sidebar = array();
		$footer = array();
		$footer['module_name'] = $header['module_name'] = 'Venue';
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/header',$header);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/venue',$content);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/footer',$footer);
	}

	public function game()
	{
		$header = array();
		$header['module_name'] = 'Tic Toc Toe';
		$content = array();
		$sidebar = array();
		$footer = array();
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/header',$header);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/game',$content);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/footer',$footer);
	}

	public function gallery()
	{
		$header = array();
		$header['module_name'] = 'Gallery';
		$content = array();
		$sidebar = array();
		$footer = array();
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/header',$header);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/gallery',$content);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/footer',$footer);
	}

	public function closed()
	{
		$header = array();
		$header['module_name'] = 'Registration Closed';
		$content = array();
		$sidebar = array();
		$footer = array();
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/header',$header);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/closed',$content);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/footer',$footer);
	}

	public function thankyou()
	{
		$header = array();
		$header['module_name'] = 'Thank You';
		$content = array();
		$sidebar = array();
		$footer = array();
		$commonRegister =  $this->uri->segment(2);
		$registrationDetail = $this->events->getRegistrationDetail($commonRegister);
		/*echo '<pre>';
		print_r($registrationDetail);
		exit;*/
		$content['eventName'] = $registrationDetail[0]->eventName;
		$content['commonRegister'] = $commonRegister;
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/header',$header);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/thank-you',$content);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/footer',$footer);
	}

}
