<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activities extends CI_Controller {
	
	public function __Construct()
	{
	   	 parent::__Construct();
	   	 $this->load->model(PUBLIC_DIR.'/homePage','home');
		 $this->load->model(PUBLIC_DIR.'/commonPage','common');
	}	
	
	public function index()
	{
		$header = array();
		if(isset($_GET['activities']) and $_GET['activities']!='')
		{
			$view_page = 'activities_detail';
			$header['module_name'] = $_GET['activities'];
		}
		else{
			$view_page = 'activities';
			$header['module_name'] = 'Activities';
		}
		$content = array();
		$sidebar = array();
		$footer = array();
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/header',$header);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/'.$view_page,$content);		
		$this->load->view(PUBLIC_DIR.'/'.PUBLIC_VERSION.'/common/footer',$footer);
	}
}
