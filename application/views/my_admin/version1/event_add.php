<div class="page-content-col">
     <!-- BEGIN PAGE BASE CONTENT -->
     <form name="add_event" id="add_event" method="post" action="<?= base_url(ADMIN_DIR.'/events/add_event/'); ?>" enctype="multipart/form-data">
           <div class="row">
                                <div class="col-sm-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-speech font-green"></i>
                                                    <span class="caption-subject bold font-green uppercase">Basic Info</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                    <div class="row">
                                                            <div class="col-sm-12">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Event Name <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                        <input type="text" placeholder="Live Bands" name="eventName" class="form-control" id="eventName" value=""> 
                                                               </div>
                                                            </div>
                                                          
                                                 	</div>
                                                    <div class="row">
                                                     <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="avatar">Event Category <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                <select class="form-control" name="eventCategory" id="eventCategory">
                                                                <option value="Art & Culture">Art & Culture</option>
                                                                <option value="Food">Food</option>
                                                                <option value="Entertainment">Entertainment</option>
                                                                <option value="Games">Games</option>
                                                                <option value="Others">Others</option>
                                                                </select>
                                                            </div>
                                                            </div>
                                                    
                                                    <div class="col-sm-6">
                                                    		<div class="form-group">
                                                                <label class="control-label">Event Date <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                <select name="eventDate" id="eventDate" class="form-control">
                                                                    <option value=""></option>
                                                                    <option value="16th December">16th December</option>
                                                                    <option value="17th December">17th December</option>
                                                                    <option value="18th December">18th December</option>
                                                                </select>
                                                        	</div>
                                                    </div>
                                                      </div>
                                                      <div class="row">
                                                            <div class="col-sm-12">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Event Location <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                        <input type="text" placeholder="Event Location" name="location" class="form-control" id="location" value=""> 
                                                               </div>
                                                            </div>
                                                          
                                                 	</div>

                                                     <div class="row">
                                                            <div class="col-sm-12">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Event Direction <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                        <input type="text" placeholder="Google Map Address Goes Here..." name="direction" class="form-control" id="direction" value=""> 
                                                               </div>
                                                            </div>
                                                          
                                                 	</div>

                                                     <div class="row">
                                                             <div class="col-sm-2">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Event Start Time <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                    <input type="text" value="2:30 PM" name="eventStartTime" id="eventStartTime" data-format="hh:mm A" class="form-control clockface_1 clockface-open"> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Event End Time <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                    <input type="text" value="2:30 PM" name="eventEndTime" id="eventEndTime" data-format="hh:mm A" class="form-control clockface_1 clockface-open"> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Appearance</label>
                                                                        <input type="text" placeholder="Benny Dayal" name="appearance" class="form-control" id="appearance" value=""> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Event Capacity <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                        <input type="text" placeholder="15000" name="eventCapacity" class="form-control" id="eventCapacity" value=""> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Status <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                    <select name="status" id="status" class="form-control">
                                                                    <option value=""></option>
                                                                    <option value="Active">Active</option>
                                                                    <option value="Cancelled">Cancelled</option>
                                                                    <option value="Completed">Completed</option>
                                                                </select> 
                                                               </div>
                                                            </div>
                                                          
                                                 	</div>

                                                     <div class="row">
                                                     <div class="col-sm-2">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Is Farmers Event ?</label>
                                                                    <select name="isFarmerEvents" class="form-control" id="isFarmerEvents">
                                                                    <option value=""></option>
                                                                    <option value="Yes">Yes</option>
                                                                    <option value="No">No</option>
                                                                    </select> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Is Link To Book My Show ?</label>
                                                                    <select name="isBookMyShow" class="form-control" id="isBookMyShow">
                                                                    <option value=""></option>
                                                                    <option value="Yes">Yes</option>
                                                                    <option value="No">No</option>
                                                                    </select> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Book My Show Link</label>
                                                                        <input type="text" placeholder="https://www.bookmyshow.com" disabled name="bookMyShowLink" class="form-control" id="bookMyShowLink" value=""> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Remarks </label>
                                                                    <textarea name="remarks" id="remarks" class="form-control"></textarea> 
                                                               </div>
                                                            </div>
                                                 	</div>

                                                    
                                            </div>
                                        </div>
                                </div>
           </div>
     
           <div class="row">
                                <div class="form-actions">
                                                        
                                                            <div class="text-center">
                                                                <input class="btn green" type="submit" name="submit" id="submit" value="Submit">
                                                                <!--<input class="btn default" type="reset" name="reset" id="reset" value="Cancel">-->
                                                            </div>
                                                        
                                                    </div>
                                 </div>
     </form>
     <!-- END PAGE BASE CONTENT -->
 </div>
 </div>
<script>
    $(document).ready(function(e) {
		
        $('#isBookMyShow').on('change', function(){
            if($('#isBookMyShow').val()=='Yes'){
                $('#bookMyShowLink').attr("disabled", false);
            } 
            else{
                $('#bookMyShowLink').attr("disabled", "disabled");
            }
});

		$('#add_event').validate({
					ignore: [],
					errorClass:'<?php echo A_JQUERY_VALIDATION_ERROR_CLASS_FULL; ?>',
					validClass:'<?php echo A_JQUERY_VALIDATION_SUCCESS; ?>',
					rules: {
						eventName : {required:true},
						eventCategory : { required:true},
						eventDate :{required:true},
                        eventStartTime : {required:true},
                        eventEndTime: {required:true},
						location : {required:true},
						direction : {required:true},
                        eventCapacity : {required:true,number:true},
                        //remarks : {required:true},
                        bookMyShowLink :{url:true,required:function(element){return $("#isBookMyShow").val()=="Yes";}},
						status : {required:true}
					},
					messages : {
						eventName : {required:"Please Enter Event Name"},
						eventCategory : { required:"Please Enter Event Category"},
						eventDate :{required:"Please Select Event Date"},
                        eventStartTime : {required:"Please Enter Event Start Time"},
                        eventEndTime : {required:"Please Enter Event End Time"},
						location : {required:"Please Enter Event Location"},
						direction : {required:"Please Enter Event Direction"},
                        eventCapacity : {required:"Please Enter Event Capacity",number:"Please Enter Valid Capacity"},
                        bookMyShowLink : {url:"Please Enter Valid Book My Show Link",required:"Please Enter Book My Show Link"},
                        //remarks : {required:"Please Enter Remarks Of Event"},
						status : {required:"Please Select Event Status"}
					}
				});
    });
    </script>
    <script src="<?= base_url('assets/library/ckeditor/ckeditor.js'); ?>"></script>
	<script src="<?= base_url('assets/library/ckeditor/adapters/jquery.js'); ?>"></script>