<!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= base_url('theme/admin/assets/pages/css/blog.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
<div class="page-content-col">
                                <!-- BEGIN PAGE BASE CONTENT -->
                                <div class="blog-page blog-content-2">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="blog-single-content bordered blog-container">
                                                <div class="blog-single-head">
                                                    <h1 class="blog-single-head-title"><?= $registrationInfo[0]->eventName; ?></h1> (<a href="<?= base_url(ADMIN_DIR.'/registered/lists'); ?>" title="Back To Events Lists">Back To List</a>)
                                                    <div class="blog-single-head-date">
                                                    	
                                                        <i class="icon-calendar font-blue"></i>
                                                        <a href="javascript:;"><?= date(DATE_FORMAT,$registrationInfo[0]->registerDate); ?></a>
                                                        
                                                    </div>
                                                </div>
                                                
                                                 <div class="blog-single-foot">
                                                 	<h3>Event Info</h3>
                                                    <p><strong>Event Category</strong> : <?= $registrationInfo[0]->eventCategory; ?></p>
                                                    <p><strong>Event Date</strong> : <?= $registrationInfo[0]->eventDate; ?></p>
                                                    <p><strong>Event Time</strong> : <?= $registrationInfo[0]->eventStartTime.' - '.$registrationInfo[0]->eventEndTime; ?></p>
                                                    <p><strong>Email :</strong> <?= $registrationInfo[0]->email; ?></p>
                                                    <p><strong>Registration Date :</strong> <?= date(DATE_FORMAT,$registrationInfo[0]->registerDate); ?></p>
                                                    <p><strong>Location :</strong> <?= $registrationInfo[0]->location; ?> - <a href="<?= $registrationInfo[0]->direction; ?>" target="_blank">direction</a></p>
                                                    <p><strong>Participants Details :</strong></p>
                                                    <?php $number=1; foreach($registrationInfo as $v) { ?>
                                                        <div class="portlet light bordered">
                                                <!-- STAT -->
                                                
                                                <!-- END STAT -->
                                                    <div>
                                                        <h4 class="profile-desc-title">Participant <?php echo $number; ?></h4>
                                                        <div class="margin-top-20 profile-desc-link">
                                                            <i class="fa fa-user"></i>
                                                            <?= $v->fullName; ?>
                                                        </div>
                                                        <div class="margin-top-20 profile-desc-link">
                                                            <i class="fa fa-phone"></i>
                                                            <?= $v->mobileNumber; ?>
                                                        </div>
                                                        <div class="margin-top-20 profile-desc-link">
                                                            <i class="fa fa-list"></i>
                                                            Registration Number : <?= $v->registrationNumber; ?>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <?php $number++; } ?>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                                <!-- END PAGE BASE CONTENT -->
                            </div>
                            </div>