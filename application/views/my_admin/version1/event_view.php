<!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= base_url('theme/admin/assets/pages/css/blog.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
<div class="page-content-col">
                                <!-- BEGIN PAGE BASE CONTENT -->
                                <div class="blog-page blog-content-2">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="blog-single-content bordered blog-container">
                                                <div class="blog-single-head">
                                                    <h1 class="blog-single-head-title"><?= $eventInfo->eventName; ?></h1> <a href="<?= base_url(ADMIN_DIR.'/events/edit/'.$eventInfo->id); ?>" title="Edit <?= $eventInfo->eventName; ?>">Edit </a> |(<a href="<?= base_url(ADMIN_DIR.'/events/lists'); ?>" title="Back To Events Lists">Back To List</a>)
                                                    <div class="blog-single-head-date">
                                                    	
                                                        <i class="icon-calendar font-blue"></i>
                                                        <a href="javascript:;"><?= date(DATE_FORMAT,$eventInfo->createdDate); ?></a>
                                                        
                                                    </div>
                                                </div>
                                                
                                                 <div class="blog-single-foot">
                                                 	<h3>Event Info</h3>
                                                    <p><strong>Event Category</strong> : <?= $eventInfo->eventCategory; ?></p>
                                                    <p><strong>Event Date</strong> : <?= $eventInfo->eventDate; ?></p>
                                                    <p><strong>Event Time</strong> : <?= $eventInfo->eventStartTime;?> - <?= $eventInfo->eventEndTime; ?></p>
                                                    <p><strong>Event Location</strong> : <?= $eventInfo->location; ?></p>
                                                    <p><strong>Event Direction</strong> : <a href="<?= $eventInfo->direction ?>" target="_blank">Direction</a></p>
                                                    <p><strong>Appearance</strong> : <?= $eventInfo->appearance; ?></p>
                                                    <p><strong>Event Capacity</strong> : <?= $eventInfo->eventCapacity; ?></p>
                                                    <p><strong>Is Farmers Event ? </strong> : <?= $eventInfo->isFarmerEvents; ?></p>
                                                    <p><strong>Is Book My Show</strong> : <?= $eventInfo->isBookMyShow; ?></p>
                                                    <p><strong>Book My Show Link</strong> : <?= $eventInfo->bookMyShowLink; ?></p>
                                                    <p><strong>Remarks</strong> : <?= $eventInfo->remarks; ?></p>
                                                    <p><strong>Status</strong> : <?= $eventInfo->status; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                                <!-- END PAGE BASE CONTENT -->
                            </div>
                            </div>