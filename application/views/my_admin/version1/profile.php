<!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= base_url('theme/admin/assets/pages/css/profile.min.css'); ?>" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->    	  

<div class="page-content-col">
                                <!-- BEGIN PAGE BASE CONTENT -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN PROFILE SIDEBAR -->
                                        <div class="profile-sidebar">
                                            <!-- PORTLET MAIN -->
                                            <div class="portlet light profile-sidebar-portlet bordered">
                                                <!-- SIDEBAR USERPIC -->
                                                <div class="profile-userpic">
                                                    <img src="<?= base_url('uploads/admin/thumb/'.$adminProfileInfo->avatar); ?>" class="img-responsive" alt="<?= $adminProfileInfo->fname.' '.$adminProfileInfo->lname; ?>"> </div>
                                                <!-- END SIDEBAR USERPIC -->
                                                <!-- SIDEBAR USER TITLE -->
                                                <div class="profile-usertitle">
                                                    <div class="profile-usertitle-name"> <?= ucfirst($adminProfileInfo->fname).' '.ucfirst($adminProfileInfo->lname); ?> </div>
                                                    <div class="profile-usertitle-job"><i class="fa fa-at"></i> <?= $adminProfileInfo->uname; ?> </div>
                                                </div>
                                                <!-- END SIDEBAR USER TITLE -->
                                                <!-- SIDEBAR MENU -->
                                                <div class="profile-usermenu">
                                                   
                                                </div>
                                                <!-- END MENU -->
                                            </div>
                                            <!-- END PORTLET MAIN -->
                                        </div>
                                        <!-- END BEGIN PROFILE SIDEBAR -->
                                        <!-- BEGIN PROFILE CONTENT -->
                                        <div class="profile-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="portlet light bordered">
                                                        <div class="portlet-title tabbable-line">
                                                            <div class="caption caption-md">
                                                                <i class="icon-globe theme-font hide"></i>
                                                                <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                            </div>
                                                            <ul class="nav nav-tabs">
                                                                <li class="active">
                                                                    <a href="#personal_info" data-toggle="tab">Personal Info</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#change_avatar" data-toggle="tab">Change Avatar</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#change_password" data-toggle="tab">Change Password</a>
                                                                </li>
                                                               
                                                            </ul>
                                                        </div>
                                                        <div class="portlet-body">
                                                            <div class="tab-content">
                                                                <!-- PERSONAL INFO TAB -->
                                                                <div class="tab-pane active" id="personal_info">
                                                                    <form role="form" action="<?= base_url(ADMIN_DIR.'/profile/edit_personal_info') ?>" name="edit_personal_info" id="edit_personal_info" method="post">
                                                                        <div class="form-group">
                                                                            <label class="control-label">First Name</label>
                                                                            <input type="text" placeholder="Aliasgar" class="form-control"  name="fname" id="fname" value="<?= $adminProfileInfo->fname; ?>"/> 
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label">Last Name</label>
                                                                             <input type="text" placeholder="Vanak" class="form-control"  name="lname" id="lname" value="<?= $adminProfileInfo->lname; ?>"/> 
                                                                       </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label">Username</label>
                                                                             <input type="text" placeholder="aliasgar.vanak" class="form-control"  name="uname" id="uname" value="<?= $adminProfileInfo->uname; ?>"/> 
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label">Email</label>
                                                                             <input type="text" placeholder="aliasgar.vanak@gmail.com" class="form-control"  name="email" id="email" value="<?= $adminProfileInfo->email; ?>"/> 
                                                                       </div>
                                                                       
                                                                        
                                                                        
                                                                        <div class="margiv-top-10">
                                                                        	<input type="hidden" name="admin_id" value="<?= $adminProfileInfo->id; ?>">
                                                                            <input type="submit" class="btn green" name="personal_info_submit" id="personal_info_submit" value="Save Changes">
                                                                            <input type="reset" name="personal_info_rest" id="personal_info_reset" class="btn default" value="Cancel">
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                                <!-- END PERSONAL INFO TAB -->
                                                                <!-- CHANGE AVATAR TAB -->
                                                                <div class="tab-pane" id="change_avatar">
                                                                    <p> </p>
                                                                    <form action="<?= base_url(ADMIN_DIR.'/profile/edit_avatar_info'); ?>" name="edit_avatar_info" id="edit_avatar_info" role="form" enctype="multipart/form-data" method="post">
                                                                        <div class="form-group">
                                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                               
                                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                                <div>
                                                                                    <span class="btn default btn-file">
                                                                                        <span class="fileinput-new"> Select image </span>
                                                                                        <span class="fileinput-exists"> Change </span>
                                                                                        <input name="avatar" id="avatar" type="file"> </span>
                                                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clearfix margin-top-10">
                                                                                <span class="label label-danger">NOTE! </span>
                                                                                <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="margin-top-10">
                                                                        	<input type="hidden" name="old_avatar" id="old_avatar" value="<?= $adminProfileInfo->avatar?>">
                                                                            <input type="hidden" name="admin_id" value="<?= $adminProfileInfo->id; ?>">
                                                                            <input type="submit" class="btn green" name="avatar_info_submit" id="personal_info_submit" value="Save Changes">
                                                                            <input type="reset" name="avatar_info_rest" id="avatar_info_reset" class="btn default" value="Cancel">
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                                <!-- END CHANGE AVATAR TAB -->
                                                               
                                                                <!-- CHANGE PASSWORD TAB -->
                                                                <div class="tab-pane" id="change_password">
                                                                    <form action="<?= base_url(ADMIN_DIR.'/profile/edit_change_password'); ?>" name="edit_password_info" id="edit_password_info" method="post">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Current Password</label>
                                                                            <input type="password" name="current_password" id="current_password" class="form-control" value="" /> 
                                                                       </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label">New Password</label>
                                                                            <input type="password" name="new_password" id="new_password" value="" class="form-control" /> 
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label">Re-type New Password</label>
                                                                            <input type="password" name="confirm_new_password" id="confirm_new_password" class="form-control" value="" />
                                                                        </div>
                                                                        <div class="margin-top-10">
                                                                        <input type="hidden" name="admin_id" value="<?= $adminProfileInfo->id; ?>">
                                                                            <input type="submit" class="btn green" name="password_info_submit" id="password_info_submit" value="Save Changes">
                                                                            <input type="reset" name="password_info_rest" id="password_info_reset" class="btn default" value="Cancel">
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                                <!-- END CHANGE PASSWORD TAB -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END PROFILE CONTENT -->
                                    </div>
                                </div>
                                <!-- END PAGE BASE CONTENT -->
                            </div>
   
</div>

<script>
    $(document).ready(function(e) {
		
		 jQuery.validator.addMethod("fileSize", function (val, element) {

          var size = element.files[0].size;
            console.log(size);

           if (size > 2048576)// checks the file more than 1 MB
           {
                return false;
           } else {
               return true;
           }

      }, "Maximum 2MB Image Size Allowed");
		
		$('#edit_personal_info').validate({
					errorClass:'<?php echo A_JQUERY_VALIDATION_ERROR_CLASS_FULL; ?>',
					validClass:'<?php echo A_JQUERY_VALIDATION_SUCCESS; ?>',
					rules: {
						fname		:	{	required:	true	},
						lname 		: 	{ 	required:	true	}, 
						uname		:	{	required:	true	},
						email 		: 	{	required:	true,
											email	:	true    }
						},
					messages : {
						fname:{required: "Please Enter First Name"},
						lname : { required:"Please Enter Last Name"}, 
						uname:{required:"Please Enter Username"},
						email : {required:"Please Enter Email Address",email:"Please Enter Valid Email Address"}
					}
				});
		
		$('#edit_avatar_info').validate({
					errorClass:'<?php echo A_JQUERY_VALIDATION_ERROR_CLASS_FULL; ?>',
					validClass:'<?php echo A_JQUERY_VALIDATION_SUCCESS; ?>',
					rules: {
						avatar	: 	{	
										required: true,
										accept	:	"jpg|png|jpeg|gif",
										fileSize : true
										}
						},
					messages : {
						avatar : { 
									required: "Please Uplaod Your Avatar",
									accept : "Allowed Image Types Are JPG, PNG, JPEG, GIF"
								 }
					}
				});
		
		$('#edit_password_info').validate({
					errorClass:'<?php echo A_JQUERY_VALIDATION_ERROR_CLASS_FULL; ?>',
					validClass:'<?php echo A_JQUERY_VALIDATION_SUCCESS; ?>',
					rules: {
						current_password	: 	{	required:	true	},
						new_password 		: 	{
													required:true
												},
						confirm_new_password: 	{	required:	true,
											equalTo : 	'#new_password'}
						},
					messages : {
						current_password: {required:"Please Enter Your Current Password"},
						new_password : {required:"Please Enter Your New Password"},
						confirm_new_password : {required:"Please Enter Confirm Password",equalTo:"New Password And Confirm Password Should Match"},
					}
				});
		
    });
    </script>