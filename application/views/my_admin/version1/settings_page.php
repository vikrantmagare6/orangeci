<div class="page-content-col">
     <!-- BEGIN PAGE BASE CONTENT -->
    <div class="portlet box">
                                            
                                            <div class="portlet-body">
                                                <div class="tabbable-custom nav-justified">
                                                    <ul class="nav nav-tabs nav-justified">
                                                       <!-- <li class="active">
                                                            <a href="#general" data-toggle="tab"> General </a>
                                                        </li>-->
                                                        <li class="active">
                                                            <a href="#contact_page_tab" data-toggle="tab"> Contact Page </a>
                                                        </li>
                                                        
                                                    </ul>
                                                    <div class="tab-content">
                                                      <!--  <div class="tab-pane active" id="general">
                                                             <form name="general_details" id="general_details" method="post" action="<?= base_url(ADMIN_DIR.'/settings/general_details/'); ?>" enctype="multipart/form-data">
           <div class="row">
                                <div class="col-sm-12">
                                        <div class="portlet light">
                                            
                                            <div class="portlet-body">
                                                    <div class="row">
                                                            <div class="col-sm-12">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Logo</label>
                                                                         <input type="file" id="logo" name="logo">
                                                               </div>
                                                            </div>
                                                    </div>
                                            </div>
                                        </div>
                                </div>
           </div>
           
           <div class="row">
                                <div class="form-actions">
                                                        
                                                            <div class="text-center">
                                                            	<input type="hidden" name="oldLogo" id="oldLogo" value="<?= $settingDetails->logo; ?>">
                                                                <input class="btn green" type="submit" name="submit" id="submit" value="Submit">
                                                                <input class="btn default" type="reset" name="reset" id="reset" value="Cancel">
                                                            </div>
                                                        
                                                    </div>
                                 </div>
     </form>
                                                        </div>
                                                       --> <div class="tab-pane active" id="contact_page_tab">
                                                            <form name="contact_page_details" id="contact_page_details" method="post" action="<?= base_url(ADMIN_DIR.'/settings/contact_page/'); ?>" enctype="multipart/form-data">
           <div class="row">
                                <div class="col-sm-12">
                                        <div class="portlet light">
                                            
                                            <div class="portlet-body">
                                                    <div class="row">
                                                            <div class="col-sm-12">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Address</label>
                                                                        <textarea rows="5"  name="contact_address" id="contact_address" class="form-control"><?= $settingDetails->contact_address; ?></textarea>
                                                               </div>
                                                            </div>
                                                    </div>
                                                    <div class="row">
                                                            <div class="col-sm-6">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Email Address</label>
                                                                       <input type="text" name="contact_email_address" id="contact_email_address" class="form-control" value="<?= $settingDetails->contact_email_address; ?>">
                                                               </div>
                                                            </div>
                                                              <div class="col-sm-6">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Phone Number</label>
                                                                       <input type="text" name="contact_phone" id="contact_phone" class="form-control" value="<?= $settingDetails->contact_phone; ?>">
                                                               </div>
                                                            </div>
                                                    </div>
                                                    
                                            </div>
                                        </div>
                                </div>
           </div>
           
           <div class="row">
                                <div class="form-actions">
                                                        
                                                            <div class="text-center">
                                                                <input class="btn green" type="submit" name="submit" id="submit" value="Submit">
                                                                <input class="btn default" type="reset" name="reset" id="reset" value="Cancel">
                                                            </div>
                                                        
                                                    </div>
                                 </div>
     </form>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
     <!-- END PAGE BASE CONTENT -->
 </div>
 </div>
<script>
    $(document).ready(function(e) {
		
		$('#contact_page_details').validate({
					ignore: [],
					errorClass:'<?php echo A_JQUERY_VALIDATION_ERROR_CLASS_FULL; ?>',
					validClass:'<?php echo A_JQUERY_VALIDATION_SUCCESS; ?>',
					rules: {
						contact_address : {required:true},
						contact_email_address : {required:true,email:true},
						contact_phone : {required:true},
						contact_longitude : {required:true},
						contact_latitude : {required:true}
					},
					messages : {
						contact_address:{required:"<?php echo "Please Enter The Address"; ?>"},
						contact_email_address : {required:"<?php echo "Please Enter An Email Address" ?>",email:"<?php echo "Please Enter Valid Email Address"; ?>"},
						contact_phone : {required:"<?php echo "Please Enter The Phone Number" ?>"},
						contact_longitude : {required:"<?php echo "Please Enter The Longitude" ?>"},
						contact_latitude : {requird:"<?php echo "Please Enter The Latitude" ?>"}
					}
				});
    });
    </script>