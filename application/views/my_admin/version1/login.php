<body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="<?= ADMIN_URL; ?>">
                <img src="<?= base_url('uploads/orange-logo.png'); ?>" alt="<?= SITE_DISPNAME; ?>" style="height: 150px;" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
        	<?php if($message_notification = $this->session->flashdata('message_notification')) { ?>
            <!-- Message Notification Start -->
     		<div id="message_notification">
			<div class="alert alert-<?= $this->session->flashdata('class'); ?>">    
            	<button class="close" data-dismiss="alert" type="button">×</button>
				<strong>
					<?= $this->session->flashdata('message_notification'); ?> 
				</strong>
			</div>
            </div>
            <!-- Message Notification End -->
            <?php } ?>
            <!-- BEGIN LOGIN FORM -->
            <?= form_open(ADMIN_DIR.'/login/login', array("class"=>"login-form","name"=>"admin-login-form","id"=>"admin-login-form","method"=>"post")); ?>
                <h3 class="form-title font-green">Sign In</h3>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9" for="Username">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="adminUserName" id="adminUserName" /> 
                    <?php echo form_error('adminUserName', '<label class="error">', '</label>'); ?>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9" for="adminPassword">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="adminPassword" id="adminPassword" />
                <?php echo form_error('adminPassword', '<label class="error">', '</label>'); ?>    
                </div>
                <div class="form-actions">
                    <input type="submit" class="btn green uppercase" name="submit" id="submit" value="Login">
                    <!--<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>-->
                </div>
              
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="<?= base_url(ADMIN_DIR.'/login/forgot_password'); ?>" method="post">
                <h3 class="font-green">Forget Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn green btn-outline">Back</button>
                    <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
