<div class="page-content-col">
     <!-- BEGIN PAGE BASE CONTENT -->
     <form name="edit_event" id="edit_event" method="post" action="<?= base_url(ADMIN_DIR.'/events/update_event/'); ?>" enctype="multipart/form-data">
           <div class="row">
                                <div class="col-sm-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-speech font-green"></i>
                                                    <span class="caption-subject bold font-green uppercase">Basic Info</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                    <div class="row">
                                                            <div class="col-sm-12">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Event Name <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                        <input type="text" placeholder="Live Bands" name="eventName" class="form-control" id="eventName" value="<?= $eventInfo->eventName; ?>"> 
                                                               </div>
                                                            </div>
                                                          
                                                 	</div>
                                                    <div class="row">
                                                     <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="avatar">Event Category <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                <select class="form-control" name="eventCategory" id="eventCategory">
                                                                <option value="Art & Culture" <?= ($eventInfo->eventCategory=='Art & Culture')?'selected':''; ?>>Art & Culture</option>
                                                                <option value="Food" <?= ($eventInfo->eventCategory=='Food')?'selected':''; ?>>Food</option>
                                                                <option value="Entertainment" <?= ($eventInfo->eventCategory=='Entertainment')?'selected':''; ?>>Entertainment</option>
                                                                <option value="Games" <?= ($eventInfo->eventCategory=='Games')?'selected':''; ?>>Games</option>
                                                                <option value="Others" <?= ($eventInfo->eventCategory=='Others')?'selected':''; ?>>Others</option>
                                                                </select>
                                                            </div>
                                                            </div>
                                                    
                                                    <div class="col-sm-6">
                                                    		<div class="form-group">
                                                                <label class="control-label">Event Date <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                <select name="eventDate" id="eventDate" class="form-control">
                                                                    <option value=""></option>
                                                                    <option value="16th December" <?= ($eventInfo->eventDate=='16th December')?'selected':''; ?>>16th December</option>
                                                                    <option value="17th December" <?= ($eventInfo->eventDate=='17th December')?'selected':''; ?>>17th December</option>
                                                                    <option value="18th December" <?= ($eventInfo->eventDate=='18th December')?'selected':''; ?>>18th December</option>
                                                                </select>
                                                        	</div>
                                                    </div>
                                                      </div>
                                                      <div class="row">
                                                            <div class="col-sm-12">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Event Location <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                        <input type="text" placeholder="Event Location" name="location" class="form-control" id="location" value="<?= $eventInfo->location; ?>"> 
                                                               </div>
                                                            </div>
                                                          
                                                 	</div>

                                                     <div class="row">
                                                            <div class="col-sm-12">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Event Direction <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                        <input type="text" placeholder="Google Map Address Goes Here..." name="direction" class="form-control" id="direction" value="<?= $eventInfo->direction; ?>"> 
                                                               </div>
                                                            </div>
                                                          
                                                 	</div>

                                                     <div class="row">
                                                     <div class="col-sm-2">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Event Start Time <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                    <input type="text" value="<?= $eventInfo->eventStartTime; ?>" name="eventStartTime" id="eventStartTime" data-format="hh:mm A" class="form-control clockface_1 clockface-open"> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Event End Time <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                    <input type="text" value="<?= $eventInfo->eventEndTime; ?>" name="eventEndTime" id="eventEndTime" data-format="hh:mm A" class="form-control clockface_1 clockface-open"> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Appearance</label>
                                                                        <input type="text" placeholder="Benny Dayal" name="appearance" class="form-control" id="appearance" value="<?= $eventInfo->appearance; ?>"> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Event Capacity <span class="<?= A_JQUERY_VALIDATION_ERROR_CLASS_FULL ?>">(*)</span></label>
                                                                        <input type="text" placeholder="15000" name="eventCapacity" class="form-control" id="eventCapacity" value="<?= $eventInfo->eventCapacity; ?>"> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Status</label>
                                                                    <select name="status" id="status" class="form-control">
                                                                    <option value=""></option>
                                                                    <option value="Active" <?= ($eventInfo->status=='Active')?'selected':''; ?>>Active</option>
                                                                    <option value="Cancelled" <?= ($eventInfo->status=='Cancelled')?'selected':''; ?>>Cancelled</option>
                                                                    <option value="Completed" <?= ($eventInfo->status=='Completed')?'selected':''; ?>>Completed</option>
                                                                </select> 
                                                               </div>
                                                            </div>
                                                          
                                                 	</div>
                                                     <div class="row">
                                                            <div class="col-sm-2">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Is Farmers Event ?</label>
                                                                    <select name="isFarmerEvents" class="form-control" id="isFarmerEvents">
                                                                    <option value=""></option>
                                                                    <option value="Yes" <?= ($eventInfo->isFarmerEvents=='Yes')?'selected':''; ?>>Yes</option>
                                                                    <option value="No" <?= ($eventInfo->isFarmerEvents=='No')?'selected':''; ?>>No</option>
                                                                    </select> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Is Link To Book My Show ?</label>
                                                                    <select name="isBookMyShow" class="form-control" id="isBookMyShow">
                                                                    <option value=""></option>
                                                                    <option value="Yes" <?= ($eventInfo->isBookMyShow=='Yes')?'selected':''; ?>>Yes</option>
                                                                    <option value="No" <?= ($eventInfo->isBookMyShow=='No')?'selected':''; ?>>No</option>
                                                                    </select> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Book My Show Link</label>
                                                                        <input type="text" placeholder="https://www.bookmyshow.com" <?= ($eventInfo->bookMyShowLink=='')?'disabled':''; ?> name="bookMyShowLink" class="form-control" id="bookMyShowLink" value="<?= $eventInfo->bookMyShowLink; ?>"> 
                                                               </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                            	<div class="form-group">
                                                                    <label class="control-label">Remarks </label>
                                                                    <textarea name="remarks" id="remarks" class="form-control"><?= $eventInfo->remarks; ?></textarea> 
                                                               </div>
                                                            </div>
                                                 	</div>
                                            </div>
                                        </div>
                                </div>
           </div>
     
           <div class="row">
                                <div class="form-actions">
                                                        
                                                            <div class="text-center">
                                                                <input type="hidden" name="id" id="id" value="<?= $eventInfo->id; ?>">
                                                                <input class="btn green" type="submit" name="submit" id="submit" value="Submit">
                                                               <!--<input class="btn default" type="reset" name="reset" id="reset" value="Cancel">-->
                                                            </div>
                                                        
                                                    </div>
                                 </div>
     </form>
     <!-- END PAGE BASE CONTENT -->
 </div>
 </div>
<script>
    $(document).ready(function(e) {
        $('#isBookMyShow').on('change', function(){
            if($('#isBookMyShow').val()=='Yes'){
                $('#bookMyShowLink').attr("disabled", false);
            } 
            else{
                $('#bookMyShowLink').attr("disabled", "disabled");
            }
});

		$('#edit_event').validate({
					ignore: [],
					errorClass:'<?php echo A_JQUERY_VALIDATION_ERROR_CLASS_FULL; ?>',
					validClass:'<?php echo A_JQUERY_VALIDATION_SUCCESS; ?>',
					rules: {
						eventName : {required:true},
						eventCategory : { required:true},
						eventDate :{required:true},
                        eventStartTime : {required:true},
                        eventEndTime: {required:true},
						location : {required:true},
						direction : {required:true},
                        eventCapacity : {required:true,number:true},
                        //remarks : {required:true},
                        bookMyShowLink :{url:true,required:function(element){return $("#isBookMyShow").val()=="Yes";}},
						status : {required:true}
					},
					messages : {
						eventName : {required:"Please Enter Event Name"},
						eventCategory : { required:"Please Enter Event Category"},
						eventDate :{required:"Please Select Event Date"},
                        eventStartTime : {required:"Please Enter Event Start Time"},
                        eventEndTime : {required:"Please Enter Event End Time"},
						location : {required:"Please Enter Event Location"},
						direction : {required:"Please Enter Event Direction"},
                        eventCapacity : {required:"Please Enter Event Capacity",number:"Please Enter Valid Capacity"},
                        bookMyShowLink : {url:"Please Enter Valid Book My Show Link",required:"Please Enter Book My Show Link"},
                       // remarks : {required:"Please Enter Remarks Of Event"},
						status : {required:"Please Select Event Status"}
					}
				});
    });
    </script>
    <script src="<?= base_url('assets/library/ckeditor/ckeditor.js'); ?>"></script>
	<script src="<?= base_url('assets/library/ckeditor/adapters/jquery.js'); ?>"></script>