</div>
        <div class="copyright">
                <p class="copyright"> <?= date('Y'); ?> &copy; <?= SITE_DISPNAME; ?></p>
                <p class="text-center copyright"> Powered by <a target="_blank" href="https://www.togglehead.in" title="Togglehead">Togglehead</a></p>
        </div>

        <!--[if lt IE 9]>
        <script src="<?= base_url('theme/admin/assets/global/plugins/respond.min.js'); ?>"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/excanvas.min.js'); ?>"></script> 
        <script src="<?= base_url('theme/assets/global/plugins/ie8.fix.min.js'); ?>"></script> 
        <![endif]-->

        <!-- BEGIN CORE PLUGINS -->
        <script src="<?= base_url('theme/admin/assets/global/plugins/jquery.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/js.cookie.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= base_url('theme/admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/select2/js/select2.full.min.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?= base_url('theme/admin/assets/global/scripts/app.min.js'); ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
       <!-- <script src="<?= base_url('theme/admin/assets/pages/scripts/login.min.js'); ?>" type="text/javascript"></script>-->
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->

		<script>
		$(document).ready(function(e) {
			$('#admin-login-form').validate({
						rules: {
							adminUserName:{required:true},
							adminPassword:{required:true}
						},
						messages : {
							adminUserName : {required: "Please Enter The User Name"},
							adminPassword : {required: "Please Enter The Password"}	
						}
					});
		});
    </script>
    </body>

</html>