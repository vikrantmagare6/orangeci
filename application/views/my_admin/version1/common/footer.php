 <!-- BEGIN FOOTER -->
                <p class="copyright"> <?= date('Y'); ?> &copy; <?= SITE_DISPNAME; ?></p>
                <p class="text-center copyright"> Powered by <a target="_blank" href="https://www.togglehead.in" title="Togglehead">Togglehead</a></p>
                <!-- END FOOTER -->
            </div>
        </div>
        <!-- END CONTAINER -->
        
        <!--[if lt IE 9]>
        <script src="<?= base_url('theme/admin/assets/global/plugins/respond.min.js'); ?>"></script>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
        <script src="<?= base_url('theme/admin/assets/global/plugins/respond.min.js'); ?>"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/excanvas.min.js'); ?> "></script> 
        <script src="<?= base_url('theme/admin/assets/global/plugins/ie8.fix.min.js'); ?>"></script> 
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?= base_url('theme/admin/assets/global/plugins/js.cookie.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/jquery.blockui.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= base_url('theme/admin/assets/global/plugins/moment.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/clockface/js/clockface.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?= base_url('theme/admin/assets/global/scripts/app.js'); ?>" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url('theme/admin/assets/pages/scripts/components-date-time-pickers.min.js'); ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?= base_url('theme/admin/assets/layouts/layout/scripts/layout.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/layouts/layout/scripts/demo.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/layouts/global/scripts/quick-sidebar.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/layouts/global/scripts/quick-nav.min.js'); ?>" type="text/javascript"></script>
        <!-- BEGIN DATATABLE PLUGINS -->
		<script src="<?= base_url('theme/admin/assets/global/scripts/datatable.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/datatables/datatables.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'); ?>" type="text/javascript"></script>
        <!--END DATATABLE PLUGINS-->


    </body>

</html>