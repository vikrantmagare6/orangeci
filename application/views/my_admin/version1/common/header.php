<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title><?php echo (isset($title))?$title:ADMIN_PAGE_TITLE; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="<?php echo (isset($meta_description))?$meta_description:ADMIN_META_DESCRIPTION; ?>" name="description" />
        <meta content="<?php echo (isset($meta_author))?$meta_author:ADMIN_META_AUTHOR; ?>" name="author" />
        <meta content="<?php echo (isset($meta_keywords))?$meta_keywords:ADMIN_META_KEYWORDS; ?>" name="keywords" />
        
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('theme/admin/assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('theme/admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('theme/admin/assets/global/plugins/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('theme/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
         
         <!--BEGIN DATE, TIME, CLOCK, DATE RANGE PICKER PLUGINS-->
         <link href="<?= base_url('theme/admin/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css');?>" rel="stylesheet" type="text/css" />
         <link href="<?= base_url('theme/admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('theme/admin/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('theme/admin/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('theme/admin/assets/global/plugins/clockface/css/clockface.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('theme/admin/assets/global/plugins/morris/morris.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('theme/admin/assets/global/plugins/fullcalendar/fullcalendar.min.css');?>" rel="stylesheet" type="text/css" />
        <!--END DATE, TIME, CLOCK, DATE RANGE PICKER PLUGINS-->
        
        <!-- BEGIN DATATABLE PLUGINS -->
        <link href="<?= base_url('theme/admin/assets/global/plugins/datatables/datatables.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('theme/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END DATATABLE PLUGINS -->

        
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?= base_url('theme/admin/assets/global/css/components.min.css');?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?= base_url('theme/admin/assets/global/css/plugins.min.css');?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?= base_url('theme/admin/assets/layouts/layout5/css/layout.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('theme/admin/assets/layouts/layout5/css/custom.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        
		<!--BEGIN JQUERY VALIDATION PLUGIN-->
		<script src="<?= base_url('theme/admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
        <script src="<?= base_url('theme/admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js'); ?>" type="text/javascript"></script>
        <!--END JQUERY VALIDATION PLUGIN-->
        
        <script>
        $(document).ready(function(e) {
            jQuery.validator.addMethod("fileSize", function (val, element) {
		 if(typeof element.files[0]=== "undefined")
		 {
			 return true;
		 }
		 else
		 {
          	var size = element.files[0].size;
            console.log(size);
			if (size > 2048576)// checks the file more than 1 MB
			   {
					return false;
			   } else {
				   return true;
			   }
		 }
	
      }, "Maximum 2MB Image Size Allowed");
  
    $('.history-back').click(function(){
			window.history.back();
		});	
		   
	    });
		
	
   
        </script>
        
        <link rel="shortcut icon" href="<?= base_url('uploads/favicon.png');?>" /> </head>
    <!-- END HEAD -->
    
        <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
            <!-- BEGIN HEADER -->
            <header class="page-header">
                <nav class="navbar mega-menu" role="navigation">
                    <div class="container-fluid">
                        <div class="clearfix navbar-fixed-top">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="toggle-icon">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </span>
                            </button>
                            <!-- End Toggle Button -->
                            <!-- BEGIN LOGO -->
                            <a id="index" class="page-logo" href="<?= ADMIN_URL; ?>">
                                <img src="<?= base_url('uploads/orange-logo.png'); ?>" alt="<?= SITE_DISPNAME; ?>"> </a>
                            <!-- END LOGO -->
                            
                            <!-- BEGIN TOPBAR ACTIONS -->
                            <div class="topbar-actions">
                                
                               
                                <!-- BEGIN USER PROFILE -->
                                <div class="btn-group-img btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <span>Hi, <?= ucfirst($adminProfileInfo->fname); ?></span>
                                        <img src="<?= base_url('uploads/admin/thumb/'.$adminProfileInfo->avatar); ?>" alt="<?= ucfirst($adminProfileInfo->fname).' '.ucfirst($adminProfileInfo->lname); ?>"> </button>
                                    <ul class="dropdown-menu-v2" role="menu">
                                        <li>
                                            <a href="<?= ADMIN_URL.'profile'; ?>">
                                                <i class="icon-user"></i> My Profile
                                            </a>
                                        </li>
                                        
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="<?= ADMIN_URL.'logout'; ?>">
                                                <i class="fa fa-power-off"></i> Log Out </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- END USER PROFILE -->
                                
                            </div>
                            <!-- END TOPBAR ACTIONS -->
                        </div>
                        <!-- BEGIN HEADER MENU -->
                         <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">
                                <li class="dropdown more-dropdown <?= ($module_heading=='Add Event' or $module_heading=='Edit Event' or $module_heading=='Events List')?'active selected':''; ?>">
                                    <a href="javascript:;" class="text-uppercase"> Events </a>
                                    <ul class="dropdown-menu">
                                        <li <?= ($module_heading=='Add Event')?'class="active"':''; ?>>
                                            <a href="<?= base_url(ADMIN_DIR.'/events/add'); ?>"> <i class="fa fa-plus"></i> Add Event</a>
                                        </li>
                                        <li <?= ($module_heading=='Events List')?'class="active"':''; ?>>
                                            <a href="<?= base_url(ADMIN_DIR.'/events/lists'); ?>"> <i class="fa fa-list"></i> Events List</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="<?= ($module_heading=='Registered Events List')?'active selected':''; ?>">
                                    <a href="<?= base_url(ADMIN_DIR.'/registered/lists'); ?>" class="text-uppercase"> Registered Event </a>
                                </li>
                            </ul>
                        </div>
                        <!-- END HEADER MENU -->
                    </div>
                    <!--/container-->
                </nav>
            </header>
            <!-- END HEADER -->
            <div class="container-fluid">
                <div class="page-content" style="padding-top:30px;" >
                    <!-- BEGIN BREADCRUMBS -->
                    <div class="breadcrumbs">
                        <h1><?= $module_heading; ?></h1>
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?= base_url(ADMIN_DIR); ?>">Home</a>
                            </li>
                            <li class="active"><?= $module_heading; ?></li>
                        </ol>
                    </div>
                    <!-- END BREADCRUMBS -->
                    <?php if($message_notification = $this->session->flashdata('message_notification')) { ?>
                    <!-- Message Notification Start -->
                    <div id="message_notification">
                    <div class="alert alert-<?= $this->session->flashdata('class'); ?>">    
                        <button class="close" data-dismiss="alert" type="button">×</button>
                        <strong>
                            <?= $this->session->flashdata('message_notification'); ?> 
                        </strong>
                    </div>
                    </div>
                    <!-- Message Notification End -->
                    <?php } ?>