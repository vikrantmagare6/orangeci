<?php foreach($country as $post){ ?>
    <div class="item col-md-4 col-sm-6 ">
    <div class="item-inner">
        <div class="liv">
            <span>live bands</span>
            <img src="<?= base_url('uploads/event/music.png'); ?>">
        </div>
        <ul>
            <li>
                <i class="fa fa-calendar" aria-hidden="true"></i> 9th December 2017
            </li>
            <li>
                <i class="fa fa-clock-o" aria-hidden="true"></i> 10:00 am - 1:00 pm
            </li>
            <li>
                <i class="fa fa-map-marker" aria-hidden="true"></i> location
            </li>
            <li>
                <i class="fa fa-map-o" aria-hidden="true"></i> Get Direction
            </li>
        </ul>
        <a href="registration.php">register now</a>
    </div>
</div>
<?php } ?>