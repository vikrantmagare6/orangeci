<div class="body-content thanks-image">
        <!-- Main Div Start -->
        <div class="thanks-box">
            <h3>Registration Closed</h3>
            <div class="bg-box">
                <div class="box-txt">
                    <div>
                        <img src="<?= base_url('uploads/sorry.png'); ?>" class="img-responsive" />
                    </div>
                    <p>Oh no! You missed a seat for this event! Worry not. You can avail passes at the venue itself or at the nearest Lokmat office.</p>
                    <div class="button-container">
                        <a href="<?= base_url(); ?>" class="button-class">Back to Homepage</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Div End -->
    </div>