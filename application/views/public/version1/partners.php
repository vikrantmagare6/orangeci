<div class="body-content">
<div class="active-banner">
    <h3>Partners</h3>
</div>
<div class="main-box">
    <!-- Heading Para section start-->
    <div class="container main-partner-new">
        <div class="row">
            <div class="col-md-12">
                <div class="content-wrapper">
                    <h4>World Orange Fest is a culmination of the efforts each of our Partners</h4>
                    <p>The initiative of the World Orange Festival is the result of great minds with great ideas, coming together to collaborate for a great cause. The inititative would not have been possible without the tireless efforts of all involved. </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Heading Para section End-->
    <div class="container-fluid wrapper-box-partners">
        <div class="row">
            <div class="col-md-12">
                <div class="gallery-tab">
                    <ul class="tabs">
                        <li class="tab-menu active" rel="tab1">
                            <a id="upl" href="javascript:void(0);">
                             <img src="<?= base_url('uploads/partners/upl-logo.jpg'); ?>">
                             <!-- <span class="tab-title">UPL</span> -->
                              <img src="<?= base_url('uploads/partners/down.png'); ?>" class="hover-img">
                            </a>
                        </li>
                        <li class="tab-menu" rel="tab3">
                            <a id="bajaj" href="javascript:void(0);">
                             <img src="<?= base_url('uploads/partners/bajaj-logo.jpg'); ?>">
                             <!-- <span class="tab-title">Bajaj</span> -->
                             <img src="<?= base_url('uploads/partners/down.png'); ?>" class="hover-img">
                           </a>
                        </li>
                        <li class="tab-menu" rel="tab2">
                            <a id="mt" href="javascript:void(0);">
                             <img src="<?= base_url('uploads/partners/mtdc-logo.jpg'); ?>">
                             <!-- <span class="tab-title">Maharashtra Tourism</span> -->
                             <img src="<?= base_url('uploads/partners/down.png'); ?>" class="hover-img">
                           </a>
                        </li>
                        <li class="tab-menu" rel="tab4">
                            <a id="coco" href="javascript:void(0);">
                             <img src="<?= base_url('uploads/partners/min-maid.jpg'); ?>">
                             <!-- <span class="tab-title">Camlin</span> -->
                             <img src="<?= base_url('uploads/partners/down.png'); ?>" class="hover-img">
                           </a>
                        </li>
                    </ul>
                    <div class="tab_container">
                        <h3 class="d_active tab_drawer_heading2" rel="tab1">
                        <span><img src="<?= base_url('uploads/partners/upl-logo.jpg'); ?>"></span>
                         </h3>
                        <!-- #UPL -->
                        <div id="tab1" class="tab_content">
                            <div class="upcl-tab">
                                <img src="<?= base_url('uploads/partners/partners-page-img.jpg'); ?>" class="img-responsive side-img" alt="" />
                                <p>UPL's actions today are driven by the food needs of the future. As global food demand is expected to double by 2050, we are leading the way with innovative products and services that are helping make agriculture sustainable and food security achievable. </p>
                                <p>Putting the ‘Farmer First’ in all our initiatives, we provide pre-sowing to post-harvest crop care technologies that include seeds, crop protection, plant health and post-harvest solutions. As a global Indian company with presence in 124 countries, we have disbursed $2.5 billion worth of total crop solutions to the farming community across the world. We deeply believe in innovation for change and pursue R&D and testing. Today, we have over 1344 products with 388 global product patents filed.</p>
                                <p>Our group company, Advanta Seeds delivers superior and proprietary germplasm and added value traits. ZEBA, our soil and water technology helps manage the same throughout the crop life cycle. </p>
                                <p>Going a step further to minimise post harvest losses and food wastage, we have DECCO range of speciality products for fruit coating and storage. We provide crop protection solutions and key technologies for a wide range of crops like soybean, corn, rice, wheat, pulses, fruits and vegetables and also plantation crops like banana, oil, palm and sugarcane. We also take pride in working towards reducing food losses with our comprehensive technologies for food grain storage and potato storage.</p>
                                <p>Making farmers resilient to uncertainties is of foremost importance in our mission to provide food to the world. Our farmer engagement initiatives aim to educate, assist and address issues of the farmer. Adarsh Kisan Centre (AKC) is a toll-free call centre help, Trust++ and Adarsh Farm Services work to reach out for assistance in aerial spraying and labour-related issues, while UNIMART provides farmers with farm products and technical guidance.</p>
                                <div class="about-deco">
                                    <h3>ABOUT DECCO</h3>
                                    <p>For 50 years growers and packers have trusted Decco with the fruit of their labor. And Decco has come through with the quality post harvest products, equipment and services, helping process countless billions of citrus, apples, stone fruits, potatoes and many others... treating each piece like it was the most important one ever grown.</p>

                                    <p>Whether you are packing citrus, apples or other fruits and vegetables, the Decco post harvest solutions provides you with coatings, cleaners, sanitizers, fungicides, growth regulators and antiscald. In several countries Decco has also developed equipment solutions with teams of mechanical representatives available to assist you with your packing needs.</p>

                                    <p>The Decco quality does not end with the product sale. We back all of our treatments with the most knowledgeable, most experienced technical service teams in the industry. We also provide assistance with residue analysis and monitoring of fungicide residues, tank concentrations and weight loss studies to spore assays, resistance management and IPM programs.</p>

                                    <p>At our Decco manufacturing and research facilities in the United States, Spain, Italy, Israel and India, we constantly strive to improving our formulations, products and equipment services. In our produce rooms we can simulate all type of conditions that may exist in packing and storage areas.</p>
                                </div>
                                <div class="fruit">
                                    <h3>Fruit Coating</h3>
                                    <p>Every 3rd fruit in the world is coated with Decco technology to ensure freshness. Providing safe and approved coating products for fruits like oranges, apples, mangoes, kinnows and more to increase their shelf-life by 25-30 days.</p>
                                </div>
                                <div id="video-gallery">
                                 <a href="https://www.youtube.com/watch?v=X5lICKXjHI4" frameborder="0" allowfullscreen class="video"><img src="<?= base_url('uploads/partners/partners-page-video2.jpg'); ?>" class="img-responsive" alt="" /></a>
                             </div>
                            </div>
                        </div>
                        <!-- #Bajaj -->
                        <h3 class="tab_drawer_heading2" rel="tab3">
                            <span><img src="<?= base_url('uploads/partners/bajaj-logo.jpg'); ?>"></span>
                           </h3>
                        <div id="tab3" class="tab_content">
                            <div class="soon-box">
                            <p>
                            Bajaj Electricals Limited (BEL), a globally renowned and trusted company, with a turnaround of ₹4298 crores (FY 16-17), is a part of "Bajaj Group". Bajaj Electricals business is spread across – Consumer Products (Appliances, Fans, Lighting), Exports, Luminaires and EPC (Illumination, Transmission Towers and Power Distribution). Bajaj Electricals has 19 branch offices spread in different parts of the country besides being supported by a chain of distributors, authorized dealers, retail outlets, exclusive showrooms called ‘Bajaj World’ and approximately 462 customer care centres. We also have a presence in the hi-end range of appliances with brands like Platini and Morphy Richards in India.
                            </p>
                            </div>
                        </div>
                        <!-- #Bajaj -->

                        
                        <!-- #Maharashtra Tourism -->
                        <h3 class="tab_drawer_heading2" rel="tab2">
                            <span><img src="<?= base_url('uploads/partners/mtdc-logo.jpg'); ?>"></span>
                             </h3>
                        <div id="tab2" class="tab_content">
                            <div class="mtdc">
                           <h3>The Maharashtra Tourism Development Corporation (MTDC)  is proud to be the hosting partner for “ The WORLD ORANGE FESTIVAL” .</h3>
                           <br/>
                           <p>Maharashtra,  a land whose sheer size and diversity is stunning , which has mountains that stretch out into the mists as far as the eye can see, has innumerous forts that stand proud and strong has immense opportunities for Tourism. Nagpur, also known as the 'Orange city' of India because of the juiciest varieties of oranges found in its orchards is one of the best tourist destinations in Maharashtra. With temples, lush green gardens, lakes and most definitely connected via main cities it becomes the ideal holiday destination. MTDC has been prudent in promotion of innovative festivals across Maharashtra and the World orange festival at Nagpur is one such festival. MTDC  hopes to generate e a lot of interest from various sections of society for this festival and thus offers a huge market for locals and the tourism sector service providers.</p>
                           <br/>
                           <p><b>MTDC has been weaving festivals around the marvels of Maharashtra to create knowledge about the assortment of destinations amongst national and global tourists and is proud of its association for the Festival.</b></p>
                       </div>
                        </div>
                        <h3 class="tab_drawer_heading2" rel="tab4">
                            <span><img src="<?= base_url('uploads/partners/min-maid.jpg'); ?>"></span>
                            </h3>
                        <div id="tab4" class="tab_content">
                          <div class="soon-box"> <h2 class="soon">COMING SOON</h2></div>
                        </div>
                    </div>
                    <!-- .tab_container -->
                </div>
            </div>
        </div>
    </div>
    <!-- Lokmat Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="lokmat">
                    <h4>An Initiative by</h4>
                    <img src="<?= base_url('uploads/partners/lokmat.png'); ?>" alt="" class="img-responsive lokmat-logo" />
                    <p>Lokmat Media Pvt. Ltd. is a leading multi platform media company with interests in a diversified portfolio of publishing, broadcast, digital, entertainment, community and sports verticals. It has an employee strength of more than 3,000 and has a pan-India network of offices.</p>
                </div>
            </div>
        </div>
    </div>


            <!-- logo div -->

            <div class="container-fluid logosrowsdivparent">
                <div class="logosrows clearfix">
                     <div class="logosrowsdiv">
                            <h4>Art Partner</h4>
                            <img src="<?= base_url('uploads/partners/camlin.jpg'); ?>" alt="" class="img-responsive timesnow-logo" />
                        </div>
                        <div class="logosrowsdiv">
                            <h4>Supported by</h4>
                            <img src="<?= base_url('uploads/partners/gom.jpg'); ?>" alt="" class="img-responsive gom-logo" />
                        </div>
                        <div class="logosrowsdiv">
                            <h4>Style Partner</h4>
                            <img src="<?= base_url('uploads/partners/integriti.jpg'); ?>" alt="" class="img-responsive integriti-logo" />
                        </div>
                        <div class="logosrowsdiv">
                            <h4>Design Partner</h4>
                            <img src="<?= base_url('uploads/partners/jj.jpg'); ?>" alt="" class="img-responsive jj-logo" />
                        </div>
                       
                </div>
                <div class="logosrows clearfix">
                     <div class="logosrowsdiv">
                            <h4>Knowledge Partner</h4>
                            <a alt="Knowledge Partners" target="_blank" href="http://www.ccringp.org.in/"><img src="<?= base_url('uploads/partners/icar.jpg'); ?>" alt="" class="img-responsive icar-logo" /></a>
                        </div>
                        <div class="logosrowsdiv">
                            <h4>Knowledge Partner</h4>
                            <img src="<?= base_url('uploads/partners/ogai.jpg'); ?>" alt="" class="img-responsive ogai-logo" />
                        </div>
                        <div class="logosrowsdiv">
                            <h4>Knowledge Partner</h4>
                            <img src="<?= base_url('uploads/partners/drpanjabrao.jpg'); ?>" alt="" class="img-responsive drpanjabrao-logo" />
                        </div>
                        <div class="logosrowsdiv">
                            <h4>Knowledge Partner</h4>
                            <img src="<?= base_url('uploads/partners/doa.jpg'); ?>" alt="" class="img-responsive doa-logo" />
                        </div>
                        
                </div>

                <div class="logosrows clearfix">
                        <div class="logosrowsdiv">
                            <h4>Radio Partner</h4>
                            <img src="<?= base_url('uploads/partners/radiocity.jpg'); ?>" alt="" class="img-responsive radiocity-logo" />
                        </div>
                        <div class="logosrowsdiv">
                            <h4>Media Partner</h4>
                            <img src="<?= base_url('uploads/partners/timesnow.jpg'); ?>" alt="" class="img-responsive timesnow-logo" />
                        </div>
                        <div class="logosrowsdiv">
                            <h4>Ambient Media Partner</h4>
                            <img src="<?= base_url('uploads/partners/khushi.jpg'); ?>" alt="" class="img-responsive khushi-logo" />
                        </div>
                        <div class="logosrowsdiv">
                            <h4>Games Partner</h4>
                            <img src="<?= base_url('uploads/partners/nickelodeon.jpg'); ?>" alt="" class="img-responsive nickelodeon-logo" />
                        </div>
                        <div class="logosrowsdiv">
                            <h4>Conceptualized by</h4>
                            <img src="<?= base_url('uploads/partners/taplight.jpg'); ?>" alt="" class="img-responsive taplight-logo" />
                        </div>
                </div>
                <div class="logosrows clearfix">
                        
                       
                </div>

            </div>

            <!-- logo div end -->


</div>
</div>