<!-- Main Wrapper Div Start -->
<div class="body-content background-image">
        <!-- Main Div Start -->
        <div class="block-register">
            <div class="outer-valign">
                <div class="inner-valign">
                    <h3>Registration</h3>
                    <div class="full-box">
                        <div class="box-txt">
                            <p>Grab your seat, fastened the seat belt and sit tight as you are about to sign up for the most exciting and juiciest event in the town!</p>
                            <!-- Form Start -->
                            <div class="form-section">
                                    <form id="register"  class="common-form" enctype="multipart/form-data" action="<?= base_url('event/submit'); ?>" method="post">
                                      <div class="clone-div">
                                        <div class="col-xs-12 col-sm-12 col-md-6 filed-box">
                                            <span><i class="fa fa-user" aria-hidden="true"></i></span>
                                            <input type="text" name="name[]" placeholder="Full Name*" class="common-inputfd">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 filed-box">
                                          <span><i class="fa fa-phone" aria-hidden="true"></i></span>
                                            <input type="text" name="phone[]" placeholder="Mobile No*" class="common-inputfd">
                                           <a href="javascript:void(0);" class="clone"> <span class="float"><i class="fa fa-plus-circle plus" aria-hidden="true"></i></span></a>
                                        </div>
                                      </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 filed-box">
                                          <span class="last-ico"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                            <input type="text" name="email" placeholder="Email ID*" class="common-inputfd last">
                                        </div>
                                        <input type="hidden" name="eventId" value="<?= $eventId; ?>">
                                        <input type="hidden" name="eventName" value="<?= $eventName; ?>">
                                        <input type="hidden" name="redirectURL" value="<?= $redirectURL; ?>">
                                        <div class="col-md-12 text-center">
                                        <button class="submit-btn" type="submit">Register Now
                                        </button>
                                    </div>
                                    </form>
                                <!-- Form Start -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Div End -->
    </div>
    <!-- Main Wrapper Div End -->