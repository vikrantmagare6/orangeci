<footer class="orangesuperfooter">
<div class="oranfooter clearfix">
    <div class="oranfooter1"><a  target="_blank" href="https://www.facebook.com/WorldOrangeFestival"><i class="oransocialfoot fa fa-facebook" aria-hidden="true"></i></a><a  target="_blank" href="https://twitter.com/WorldOrangeFest"><i class="oransocialfoot fa fa-twitter" aria-hidden="true"></i></a><a  target="_blank" href="https://www.instagram.com/worldorangefestival/"><i class="oransocialfoot fa fa-instagram" aria-hidden="true"></i></a></div>
    <div class="oranfooter2">@<?= date('Y'); ?> <?= SITE_DISPNAME; ?>. ALL RIGHTS RESERVED. CRAFTED BY <a href="http://www.togglehead.in" target="_blank" class="togglelinks">TOGGLEHEAD</a></div>
</div>
</footer>
<script src="<?= base_url('theme/front/js/main.min.js'); ?>"></script>
<script src="<?= base_url('theme/front/js/jquery.validate.js'); ?>"></script>
<script src="<?= base_url('theme/front/js/lib/lightgallery.min.js'); ?>"></script>
<script src="<?= base_url('theme/front/js/lib/lg-video.min.js'); ?>"></script>
<?php if($module_name=='Venue') { ?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmGptseMG6ldG2_Qu_TuazaeDyX5h4JRA&callback=initMap"></script>
<?php } ?>
<script>
 $(document).ready(function() {
        $("#photos").lightGallery({
            selector: 'a'
        });
        $('#video-gallery').lightGallery({
            selector: 'a',
            loadYoutubeThumbnail: true,
            youtubeThumbSize: 'default',
            loadVimeoThumbnail: true,
            vimeoThumbSize: 'thumbnail_medium',
        });

    });
</script>
<?php if($module_name=='Event Schedule' or $module_name=='Farmers First') { ?>
    <script>
function change_event_date(value) {
   location.href = '<?= base_url('event_schedule/?'); ?>eventDate='+value;
}

function change_event_date_farmers(value) {
   location.href = '<?= base_url('farmers-first/?'); ?>eventDate='+value;
}

var action = 'inactive';
var limit = 9;
var card_height = $('#load_data .item').eq(0).height() ;
var document_height = $(document).height();
$(document).ready(function(){
var start = parseInt($('#start_val').val());
});
function load_events()
 {
 eventDate = '<?= (isset($_GET['eventDate']) and $_GET['eventDate']!='')?$_GET['eventDate']:''; ?>';
 start = parseInt($('#start_val').val());
 limit = 9;
 $('#load_more_button').html('');
 $('#load_more_button').html('Loading...');
  $.ajax({
   url:"<?= base_url('event/load_more'); ?>",
   method:"POST",
   data:{limit:limit, start:start,eventDate:eventDate},
   cache:false,
   success:function(data)
   {
    $('#load_data').append(data);
    if(data == '')
    {
     $('#load_data_message').html("<a href='javascript:void(0);' id='load_more_button' class='loadmore'>No More Event Found</a>");
     action = 'active';
    }
    else
    {
     $('#load_data_message').html("<a href='javascript:void(0);' onclick='load_more();' id='load_more_button' class='loadmore'>Load More</a>");
        action = "inactive";
        start = $('#start_val').val();
        var new_start = parseInt(start)+9;
        $('#start_val').val(new_start);
        $('html, body').animate({
            scrollTop: (document_height - card_height) 
        }, 200);
        document_height = $(document).height();

    }
   }
  });
 }
function load_more()
 {
    action = 'active';
    setTimeout(function(){
     load_events();
    }, 1000);
 }
 
 function load_events_farmers()
 {
eventDate = '<?= (isset($_GET['eventDate']) and $_GET['eventDate']!='')?$_GET['eventDate']:''; ?>';
 start = parseInt($('#start_val').val());
 limit = 9;
 //eventDate = '<?= (isset($_GET['eventDate']) and $_GET['eventDate']!='')?$_GET['eventDate']:''; ?>';
 $('#load_more_button').html('');
 $('#load_more_button').html('Loading...');
  $.ajax({
   url:"<?= base_url('farmers/load_more'); ?>",
   method:"POST",
   data:{limit:limit, start:start,eventDate:eventDate},
   cache:false,
   success:function(data)
   {
    $('#load_data').append(data);
    if(data == '')
    {
     $('#load_data_message').html("<a href='javascript:void(0);' id='load_more_button' class='loadmore'>No More Event Found</a>");
     action = 'active';
    }
    else
    {
     $('#load_data_message').html("<a href='javascript:void(0);' onclick='load_more_farmers();' id='load_more_button' class='loadmore'>Load More</a>");
        action = "inactive";
        start = $('#start_val').val();
        var new_start = parseInt(start)+9;
        $('#start_val').val(new_start);
        $('html, body').animate({
            scrollTop: (document_height - card_height) 
        }, 200);
        document_height = $(document).height();

    }
   }
  });
 }
function load_more_farmers()
 {
    action = 'active';
    setTimeout(function(){
        load_events_farmers();
    }, 1000);
 }

</script>
<?php } ?>
<?php if($module_name=='Event Registration') { ?>
    <script type="text/javascript">
       _.templateSettings.variable = "element";
var tpl = _.template($("#tan").html());

var counter = 1;

$('.clone').on('click',function(e){
    e.preventDefault();
    var tplData = {
        i: counter,
        p: counter
    };
   
    $(tpl(tplData)).insertAfter($('.clone-div').last());
    counter += 1;

    $('.tesf').each(function () {
        $(this).rules("add", {
            required: true,
            messages: {
                required: "Please Enter Your Name",
                
              }
        });
    });

    $('.teref2').each(function () {
        $(this).rules("add", {
            digits: true,
            maxlength:15,
            minlength: 6,
             messages: {
                 required: "Please Enter Your Phone Number",
                digits: "Please enter only digits",
                
              }
        });
    });
});

$('.delete-clone').on('click',function(e){
    e.preventDefault();
    var tplData = {
        i: counter,
        p: counter
    };

   if (counter > 1) {
        $('.clone-div').last().remove();
        console.log(counter);
        counter -= 1;
   }else {
    console.log('dfsdfsd');
    counter = 1;
   }
});
    </script>
<?php } ?>
</body>

</html>