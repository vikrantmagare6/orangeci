<!DOCTYPE html>
<html lang="en" <?= ($module_name=='Activities')?'class="activityinit"':''; ?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo (isset($module_name))?$module_name.' | '.SITE_DISPNAME:PUBLIC_PAGE_TITLE; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <link rel="icon" type="image/png" href="<?= base_url('uploads/favicon.png'); ?>" />
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="<?= base_url('theme/front/css/styles.min.css'); ?>">

    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1475111469403935');
    fbq('track', 'PageView');
    </script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110934761-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-110934761-1');
    </script>
    
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 998487496;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript"
    src="//www.googleadservices.com/pagead/conversion.js">
    </script>

    <?php if($module_name=='Thank You') { ?>
    <!-- Google Code for World Orange Festival Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 998487496;
    var google_conversion_label = "2QwpCIu5kHoQyOuO3AM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <script type="text/javascript"
    src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <?php } ?>

</head>
<noscript>
<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1475111469403935&ev=PageView&noscript=1"/>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""
src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/998487496/?gu
id=ON&amp;script=0"/>
</div>
<?php if($module_name=='Thank You') { ?>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/998487496/?label=2QwpCIu5kHoQyOuO3AM&amp;guid=ON&amp;script=0"/>
</div>
<?php } ?>
</noscript>

<body <?= ($module_name=='Activities')?'class="activityinit"':''; ?>>
<?php if($module_name=='Thank You') { ?>
<script>
 fbq('track', 'CompleteRegistration');
</script>
<?php } ?>
    <?php if($module_name=='Home') { ?>
        <div class="se-pre-con">
            <div class="wrap">
                <img src="<?= base_url('uploads/splashspinner.gif'); ?>" class="orangesplash">
            </div>
        </div>
    <?php } ?>
    <a class="orregister" href="<?= base_url('event_schedule'); ?>"><span>Register</span></a>
    <div class="full-header">
        <div class="orangecontactparentdiv">
            <div class="orangecontactdiv">
                <span class="mailid">
            <a href="mailto:worldorangefestival@lokmat.com" class="orangecontactanchors"><i class="fa fa-envelope-o" aria-hidden="true"></i> worldorangefestival@lokmat.com</a>
            <div class="gamesparent"><i class="fa fa-hashtag gamesicohead" aria-hidden="true"><a href="<?= base_url('game'); ?>" class="gamesicospan">PLAY</a></i></div>
            </span>
            </div>
        </div>
        <nav class="navparent">
            <div class="nav-bar">
                <div class="module left">
                    <a href="<?= base_url(); ?>"><img class="logoimgsvg" src="<?= base_url('uploads/orange-logo.png'); ?>"></a>
                </div>
                <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
                    <span class="hamburger"></span>
                    <span class="hamburger"></span>
                    <span class="hamburger"></span>
                </div>
                <div class="module-group right">
                    <div class="module left oranmobmenu">
                        <ul class="menu">
                            <li class=""> <a href="<?= base_url('farmers-first'); ?>" <?= ($module_name=='Farmers First')?'class="active"':''; ?>>Farmers First</a>
                            </li>
                            <li class="has-dropdown"> <a href="<?= base_url('activities'); ?>">Activities</a>
                                <ul>
                                    <li class=""> <a href="<?= base_url('activities/?activities=art-and-culture'); ?>" target="">Art & Culture</a> </li>
                                    <li class=""> <a href="<?= base_url('activities/?activities=food'); ?>" target="">Food</a> </li>
                                    <li class=""> <a href="<?= base_url('activities/?activities=entertainement'); ?>" target="">Entertainment</a> </li>
                                    <li class=""> <a href="<?= base_url('activities/?activities=games'); ?>" target="">Games</a> </li>
                                    <li class=""> <a href="<?= base_url('activities/?activities=others'); ?>" target="">Others</a> </li>
                                </ul>
                            </li>
                            <li class=""> <a href="<?= base_url('event_schedule'); ?>" <?= ($module_name=='Event Schedule')?'class="active"':''; ?>> Event Schedule </a> </li>
                            <li class=""> <a href="<?= base_url('venue'); ?>" <?= ($module_name=='Venue')?'class="active"':''; ?>> Venue </a></li>
                            <li class=""> <a href="<?= base_url('partners'); ?>" <?= ($module_name=='Partners')?'class="active"':''; ?>> Partners </a></li>
                            <li class=""> <a href="<?= base_url('gallery'); ?>" <?= ($module_name=='Gallery')?'class="active"':''; ?>> Gallery </a></li>
                            <li class="gamesli"> <a href="<?= base_url('game'); ?>" <?= ($module_name=='Games')?'class="active"':''; ?>> Games </a></li>        
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <div class="pagetoppad">
    </div>