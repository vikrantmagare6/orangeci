<!doctype html>
<!--[if IE 9 ]><html class="ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
<head>
<title><?php echo (isset($page_title))?$page_title.' | '.SITE_DISPNAME:PUBLIC_PAGE_TITLE; ?></title>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta content="<?php echo (isset($meta_description))?$meta_description:PUBLIC_META_DESCRIPTION; ?>" name="description" />
<meta content="<?php echo (isset($meta_author))?$meta_author:PUBLIC_META_AUTHOR; ?>" name="author" />
<meta content="<?php echo (isset($meta_keywords))?$meta_keywords:PUBLIC_META_KEYWORDS; ?>" name="keywords" />
<link rel="manifest" href="site.webmanifest">
<link rel="apple-touch-icon" href="<?= base_url('uploads/favicon.png'); ?>">
<!-- Place favicon.ico in the root directory -->
<link rel="stylesheet" href="<?= base_url('theme/front/css/styles.min.css'); ?>">
<script src="<?= base_url('theme/front/js/main.min.js'); ?>"></script>
</head>
<body <?= ($module_name=='Activities')?'class="activityinit"':''; ?>>
    <a class="orregister" href="<?= base_url('event_schedule'); ?>"><span>Register</span></a>
    <div class="full-header">
        <div class="orangecontactparentdiv">
            <div class="orangecontactdiv">
                <span class="mailid">
            <a href="mailto:info@orangefest.com" class="orangecontactanchors"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@orangefest.com</a>
            </span><span class="mnumber">
            <a href="tel:9920234567" tel:="" class="orangecontactanchors"><i class="fa fa-phone" aria-hidden="true"></i> +91 992 023 4567</a>
            </span>
            </div>
        </div>
        <nav class="navparent">
            <div class="nav-bar">
                <div class="module left">
                    <a href="index.html"><img class="logoimgsvg" src="<?= base_url('uploads/orange-logo.svg'); ?>"></a>
                </div>
                <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
                    <span class="hamburger"></span>
                    <span class="hamburger"></span>
                    <span class="hamburger"></span>
                </div>
                <div class="module-group right">
                    <div class="module left oranmobmenu">
                        <ul class="menu">
                            <li class=""> <a href="javascript:void(0);">Farmers First</a>
                            </li>
                            <li class=" has-dropdown"> <a href="<?= base_url('activities'); ?>" title="Activities">Activities</a>
                                <ul>
                                    <li class=""> <a href="<?= base_url('activities/?name=art-and-culture'); ?>" title="Art & Culture">Art & Culture</a> </li>
                                    <li class=""> <a href="<?= base_url('activities/?name=food'); ?>" title="Food">Food</a> </li>
                                    <li class=""> <a href="<?= base_url('activities/?name=entertainement'); ?>" title="Entertainment">Entertainment</a> </li>
                                    <li class=""> <a href="<?= base_url('activities/?activities=games'); ?>" title="Games">Games</a> </li>
                                </ul>
                            </li>
                            <li class=""> <a href="<?= base_url('event_schedule'); ?>" title="Event Schedule"> Event Schedule </a> </li>
                            <li class=""> <a href="<?= base_url('venue'); ?>" title="Venue"> Venue </a>
                            <li class=""> <a href="<?= base_url('partners'); ?>" title="Partners"> Partners </a>
                                <li class=""> <a href="<?= base_url('gallery'); ?>" title="Gallery"> Gallery </a>
                                </li>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <div class="pagetoppad">
    </div>