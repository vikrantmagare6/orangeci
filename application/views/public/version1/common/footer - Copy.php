<footer class="orangesuperfooter">
        <div class="oranfooter clearfix">
            <div class="oranfooter1"><a href="https://www.facebook.com/WorldOrangeFestival"><i class="oransocialfoot fa fa-facebook" aria-hidden="true"></i></a><a href="https://twitter.com/WorldOrangeFest"><i class="oransocialfoot fa fa-twitter" aria-hidden="true"></i></a><a href="https://www.instagram.com/worldorangefestival/"><i class="oransocialfoot fa fa-instagram" aria-hidden="true"></i></a></div>
            <div class="oranfooter2">@2017 ORANGEFEST. ALL RIGHTS RESERVED. CRAFTED BY <a href="www.togglehead.in" class="togglelinks">TOGGLEHEAD</a></div>
        </div>
    </footer>
</body>

</html>