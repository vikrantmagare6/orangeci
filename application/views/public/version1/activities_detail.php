<div class="body-content">
<div class="active-banner">
    <h3>Activities</h3>
</div>
<div class="main-activebx">
    <img src="<?= base_url('uploads/activities/parachute.png'); ?>" class="side-img3 moving-enemy t1 te">
    <img src="<?= base_url('uploads/activities/fish-animation.gif'); ?>" class="gif-img side-img6 t2 te">
    <img src="<?= base_url('uploads/activities/guitar.gif'); ?>" class="guitar-gif side-img3 t3 te">
    <img src="<?= base_url('uploads/activities/headphones.gif'); ?>" class="headphone-gif side-img1 t4 te">
    <img src="<?= base_url('uploads/activities/pan-pizza-gif.gif'); ?>" class="gif-img side-img2 t5 te">
    <img src="<?= base_url('uploads/activities/Badminton-3.gif'); ?>" class="gif-img side-img5 t6 te">
    <img src="<?= base_url('uploads/activities/Basketball-2.gif'); ?>" class="gif-img side-img4 t7 te">
    <img src="<?= base_url('uploads/activities/hola-hoops.gif'); ?>" class="gif-img side-img3 t8 te">
    <img src="<?= base_url('uploads/activities/Baloon.png'); ?>" class="side-img4 moving-enemy t9 te">
    <img src="<?= base_url('uploads/activities/Firki.gif'); ?>" class="gif-img side-img3 t10 te">
    <img src="<?= base_url('uploads/activities/sun.gif'); ?>" class="side-img1 t11 te">
    <img src="<?= base_url('uploads/activities/lollipop.gif'); ?>" class="headphone-gif side-img3 t12 te">
    <div class="container-fluid wrapper-box">
        <div class="row">
            <div class="col-md-12">
                <div class="activities-tab">
                    <ul class="tabs">
                        <li class="tab-menu active" rel="tab1">
                            <a id="art-and-culture" href="javascript:void(0);">
                             <img src="<?= base_url('uploads/activities/art-icon.png'); ?>">
                             <span class="tab-title">Art &amp; Culture</span>
                              <img src="<?= base_url('uploads/activities/leaf.png'); ?>" class="hover-img">
                            </a>
                        </li>
                        <li class="tab-menu" rel="tab2" id="food">
                            <a id="food" href="javascript:void(0);">
                             <img src="<?= base_url('uploads/activities/food-icon.png'); ?>">
                             <span class="tab-title">Food</span>
                             <img src="<?= base_url('uploads/activities/leaf.png'); ?>" class="hover-img">
                           </a>
                        </li>
                        <li class="tab-menu" rel="tab3">
                            <a id="entertainement" href="javascript:void(0);">
                             <img src="<?= base_url('uploads/activities/entertainment-icon.png'); ?>">
                             <span class="tab-title">Entertainment</span>
                             <img src="<?= base_url('uploads/activities/leaf.png'); ?>" class="hover-img">
                            </a>
                        </li>
                        <li class="tab-menu" rel="tab4">
                            <a id="games" href="javascript:void(0);">
                               <img src="<?= base_url('uploads/activities/games-icon.png'); ?>">
                               <span class="tab-title">Games</span>
                               <img src="<?= base_url('uploads/activities/leaf.png'); ?>" class="hover-img">
                            </a>
                        </li>
                        <li class="tab-menu" rel="tab5">
                            <a id="others" href="javascript:void(0);">
                               <img src="<?= base_url('uploads/activities/Other-icon.png'); ?>">
                               <span class="tab-title">Others</span>
                               <img src="<?= base_url('uploads/activities/leaf.png'); ?>" class="hover-img">
                            </a>
                        </li>
                    </ul>
                    <div class="tab_container">
                        <h3 class="d_active tab_drawer_heading" rel="tab1">
                        <span><img src="<?= base_url('uploads/activities/art-icon.png'); ?>"></span>
                        Art &amp; Culture</h3>
                         <!-- #Art &amp; Culture -->
                        <div id="tab1" class="tab_content">
                            <div class="divTable">
                                <div class="divTableBody">
                                    <div class="divTablesRow">
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Art Workshop</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Learn and expand your artistic side at the hands of experienced teachers and artists.</p>
                                                </div>
                                            </article>
                                        </div>
                                          <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Children's Art Competition </h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>It is time for the kids to showcase their art skills on the canvas of the World Orange Festival. </p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Installations</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Watch the work of the students of JJ School of Arts embellish the entire city of Nagpur!</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Marathi Play- White Lily Night Rider</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>2 single 30 year olds chat online under pseudonyms, but all hell breaks lose when they meet in person. Also interact with the leads after the performance.</p>
                                                </div>
                                            </article>
                                        </div>
                                           <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Hindi Play- Hum Do Humare Woh </h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Swindled by a fortune-teller, a husband begins his hunt to find his wife a husband! Also interact with the leads after their performance.</p>
                                                </div>
                                            </article>
                                        </div>
                                         <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Sufi Singing by Roopmati Jolly and Luke Kenny</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>The Indian VJ, Director, musician and song writer, Luke Kenny comes together with Sufi singer Rupmati Jolly for a night of soul-touching tunes.</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Landscape competition</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Bring out your artistic side by painting everything that inspires you about Nagpur</p>
                                                </div>
                                            </article>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="button-container clearfix">
                                            <a href="<?= base_url('event_schedule'); ?>" class="button-class">View Schedule &amp; Register</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- #Food -->
                        <h3 class="tab_drawer_heading" rel="tab2">
                            <span><img src="<?= base_url('uploads/activities/food-icon.png'); ?>"></span>
                            Food</h3>
                        <div id="tab2" class="tab_content">
                            <div class="divTable">
                                <div class="divTableBody">
                                    <div class="divTablesRow">
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Master Chef - Sarah Todd</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>The model-turned-restaurateur and former Masterchef Australia contestant teaches you some of her world-famous recipes!</p>
                                                </div>
                                            </article>
                                        </div>
                                          <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Mixologist - Shatbi Basu</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>The Cocktail Queen of India and co-founder of STIR- The official Meet for Bartenders, Shatbhi Basu teaches you how to mix some heady cocktails but with tangy oranges!</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Master Chef - Vicky Ratnani</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Chef, author and cooking show host, Vicky Ratnani is a culinary rockstar. Join him as he talks you through his award-winning recipes. </p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="button-container clearfix">
                                            <a href="<?= base_url('event_schedule'); ?>" class="button-class">View Schedule &amp; Register</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- # Entertainment -->
                        <h3 class="tab_drawer_heading" rel="tab3">
                        <span><img src="<?= base_url('uploads/activities/entertainment-icon.png'); ?>"></span>
                        Entertainment</h3>
                        <div id="tab3" class="tab_content">
                            <div class="divTable">
                                <div class="divTableBody">
                                    <div class="divTablesRow">
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>African Acrobats</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>A rare art form, the African Acrobats will perform gravity defying tricks to charged beats. This one is a must for the young and old.</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Antre Dance</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Feathers, frolic and fun! Focus your eyes on the centre stage as the beautiful trio of Antre dancers perform at the World Orange Fest.</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Benny Dayal</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Singer, powerhouse performer, songwriter, music composer and overall entertainer. For the first time in Nagpur, this event is an essential for any music lover.</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Butterfly Stilts</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>A colourfully stunning act, watch these professionals take you to new “heights” of entertainment.</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Can Can International Dancers </h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>The high-energy French dance form performed by Internationally trained dancers, isn’t something you want to miss. </p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Candyfloss Girl </h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Enjoy some sweet treats and art all at the same time as the Candyfloss Girl shows you her unique talents.</p>
                                                </div>
                                            </article>
                                        </div>
                                         <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Consumer Fair and Live Music</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Choose from an array of exclusive World Orange Festival Merchandise and some of the best produce Nagpur has to offer only at the consumer Fair.</p>
                                                </div>
                                            </article>
                                        </div>
                                         <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Cultural Parade</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>A sea of floats will take over the streets of Nagpur, watch as the Orange pride engulfs the entire city</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Cultural / Bollywood performance by Sonali Kulkarni</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Be prepared to be taken on a journey that will enthral your senses as the troop dances to a mix of the biggest Bollywood hits. </p>
                                                </div>
                                            </article>
                                        </div>
                                          <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>EDM</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Let your hair down and dance the night away at this high octane concert which will have your favourite tunes but with an electro twist. </p>
                                                </div>
                                            </article>
                                        </div>
                                         <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Human Fountain</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Truly a sight to behold, watch a fountain come to life only at The World Orange Fest.</p>
                                                </div>
                                            </article>
                                        </div>
                                         <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Human Statue</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Be completely awestruck as a seemingly inanimate object comes to life! The children will love this one.</p>
                                                </div>
                                            </article>
                                        </div>
                                         <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>International Dancers</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>A culmination of art forms from all over the world, prepared to be thoroughly awestruck.</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Latino Dance</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Watch the dancers set the dance floor on fire with their moves that are a combination of salsa, bachata, rumba and cha-cha!</p>
                                                </div>
                                            </article>
                                        </div>
                                         <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Liquid Drum Act</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>An act that will change the way you look at drumming, the liquid drum act is a combination of music, lights, energy and excitement!</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Silver Strings</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>A quartet of international musicians comes together to play you some of your favourite Hindi and English songs. </p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Suleiman - Flutist & DJ Aqeel</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>The 13 year old prodigy hailing from Amritsar was the winner of India’s Got Talent, 2016. He brings his musical stylings to the World Orange Fest stage. </p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Sword Balancing</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>A nail-biting adventure, you will be at the edge of your seat throughout this thrilling performance.</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Vegas / Antre - International Dancers</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>All the way from Sin City, the dance troop will entertain you like never before. You’ll definitely regret missing this one.</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="button-container clearfix">
                                            <a href="<?= base_url('event_schedule'); ?>" class="button-class">View Schedule &amp; Register</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- #Games -->
                        <h3 class="tab_drawer_heading" rel="tab4">
                        <span><img src="<?= base_url('uploads/activities/games-icon.png'); ?>"></span>
                        Games</h3>
                        <div id="tab4" class="tab_content">
                            <div class="divTable">
                                <div class="divTableBody">
                                    <div class="divTablesRow">
                                          <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Angry Oranges</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Play the classic and chart-topping game, with an orange twist!</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Obstacle Races </h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Get your adrenaline pumping with these games that will bring out your competitive side.</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Orange Pong</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>The age-old drinking game gets and orange “twist”. Play, enjoy and win, you don’t want to miss this. </p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Minute Mania / Orange Races</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Participate in Orange-themed games, races and much more and stand to win some exciting prizes! A treat for the whole family. </p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Sumo Wrestling</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Come on down to the Japanese “akhada” and truly enjoy yourself in our fat suits! Cheer on your friends and family as they get down in the ring. </p>
                                                </div>
                                            </article>
                                        </div>
                                       <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="button-container">
                                <a href="<?= base_url('event_schedule'); ?>" class="button-class">View Schedule &amp; Register</a>
                            </div>
                        </div>
                        <!-- #Others -->
                         <h3 class="tab_drawer_heading" rel="tab5">
                        <span><img src="<?= base_url('uploads/activities/Other-icon.png'); ?>"></span>
                        Others</h3>
                        <div id="tab5" class="tab_content">
                            <div class="divTable">
                                <div class="divTableBody">
                                    <div class="divTablesRow">
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>B2B Summit</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>A first-hand opportunity to understand International farming practices, the B2B Summit has a panel discussion session give a wider perspective.</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Citrus Fruit Show</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>The CCRI presents a unique array of engineered oranges that you wouldn't want to miss seeing or tasting! </p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Morning Raga</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Rahul Deshpande with his voice like butter will kickstart your morning with some soothing melodies</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="divTableCell">
                                            <article class="details-block">
                                                <h3>Trade Fair / Exhibition</h3>
                                                <hr/>
                                                <div class="box">
                                                    <p>Oranges from around the world and latest equipment are brought together on one single platform to give the farmers an advanced mastery over their cultivation and trade.</p>
                                                </div>
                                            </article>
                                        </div>
                                        <div class="button-container clearfix">
                                            <a href="<?= base_url('event_schedule'); ?>" class="button-class">View Schedule &amp; Register</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Tab 5 -->
                    </div>
                    <!-- .tab_container -->
                </div>
            </div>
        </div>
    </div>
</div>
</div>