<div class="body-content">
<div class="activitylandingslider">
<section role="page">
  <div class="activitymaincontdiv">
    <h1 class="activitymainheading">Art & Culture</h1>
    <img src="<?= base_url('uploads/activities/leaf.png'); ?>">
    <p class="activitymaincontent">Watch the Orange in all of its glory as it becomes the center of focus of our installations, workshops and murals!</p>
    <a href="<?= base_url('activities/?activities=art-and-culture'); ?>" class="activitybutton-class">Know More</a>
  </div>
    <div class="activityoverlay"></div>      
<img class="activitypagefloatorange" src="<?= base_url('uploads/activities/artandculturelogo.png'); ?>">
</section>
<section role="page">
  <div class="activitymaincontdiv">
    <h1 class="activitymainheading">Food</h1>
    <img src="<?= base_url('uploads/activities/leaf.png'); ?>">
    <p class="activitymaincontent">Learn from the best, as our master chefs talk you through some of their classic recipes, with a twist of Orange.</p>
    <a href="<?= base_url('activities/?activities=food'); ?>" class="activitybutton-class">Know More</a>
  </div>
    <div class="activityoverlay"></div>
<img class="activitypagefloatorange" src="<?= base_url('uploads/activities/foodlogo.png'); ?>">
</section>
<section role="page">
  <div class="activitymaincontdiv">
    <h1 class="activitymainheading">Entertainment</h1>
    <img src="<?= base_url('uploads/activities/leaf.png'); ?>">
    <p class="activitymaincontent">Everything from concerts by the greats to dances and acts await you through these fun-packed 3 days!</p>
    <a href="<?= base_url('activities/?activities=entertainement'); ?>" class="activitybutton-class">Know More</a>
  </div>
    <div class="activityoverlay"></div>
<img class="activitypagefloatorange" src="<?= base_url('uploads/activities/entertainmentlogo.png'); ?>">
</section>
<section role="page">
  <div class="activitymaincontdiv">
    <h1 class="activitymainheading">Games</h1>
    <img src="<?= base_url('uploads/activities/leaf.png'); ?>">
    <p class="activitymaincontent">Orange themed everything! Make sure you participate in these adrenaline pumping games.</p>
    <a href="<?= base_url('activities/?activities=games'); ?>" class="activitybutton-class">Know More</a>
  </div>
    <div class="activityoverlay"></div>
<img class="activitypagefloatorange" src="<?= base_url('uploads/activities/entertainmentlogo.png'); ?>">
</section>
<section role="page" class="lastsection">
  <div class="activitymaincontdiv">
    <h1 class="activitymainheading">Others</h1>
    <img src="<?= base_url('uploads/activities/leaf.png'); ?>">
    <p class="activitymaincontent">A combination of fitness,music and entertainment all come together in the beautiful Orange City!</p>
    <a href="<?= base_url('activities/?activities=others'); ?>" class="activitybutton-class">Know More</a>
  </div>    
    <div class="activityoverlay"></div>    
<img class="activitypagefloatorange" src="<?= base_url('uploads/activities/gameslogo.png'); ?>">
<footer class="orangesuperfooter">
    <div class="oranfooter clearfix">
        <div class="oranfooter1"><a  target="_blank" href="https://www.facebook.com/WorldOrangeFestival"><i class="oransocialfoot fa fa-facebook" aria-hidden="true"></i></a><a  target="_blank" href="https://twitter.com/WorldOrangeFest"><i class="oransocialfoot fa fa-twitter" aria-hidden="true"></i></a><a  target="_blank" href="https://www.instagram.com/worldorangefestival/"><i class="oransocialfoot fa fa-instagram" aria-hidden="true"></i></a></div>
        <div class="oranfooter2">@ <?= date('Y'); ?> <?= SITE_DISPNAME; ?>. ALL RIGHTS RESERVED. CRAFTED BY <a href="http://www.togglehead.in" target="_blank" class="togglelinks">TOGGLEHEAD</a></div>
    </div>
</footer>
</section>
</div>
</div>