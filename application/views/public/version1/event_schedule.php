<div class="body-content">
<div class="active-banner event-schedule">
    <h3>Event schedule</h3>
    <form id="vint" method="get">
        <select name="eventselection" onchange="change_event_date(this.value)" class="eventselection">
            <option selected="selected" value="all">Select Date</option>
            <option <?= ($eventDate==16)?'selected=selected':''; ?> value="16">16 Dec</option>
            <option <?= ($eventDate==17)?'selected=selected':''; ?> value="17">17 Dec</option>
            <option <?= ($eventDate==18)?'selected=selected':''; ?> value="18">18 Dec</option>
        </select>
    </form>
</div>
<div class="event-schedule-content" id="event-schedule-content">
    <img src="<?= base_url('uploads/event/guitar.gif'); ?>" style="width: 150px;" class="evnt1">
    <img src="<?= base_url('uploads/event/saxophone.gif'); ?>" class="evnt2" style="width: 100px;">
    <img src="<?= base_url('uploads/event/pan-pizza-gif.gif'); ?>" class="evnt3" style="width: 100px;">
    <div class="outter-c container-fluid">
        <div class="inner-c row">
        <div id="load_data">
                    <input type="hidden" name="start_val" id="start_val" value="<?= $event_limit; ?>">
                    <?php 
                    $CI =& get_instance();
                    $CI->load->model(PUBLIC_DIR.'/eventPage','event_modal');
                    foreach($events as $event) { 
                        $event_registration_count = $CI->event_modal->registerCount($event->id);
                    switch ($event->eventCategory) {
                        case "Art & Culture":
                            $icon =  "art-and-culture.png";
                            break;
                        case "Games":
                            $icon =  "games.png";
                            break;
                        case "Entertainment":
                            $icon =  "music.png";
                            break;
                        case "Food":
                            $icon = "food.png";
                            break;
                        default:
                            $icon =  "other-icon.png";
                    }    
                    ?>
                        <div class="item col-md-4 col-sm-6">
                            <div class="item-inner">
                                <div class="liv">
                                    <span><?= $event->eventName; ?></span>
                                    <img src="<?= base_url('uploads/event/'.$icon); ?>">
                                </div>
                                <ul>
                                    <li>
                                        <i class="fa fa-calendar" aria-hidden="true"></i> <?= $event->eventDate; ?> 2017
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i> <?= $event->eventStartTime; ?> - <?= $event->eventEndTime; ?>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker" aria-hidden="true"></i> <?= $event->location; ?>
                                    </li>
                                    <li>
                <i class="fa fa-map-o" aria-hidden="true"></i> <?php if($event->direction!='') { ?><a class="getdir" href="<?= $event->direction; ?>" target="_blank">Get Direction</a> <?php }  ?>
                                    </li>
                                </ul>
                                    <?php 
                                    if($event->id>0)
                                    //if(($event->eventCapacity>0 and $event_registration_count>=$event->eventCapacity) or ($event->id!=22 and $event->id!=20))
                                    //if($event->eventCapacity>0 and $event_registration_count>=$event->eventCapacity)
                                    {
                                        $href = "javascript:void(0);";
                                        $label = 'Registration Closed';
                                        /*$registrationLink = '<a href="javascript:void(0);" >Registration Closed</a>';
                                        echo $registrationLink;*/
                                    }
                                    else{
                                        $registration_url = base64_encode($event->eventName.' & '.$event->eventDate.' & '.$event->id);
                                        $href = base_url('registration/'.$registration_url);
                                        /*$registrationLink = '<a href="'.base_url('registration/'.$registration_url).'>register now</a>';
                                        echo ' <a href="javascript:void(0);" >Registration Closed</a>';*/
                                        $label = 'Register Now';
                                    }
                                    ?>
                                   <a href="<?= $href; ?>" class="regis"><?= $label; ?></a>
                            </div>
				        </div>
                    <?php } ?>
                    </div>    
        </div>
    </div>
    <div id="load_data_message"> <a href="javascript:void(0);" onclick='load_more();' id='load_more_button' class="loadmore">Load More</a></div>
</div>
</div>