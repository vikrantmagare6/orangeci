<div class="body-content">
        <div class="orangebg-banner orangemobileparent">
        <div class="orangebannerdiv orangemobilebannerhide"><!-- 
            <div class="orangebannersocial">
                <i class="oransocialfoot fa fa-facebook" aria-hidden="true"></i><i class="oransocialfoot fa fa-twitter" aria-hidden="true"></i><i class="oransocialfoot fa fa-instagram" aria-hidden="true"></i>
            </div> -->
            <div class="orangebannerdivslider">
                <div class="orangebannerdivinner">
                    <div class="orangebannerdivinnerwrap">
                        <a href="<?= base_url('activities/?activities=entertainement'); ?>"><img class="img-responsive orangebannerdivinnerimg" src="<?= base_url('uploads/entertainment.png'); ?>" /></a>
                        <!-- <div class="orangebannerdivinnermore">
                            <h1>Entertainment</h1>
                            <p>3 Days of Extravaganza in the orange capital of the country</p>
                            <a href="<?= base_url('activities/?activities=entertainement'); ?>" class="button-class">Know More</a>
                        </div> -->
                    </div>
                </div>
                <div class="orangebannerdivinner">
                    <div class="orangebannerdivinnerwrap">
                        <a href="<?= base_url('activities/?activities=games'); ?>"><img class="img-responsive orangebannerdivinnerimg" src="<?= base_url('uploads/games.png'); ?>" /></a>
                                            </div>
                </div>
                <div class="orangebannerdivinner">
                    <div class="orangebannerdivinnerwrap">
                        <a href="<?= base_url('activities/?activities=food'); ?>"><img class="img-responsive orangebannerdivinnerimg" src="<?= base_url('uploads/food.png'); ?>" /></a>
                    </div>
                </div>
                <div class="orangebannerdivinner">
                    <div class="orangebannerdivinnerwrap">
                       <a href="<?= base_url('activities/?activities=art-and-culture'); ?>"><img class="img-responsive orangebannerdivinnerimg" src="<?= base_url('uploads/art.png'); ?>" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container client-logos">
        <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4">
            <span><a class="desksponlogo"  target="_blank" href="https://www.uplonline.com/"><img src="<?= base_url('uploads/upl-logo.png'); ?>" alt="" class="img-responsive" /></a></span>
            
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <span><a class="desksponlogo" href="http://www.bajajelectricals.com/" target="_blank"><img src="<?= base_url('uploads/bajaj-logo.png'); ?>" alt="" class="img-responsive" /></a></span>
           
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <span><a class="desksponlogo" target="_blank" href="https://www.maharashtratourism.gov.in/"><img src="<?= base_url('uploads/tourism-logo.png'); ?>" alt="" class="img-responsive" /></a></span>

        </div>
    </div>
            </div>
    </div>
        <div class="orangebg-banner orangedesktopparent">
            <div class="orangebannerdiv">
            	
            <div class="downarrowscroll"><i class="fa fa-long-arrow-down downarrowscrollico" aria-hidden="true"></i></div>
                <!-- <div class="orangebannersocial">
                <i class="oransocialfoot fa fa-facebook" aria-hidden="true"></i><i class="oransocialfoot fa fa-twitter" aria-hidden="true"></i><i class="oransocialfoot fa fa-instagram" aria-hidden="true"></i>
            </div> -->
                <div class="orangebannerdivinner orangedesktopslider">

                    <div class="needforcenter">
                        <div class="testbanparent clearfix">
                            <div class="testbanchild1">
                                <h1 class="orangeworld">World</h1><img class="testorangeimage" src="<?= base_url('uploads/orange-txt.svg'); ?>">
                                <img class="hangingorangestuff hangingradio" src="<?= base_url('uploads/radio.gif'); ?>"/></div>
                            <div class="testbanchild2">
                                <div class="testbanchild2inner">
                                    <div class="testbanchild2innerbottom">
                                        <h1>Festival</h1>
                                        <h1>In Nagpur</h1>
                                        <h1>16 - 17 - 18</h1>
                                        <h1>December 2017</h1>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="testbanparent2">
                            <div class="testbaninnerchild1">
                                <h1>Entertainment</h1>
                            <p><strong class="pstrongquote">“Squeezing”</strong> in hours of music and activities.</p>
                            <a href="<?= base_url('activities/?activities=entertainement'); ?>" class="button-class">Know More</a>
                            </div>
                        </div>
                        <div class="testbanparent3"></div>
                    </div>

                    <div class="needforcenter">
                        <div class="testbanparent clearfix">
                            <div class="testbanchild1">
                                <h1 class="orangeworld">World</h1><img class="testorangeimage" src="<?= base_url('uploads/orange-txt.svg'); ?>">
                                <img class="hangingorangestuff hangingcycles" src="<?= base_url('uploads/cycle.gif'); ?>"/></div>
                            <div class="testbanchild2">
                                <div class="testbanchild2inner">
                                    <div class="testbanchild2innerbottom">
                                        <h1>Festival</h1>
                                        <h1>In Nagpur</h1>
                                        <h1>16 - 17 - 18</h1>
                                        <h1>December 2017</h1>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div class="testbanparent2">
                            <div class="testbaninnerchild1">
                                <h1>Games</h1>
                            <p>Enjoy yourself to the <strong class="pstrongquote">“core”</strong> with our thrilling games.</p>
                            <a href="<?= base_url('activities/?activities=games'); ?>" class="button-class">Know More</a>
                            </div>
                        </div>
                        <div class="testbanparent3"></div>
                    </div>

                    <div class="needforcenter">
                        <div class="testbanparent clearfix">
                            <div class="testbanchild1">
                                <h1 class="orangeworld">World</h1><img class="testorangeimage" src="<?= base_url('uploads/orange-txt.svg'); ?>">
                                <img class="hangingorangestuff hangingjar" src="<?= base_url('uploads/jar-gif.gif'); ?>"/></div>
                            <div class="testbanchild2">
                                <div class="testbanchild2inner">
                                    <div class="testbanchild2innerbottom">
                                        <h1>Festival</h1>
                                        <h1>In Nagpur</h1>
                                        <h1>16 - 17 - 18</h1>
                                        <h1>December 2017</h1>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="testbanparent2">
                            <div class="testbaninnerchild1">
                                <h1>Food</h1>
                            <p>Infusing the <strong class="pstrongquote">“flavours”</strong> of Nagpur’s bounty.</p>
                            <a href="<?= base_url('activities/?activities=food'); ?>" class="button-class">Know More</a>
                            </div>
                        </div>
                        <div class="testbanparent3"></div>
                    </div>

                    <div class="needforcenter">
                        <div class="testbanparent clearfix">
                            <div class="testbanchild1">
                                <h1 class="orangeworld">World</h1><img class="testorangeimage" src="<?= base_url('uploads/orange-txt.svg'); ?>">
                                <img class="hangingorangestuff hangingdream" src="<?= base_url('uploads/dream.gif'); ?>"/></div>
                            <div class="testbanchild2">
                                <div class="testbanchild2inner">
                                    <div class="testbanchild2innerbottom">
                                        <h1>Festival</h1>
                                        <h1>In Nagpur</h1>
                                        <h1>16 - 17 - 18</h1>
                                        <h1>December 2017</h1>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="testbanparent2">
                            <div class="testbaninnerchild1">
                                <h1>Art and Culture</h1>
                            <p>Getting the creative <strong class="pstrongquote">“juices”</strong> flowing.</p>
                            <a href="<?= base_url('activities/?activities=art-and-culture'); ?>" class="button-class">Know More</a>
                            </div>
                        </div>
                        <div class="testbanparent3"></div>
                    </div>


                </div>
            </div>



        <div class="container client-logos" id="client-logos">
        <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4">
            <span><a class="desksponlogo"  target="_blank" href="https://www.uplonline.com/"><img src="<?= base_url('uploads/upl-logo.png'); ?>" alt="" class="img-responsive" /></a></span>
            
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <span><a class="desksponlogo" href="http://www.bajajelectricals.com/" target="_blank"><img src="<?= base_url('uploads/bajaj-logo.png'); ?>" alt="" class="img-responsive" /></a></span>
           
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <span><a class="desksponlogo" target="_blank" href="https://www.maharashtratourism.gov.in/"><img src="<?= base_url('uploads/tourism-logo.png'); ?>" alt="" class="img-responsive" /></a></span>

        </div>
    </div>
            </div>
        </div>
        <!-- <div style="min-height:1500px;">
        </div> -->
        <!-- About Us Tabs Start -->
        <div class="about-section">
            <div class="container card-part">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-inner">
                            <h3>The Orange City opens its gates to celebrate the World Orange Festival.</h3>
                            <p>Nagpur, the city of Oranges hosts the first ever World Orange Festival. The 3-day long extravaganza celebrates the fruit, the city it grows in, its farmer and the community. An activity packed weekender, this festival will be complete with seminars and exhibitions for the industry, master classes with master chefs for the foodies, installations by artists for the art lovers, concerts by the greats, domestic and international acts, games and much more scattered all through the city. We welcome you, the locals and the tourists, the farmers and the traders, the children and the adults, to be a part of Nagpur’s biggest celebration.</p>
                            <div class="button-container">
                                <a href="<?= base_url('partners'); ?>" class="button-class">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About Us Tabs Ends -->
        <!-- Activities Tabs Start -->
        <div class="home-activity">
            <div class="container-fluid active-outer-box">
                <div class="row">
                    <div class="col-md-12 title-section">
                        <h3>Activities</h3>
                        <span><img src="<?= base_url('uploads/leaf.png'); ?>" alt="" /></span>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <a href="<?= base_url('activities/?activities=art-and-culture'); ?>">
                            <div class="activity-wrapper">
                                <div class="activity-outer-div art">
                                    <div class="activity-overlay">
                                        <div class="activity-content-div">
                                            <div class="activity-outer-valign">
                                                <div class="activity-inner-valign">
                                                    <div class="activity-content">
                                                        <img src="<?= base_url('uploads/art-icon.png'); ?>">
                                                        <h3>Art &amp; Culture</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="arrow-box">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <a href="<?= base_url('activities/?activities=entertainement'); ?>">
                            <div class="activity-wrapper">
                                <div class="activity-outer-div entertain">
                                    <div class="activity-overlay">
                                        <div class="activity-content-div">
                                            <div class="activity-outer-valign">
                                                <div class="activity-inner-valign">
                                                    <div class="activity-content">
                                                        <img src="<?= base_url('uploads/entertainment-icon.png'); ?>">
                                                        <h3>Entertainment</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="arrow-box">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <a href="<?= base_url('activities/?activities=food'); ?>">
                            <div class="activity-wrapper">
                                <div class="activity-outer-div food">
                                    <div class="activity-overlay">
                                        <div class="activity-content-div">
                                            <div class="activity-outer-valign">
                                                <div class="activity-inner-valign">
                                                    <div class="activity-content">
                                                        <img src="<?= base_url('uploads/food-icon.png'); ?>">
                                                        <h3>Food</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="arrow-box">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <a href="<?= base_url('activities/?activities=games'); ?>">
                            <div class="activity-wrapper">
                                <div class="activity-outer-div games">
                                    <div class="activity-overlay">
                                        <div class="activity-content-div">
                                            <div class="activity-outer-valign">
                                                <div class="activity-inner-valign">
                                                    <div class="activity-content">
                                                        <img src="<?= base_url('uploads/games-icon.png'); ?>">
                                                        <h3>Games</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="arrow-box">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Activities Tabs End -->
        <div class="homepageevent homepageevent1">
        <img src="<?= base_url('uploads/orangefarmer-1.png'); ?>" class="orangefarmer1">
        <img src="<?= base_url('uploads/orangefarmer-2.png'); ?>" class="orangefarmer2">            
            <div class="activitymaincontdiv">
        <h1 class="activitymainheading">Farmers First</h1>
        <img src="<?= base_url('uploads/activities/leaf.png'); ?>">
        <p class="activitymaincontent">The backbone of the orange economy display the fruits of their labour in an array of exhibitions, seminars and farmers markets.</p>
        <a href="<?= base_url('farmers-first'); ?>" class="activitybutton-class">Know More</a>
      </div>
        </div>


        <div class="homepageevent homepageevent2">
        <div class="homepageevent2overlay">
        </div>            
            <div class="activitymaincontdiv">
        <h1 class="activitymainheading">Event Schedule</h1>
        <img src="<?= base_url('uploads/activities/leaf.png'); ?>">
        <p class="activitymaincontent">Whether you’re an orange expert or novice, young or old, a foodie or an art lover, there’s something for everyone. Click here to find out what we have in store for you.</p>
        <a href="<?= base_url('event_schedule'); ?>" class="activitybutton-class">Know More</a>
      </div>
        </div>

        <!-- Social Media Live Feeds Start -->
        <div class="container feeds">
        </div>
        <!-- Social Media Live Feed End -->

    </div>