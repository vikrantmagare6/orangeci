<div class="body-content thanks-image">
        <!-- Main Div Start -->
        <div class="thanks-box">
                <h3>Thank You</h3>
                <div class="bg-box">
                    <div class="box-txt">
                    <img src="<?= base_url('uploads/thank-orange.png'); ?>" class="img-responsive" />
                   <!-- <div class="button-container btn-th">
                        <a href="#" class="button-class">Add to Calendar <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                    </div>-->
                    <p>You have signed up for the <?= $eventName; ?> event in the Orange city of India.The details of the event are sent to your e-mail ID. We request you to present them on the day of the event.</p>
                    <!--<p>Please note down this ID '<?= $commonRegister; ?>' in case you forget your registration ID's and email. Then show this ID to the venue of the Event.</p>-->
                    <div class="button-container">
                        <a href="<?= base_url('event_schedule'); ?>" class="button-class">Back to Event Schedule</a>
                    </div>
                </div>
                </div>
        </div>
        <!-- Main Div End -->
    </div>