<div class="body-content">
    <div class="container gameparent">

        <div class="game-head">
            <img src="<?= base_url('uploads/games/tic-tac-tow.jpg'); ?>" alt="">
        </div>
    <div class="game-container">
        <div class="col-lg-7 col-md-8 col-sm-8">
            <div class="tic-tac-toe">
                    <div id="square1" class="tile free"></div>
                    <div id="square2" class="tile free"></div>
                    <div id="square3" class="tile free"></div>
                    <div id="square4" class="tile free"></div>
                    <div id="square5" class="tile free"></div>
                    <div id="square6" class="tile free"></div>
                    <div id="square7" class="tile free"></div>
                    <div id="square8" class="tile free"></div>
                    <div id="square9" class="tile free"></div>
              </div>
              <div class="button-r" id="reset-button">
                <button class="start-btn">Restart</button>
              </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 left-game">
          <div class="game-wrape">
            <h4>player</h4>
            <button class="p-bnt">single player</button>

            <ul class="player-list">
                <li>
                    <p>you</p>
                    <img src="<?= base_url('uploads/games/orange.jpg'); ?>" alt="first">
                </li>
                <li>
                  <p>computer</p>
                    <img src="<?= base_url('uploads/games/leaf.jpg'); ?>" alt="Second">
                </li>
            </ul>
          </div>
        <div class="result-wrap">
          <div class="result">
            <button class="re-btn">result</button>
            <div class="re-content running">
                <p>Up for a quick game? See if you still own the best pastime.</p>
                <!-- <img src="sad.png" alt=""> -->
            </div> 

            <div class="re-content lost">
                <h4>You lose</h4>
                <p>Ouch! Are the Oranges ‘sour’? You can prove it otherwise by playing another round!</p>
                <img src="<?= base_url('uploads/games/sad.png'); ?>" alt="">
            </div>      

            <div class="re-content draw">
                <h4>Draw</h4>
                <p>Ah! Looks like someone is as good as you. Play another game and you might win!</p>
                <img src="<?= base_url('uploads/games/draw.png'); ?>" alt="">
            </div>

            <div class="re-content won">
                <h4>You Won</h4>
                <p>We have a pro in the house! Dare to have a winning streak? Play again!</p>
                <img src="<?= base_url('uploads/games/won.png'); ?>" alt="">
            </div> 

          <div class="button-r" id="replay-button">
           <button class="start-btn">Play Again</button>
          </div>  

          <div class="button-r" id="close-button">
           <button class="start-btn">X</button>
          </div>    
          </div>
        </div>
<!--            <p>A simple game of tic tac toe to play<br/>
                You play as <span class="red">X</span> and the computer plays as <span class="blue">O</span><br/>
                Start by choosing a square to play</p> -->
        </div>
        <div class="col-lg-1"></div>
    </div>
    </div>
    </div>