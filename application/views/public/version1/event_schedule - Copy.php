<div class="body-content" style="position: relative;">

            <img src="<?= base_url('uploads/event/guitar.gif'); ?>" style="width: 150px;" class="evnt1">
           <img src="<?= base_url('uploads/event/saxophone.gif'); ?>" class="evnt2" style="width: 100px;">
           <img src="<?= base_url('uploads/event/pan-pizza-gif.gif'); ?>" class="evnt3" style="width: 100px;">
        <div class="active-banner event-schedule">
            <h3>Event schedule</h3>
            <select name="eventselection" class="eventselection">
                <option selected="selected" value="all">All</option>
                <option value="day1">16 Dec</option>
                <option value="day2">17 Dec</option>
                <option value="day3">18 Dec</option>
            </select>
        </div>
        <div class="event-schedule-content" id="event-schedule-content">
            <div class="outter-c container-fluid">
                <div class="inner-c row">
                    <div id="load_data">
                    <input type="hidden" name="start_val" id="start_val" value="0">
                    <?php foreach($events as $event) { ?>
                        <div class="item col-md-4 col-sm-6">
                            <div class="item-inner">
                                <div class="liv">
                                    <span>live bands</span>
                                    <img src="<?= base_url('uploads/event/music.png'); ?>">
                                </div>
                                <ul>
                                    <li>
                                        <i class="fa fa-calendar" aria-hidden="true"></i> 9th December 2017
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o" aria-hidden="true"></i> 10:00 am - 1:00 pm
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker" aria-hidden="true"></i> location
                                    </li>
                                    <li>
                                        <i class="fa fa-map-o" aria-hidden="true"></i> Get Direction
                                    </li>
                                </ul>
                                <a href="registration.php">register now</a>
                            </div>
				        </div>
                    <?php } ?>
                    </div>
                    <div id="load_data_message"><button type='button' class='btn btn-warning' onclick='load_more();' id='load_more_button'>Load More</button></div>    
                </div> 
                </div>
            </div>
        </div>
    </div>