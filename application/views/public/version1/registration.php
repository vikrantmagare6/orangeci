 <!-- Main Wrapper Div Start -->
 <div class="body-content background-image">
 <!-- Main Div Start -->

<script type="text/html" id="tan">
     <div class="clone-div">
         <div class="col-xs-12 col-sm-12 col-md-6 filed-box">
             <span><i class="fa fa-user" aria-hidden="true"></i></span>
             <input type="text" name="name[<%= element.i %>]"  placeholder="Full Name*" class="common-inputfd tesf ">
         </div>
         <div class="col-xs-12 col-sm-12 col-md-6 filed-box">
             <span><i class="fa fa-phone" aria-hidden="true"></i></span>
             <input type="text" name="phone[<%= element.p %>]" placeholder="Mobile No*" class="common-inputfd tesf teref2">
             <!-- <a href="javascript:void(0);" class="clone"> <span class="float"><i class="fa fa-plus-circle plus" aria-hidden="true"></i></span></a> -->
         </div>
     </div>
 </script>

 <div class="block-register">
     <div class="outer-valign">
         <div class="inner-valign">
             <h3>Registration</h3>
             <div class="full-box">
                 <div class="box-txt">
                         <?php if($message_notification = $this->session->flashdata('message_notification')) { ?>
                            <!-- Message Notification Start -->
                            <div id="message_notification">
                            <div class="alert alert-<?= $this->session->flashdata('class'); ?>">    
                                <button class="close" data-dismiss="alert" type="button">×</button>
                                <strong>
                                    <?= $this->session->flashdata('message_notification'); ?> 
                                </strong>
                            </div>
                            </div>
                            <!-- Message Notification End -->
                            <?php } ?>
                     <p>Grab your seat, fastened the seat belt and sit tight as you are about to sign up for the most exciting and juiciest event in the town!</p>
                     <!-- Form Start -->
                     <div class="form-section">
                     <form id="register"  class="common-form" enctype="multipart/form-data" action="<?= base_url('event/submit'); ?>" method="post">
                               <div class="clone-div">
                                 <div class="col-xs-12 col-sm-12 col-md-6 filed-box">
                                     <span><i class="fa fa-user" aria-hidden="true"></i></span>
                                     <input type="text" name="name[0]" placeholder="Full Name*" class="common-inputfd">
                                 </div>
                                 <div class="col-xs-12 col-sm-12 col-md-6 filed-box">
                                   <span><i class="fa fa-phone" aria-hidden="true"></i></span>
                                     <input type="text" name="phone[0]" placeholder="Mobile No*" class="common-inputfd">
                                 </div>

                               </div>
                               <div class="incredecre">
                                <p>Add a member</p>
                                    <a href="javascript:void(0);" class="clone"> <span class="nonfloat"><i class="fa fa-plus-circle plus" aria-hidden="true"></i></span></a>
                                    <a href="javascript:void(0);" class="delete-clone"> <span class="nonfloat minus"><i class="fa fa-minus-circle" aria-hidden="true"></i></span></a>
                                </div>
                                 <div class="col-xs-12 col-sm-12 col-md-12 filed-box">
                                   <span class="last-ico"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                     <input type="text" name="email" placeholder="Email ID*" class="common-inputfd last">
                                 </div>
                                 <div class="col-md-12 text-center">
                                 <input type="hidden" name="eventId" value="<?= $eventId; ?>">
                              <input type="hidden" name="eventName" value="<?= $eventName; ?>">
                              <input type="hidden" name="redirectURL" value="<?= $redirectURL; ?>">
                                 <button class="submit-btn" type="submit">Register Now
                                 </button>
                             </div>
                             </form>
                         <!-- Form Start -->
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <!-- Main Div End -->
</div>