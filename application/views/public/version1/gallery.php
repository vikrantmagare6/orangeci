<div class="body-content">
	<div class="active-banner">
		<h3>Gallery</h3>
	</div>
	<div class="main-activebx">
		<div class="container-fluid wrapper-box">
			<div class="row">
				<div class="col-md-12">
					<div class="gallery-tab">
						<ul class="tabs">
							<li class="tab-menu active" rel="tab1">
								<a id="art-and-culture" href="javascript:void(0);">
									<img src="<?= base_url('uploads/gallery/picture-icon.png'); ?>">
									<span class="tab-title">Pictures</span>
									<img src="<?= base_url('uploads/gallery/leaf.png'); ?>" class="hover-img">
								</a>
							</li>
							<li class="tab-menu" rel="tab2" id="food">
								<a id="food" href="javascript:void(0);">
									<img src="<?= base_url('uploads/gallery/video-icon.png'); ?>">
									<span class="tab-title">Videos</span>
									<img src="<?= base_url('uploads/gallery/leaf.png'); ?>" class="hover-img">
								</a>
							</li>
						</ul>
						<div class="tab_container">
							<h3 class="d_active tab_drawer_heading" rel="tab1">
								<span><img src="<?= base_url('uploads/gallery/picture-icon.png'); ?>"></span>
							Pictures</h3>
							<!-- #Pictures -->
							<div id="tab1" class="tab_content">
								<div id="photos" class="gal">
									<div class="gallery-grid">
										<a href="<?= base_url('uploads/gallery/full-view-01.jpg'); ?>" data-sub-html="<h4>Famed chefs share their kitchen tricks which will up your culinary skills</h4>">
											<img src="<?= base_url('uploads/gallery/01.jpg'); ?>" class="img-responsive" />
										</a>
										<h4>Famed chefs share their kitchen tricks which will up your culinary skills</h4>
									</div>


									<!-- new images-->
									<div class="gallery-grid">
										<a href="<?= base_url('uploads/gallery/DSC6145.jpg'); ?>" data-sub-html="<h4>DJ Aqeel making the crowd groove to his beats.</h4>">
											<img src="<?= base_url('uploads/gallery/sDSC6145.jpg'); ?>" class="img-responsive" />
										</a>
										<h4>DJ Aqeel making the crowd groove to his beats.</h4>
									</div>
									<div class="gallery-grid">
										<a href="<?= base_url('uploads/gallery/DSC6188.jpg'); ?>" data-sub-html="<h4>What a night it was when DJ Aqeel delivered one of his best performances at the #WorldOrangeFestival.</h4>">
											<img src="<?= base_url('uploads/gallery/sDSC6188.jpg'); ?>" class="img-responsive" />
										</a>
										<h4>What a night it was when DJ Aqeel delivered one of his best performances at the #WorldOrangeFestival.</h4>
									</div>
									<div class="gallery-grid">
										<a href="<?= base_url('uploads/gallery/DSC7936.jpg'); ?>" data-sub-html="<h4>When Nagpur got Bennyfied!</h4>">
											<img src="<?= base_url('uploads/gallery/sDSC7936.jpg'); ?>" class="img-responsive" />
										</a>
										<h4>When Nagpur got Bennyfied!</h4>
									</div>
									<div class="gallery-grid">
										<a href="<?= base_url('uploads/gallery/DSC8714.jpg'); ?>" data-sub-html="Benny Dayal performing live at the #WorldOrangeFestival.</h4>">
											<img src="<?= base_url('uploads/gallery/sDSC8714.jpg'); ?>" class="img-responsive" />
										</a>
										<h4>Benny Dayal performing live at the #WorldOrangeFestival.</h4>
									</div>
									<div class="gallery-grid">
										<a href="<?= base_url('uploads/gallery/17pho400f.jpg'); ?>" data-sub-html="<h4>DJ Nina and Malika adding their charm and beats at the #WorldOrangeFestival.</h4>">
											<img src="<?= base_url('uploads/gallery/s17pho400f.jpg'); ?>" class="img-responsive" />
										</a>
										<h4>DJ Nina and Malika adding their charm and beats at the #WorldOrangeFestival.</h4>
									</div>
									<div class="gallery-grid">
										<a href="<?= base_url('uploads/gallery/17pho400j.jpg'); ?>" data-sub-html="<h4>DJ Peter ‘Mukxa’ Manuel - The Maestro working his magic on the console.</h4>">
											<img src="<?= base_url('uploads/gallery/s17pho400j.jpg'); ?>" class="img-responsive" />
										</a>
										<h4>DJ Peter ‘Mukxa’ Manuel - The Maestro working his magic on the console.</h4>
									</div>
									<div class="gallery-grid">
										<a href="<?= base_url('uploads/gallery/108.jpg'); ?>" data-sub-html="<h4>Sonalee Kulkarni’s mesmerizing performance in front of thousands at the #WorldOrangeFestival.</h4>">
											<img src="<?= base_url('uploads/gallery/s108.jpg'); ?>" class="img-responsive" />
										</a>
										<h4>Sonalee Kulkarni’s mesmerizing performance in front of thousands at the #WorldOrangeFestival.</h4>
									</div>
									<div class="gallery-grid">
										<a href="<?= base_url('uploads/gallery/DCM_2572.jpg'); ?>" data-sub-html="<h4>Luke Kenny with a dynamic and a lively performance.</h4>">
											<img src="<?= base_url('uploads/gallery/sDCM_2572.jpg'); ?>" class="img-responsive" />
										</a>
										<h4>Luke Kenny with a dynamic and a lively performance.</h4>
									</div>
									<div class="gallery-grid">
										<a href="<?= base_url('uploads/gallery/DCM_2749.jpg'); ?>" data-sub-html="<h4>13 year old Sulieman stole the show by delivering a mesmerizing performance.</h4>">
											<img src="<?= base_url('uploads/gallery/sDCM_2749.jpg'); ?>" class="img-responsive" />
										</a>
										<h4>13 year old Sulieman stole the show by delivering a mesmerizing performance.</h4>
									</div>
									<div class="gallery-grid">
										<a href="<?= base_url('uploads/gallery/DSC_3597.jpg'); ?>" data-sub-html="<h4>Rahul Deshpande at the morning raga session made sure that everyone present had a brilliant start to their day.</h4>">
											<img src="<?= base_url('uploads/gallery/sDSC_3597.jpg'); ?>" class="img-responsive" />
										</a>
										<h4>Rahul Deshpande at the morning raga session made sure that everyone present had a brilliant start to their day.</h4>
									</div>

									<div class="gallery-grid">
										<a href="<?= base_url('uploads/gallery/full-view-01psk.jpg'); ?>" data-sub-html="<h4>The Orange Army leading the 3kms Cultural Parade like a BOSS!</h4>">
											<img src="<?= base_url('uploads/gallery/01psk.jpg'); ?>" class="img-responsive" />
										</a>
										<h4>The Orange Army leading the 3kms Cultural Parade like a BOSS!</h4>
									</div>
<!-- <div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-02psk.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/02psk.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-17pho199b.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/17pho199b.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div> -->
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-17pho316.jpg'); ?>" data-sub-html="<h4>The Nagpuri Eiffel Tower made out of Nagpuri Oranges grabbed plenty of eyeballs.</h4>">
		<img src="<?= base_url('uploads/gallery/17pho316.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>The Nagpuri Eiffel Tower made out of Nagpuri Oranges grabbed plenty of eyeballs.</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-17pho378.jpg'); ?>" data-sub-html="<h4>The festival reached great heights with this Hot Air Balloon Rides at the Indoor Stadium.</h4>">
		<img src="<?= base_url('uploads/gallery/17pho378.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>The festival reached great heights with this Hot Air Balloon Rides at the Indoor Stadium.</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-17pho381.jpg'); ?>" data-sub-html="<h4>The OrangeMan and the people of Nagpur jumping with joy after their Hot air Balloon ride.</h4>">
		<img src="<?= base_url('uploads/gallery/17pho381.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>The OrangeMan and the people of Nagpur jumping with joy after their Hot air Balloon ride.</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-17pho394.jpg'); ?>" data-sub-html="<h4>The kids of Nagpur being competitive and fun, all at the same time.</h4>">
		<img src="<?= base_url('uploads/gallery/17pho394.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>The kids of Nagpur being competitive and fun, all at the same time.</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-18-sara.jpg'); ?>" data-sub-html="<h4>MasterChef Sarah Todd delights Nagpur audience’s taste buds with her culinary skills.</h4>">
		<img src="<?= base_url('uploads/gallery/18-sara.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>MasterChef Sarah Todd delights Nagpur audience’s taste buds with her culinary skills.</h4>
</div>
<!-- <div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-021.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/021.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DCM_1800.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/DCM_1800.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div> -->
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DCM_2083.jpg'); ?>" data-sub-html="<h4>One of the many fun filled elements of our famous Cultural Parade.</h4>">
		<img src="<?= base_url('uploads/gallery/DCM_2083.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>One of the many fun filled elements of our famous Cultural Parade.</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DCM_2246.jpg'); ?>" data-sub-html="<h4>Bajaj Electricals’ Wish Tree proved to be a Prominent Photopoint for the people of Nagpur.</h4>">
		<img src="<?= base_url('uploads/gallery/DCM_2246.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Bajaj Electricals’ Wish Tree proved to be a Prominent Photopoint for the people of Nagpur.</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DCM_2343.jpg'); ?>" data-sub-html="<h4>Rupmatii Jolly creates a perfect evening at the South Central Zone with her melodious sufi voice.</h4>">
		<img src="<?= base_url('uploads/gallery/DCM_2343.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Rupmatii Jolly creates a perfect evening at the South Central Zone with her melodious sufi voice.</h4>
</div>
<!-- <div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DCM_2396.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/DCM_2396.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div> -->
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DCM_2529.jpg'); ?>" data-sub-html="<h4>Luke Kenny adds his charm to a power packed music display for the Nagpur audience.</h4>">
		<img src="<?= base_url('uploads/gallery/DCM_2529.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Luke Kenny adds his charm to a power packed music display for the Nagpur audience.</h4>
</div>
<!-- <div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DCM_2672.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/DCM_2672.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DCM_2712.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/DCM_2712.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC3127.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/_DSC3127.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div> -->
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC3181.jpg'); ?>" data-sub-html="<h4>Our mascot, the OrangeMan greets the Nagpur crowd with sheer enthusiasm.</h4>">
		<img src="<?= base_url('uploads/gallery/_DSC3181.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Our mascot, the OrangeMan greets the Nagpur crowd with sheer enthusiasm.</h4>
</div>
<!-- <div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC4062.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/_DSC4062.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC4148.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/_DSC4148.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC4189.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/_DSC4189.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC4198.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/_DSC4198.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC4223.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/_DSC4223.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC4247.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/_DSC4247.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC4354.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/_DSC4354.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC4379.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/_DSC4379.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC4384.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/_DSC4384.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC4410.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/_DSC4410.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC4443.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/_DSC4443.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div> -->
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-DSC4456.jpg'); ?>" data-sub-html="<h4>Our EDM night with the best DJs, got the crowd going 'once more' all the time!</h4>">
		<img src="<?= base_url('uploads/gallery/_DSC4456.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Our EDM night with the best DJs, got the crowd going 'once more' all the time!</h4>
</div>

<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-PSK_5387.jpg'); ?>" data-sub-html="<h4>The Orange Elephant at the Traffic Park was a sight to behold and capture!</h4>">
		<img src="<?= base_url('uploads/gallery/PSK_5387.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>The Orange Elephant at the Traffic Park was a sight to behold and capture!</h4>
</div>
<!-- <div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-PSK_5715.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/PSK_5715.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-RAJ_1039.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
	<img src="<?= base_url('uploads/gallery/RAJ_1039.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div> -->



<!-- end new images-->

<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/02.jpg'); ?>" data-sub-html="<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>">
		<img src="<?= base_url('uploads/gallery/02.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Picking fresh oranges directly from the tree is no more a long-lost wish!</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-03.jpg'); ?>" data-sub-html="<h4>The all-girls band take you through some of their best medleys</h4>">
		<img src="<?= base_url('uploads/gallery/03.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>The all-girls band take you through some of their best medleys</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-04.jpg'); ?>" data-sub-html="<h4>Silver Strings come in with a surprise for those who are fans of both Bollywood and Hollywood music</h4>">
		<img src="<?= base_url('uploads/gallery/04.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Silver Strings come in with a surprise for those who are fans of both Bollywood and Hollywood music</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-05.jpg'); ?>" data-sub-html="<h4>Tap on the tunes and hum a song as the band strikes a right cord </h4>">
		<img src="<?= base_url('uploads/gallery/05.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Tap on the tunes and hum a song as the band strikes a right cord </h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-07.jpg'); ?>" data-sub-html="<h4>You are in a for a treat as Silver Strings pick their popular Hindi and English mixes </h4>">
		<img src="<?= base_url('uploads/gallery/07.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>You are in a for a treat as Silver Strings pick their popular Hindi and English mixes </h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/08.jpg'); ?>" data-sub-html="<h4>A live performance by professional musicians to soar the festive spirit </h4>">
		<img src="<?= base_url('uploads/gallery/08.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>A live performance by professional musicians to soar the festive spirit </h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/09.jpg'); ?>" data-sub-html="<h4>The tradition of vibrant Kenya comes straight on the stage of World Orange Festival</h4>">
		<img src="<?= base_url('uploads/gallery/09.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>The tradition of vibrant Kenya comes straight on the stage of World Orange Festival</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/10.jpg'); ?>" data-sub-html="<h4>Hold your breath as African Acrobats display some stunning co-ordination, balance and much grace</h4>">
		<img src="<?= base_url('uploads/gallery/10.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>Hold your breath as African Acrobats display some stunning co-ordination, balance and much grace</h4>
</div>
<div class="gallery-grid">
	<a href="<?= base_url('uploads/gallery/full-view-11.jpg'); ?>" data-sub-html="<h4>The tribal art mingles many elements of nature, one of them being fire</h4>">
		<img src="<?= base_url('uploads/gallery/11.jpg'); ?>" class="img-responsive" />
	</a>
	<h4>The tribal art mingles many elements of nature, one of them being fire</h4>
</div>
</div>
</div>
<!-- #Videos -->
<h3 class="tab_drawer_heading" rel="tab2">
	<span><img src="<?= base_url('uploads/gallery/video-icon.png'); ?>"></span>
Videos</h3>
<div id="tab2" class="tab_content">
                                    <!-- <div id="video-gallery" class="gal">
                                        <div class="gallery-grid video-bg">
                                            <a href="https://www.youtube.com/embed/meBbDqAXago?rel=0" frameborder="0" allowfullscreen class="video">
                                          <img src="images/gallery/video-img.jpg" class="img-responsive" />
                                          <div class="demo-gallery-poster">
                                            <img src="images/gallery/play-button.png">
                                          </div>
                                      </a>
                                        <h4>Video 1</h4>
                                        </div>
                                        <div class="gallery-grid video-bg">
                                            <a href="https://vimeo.com/1084537" frameborder="0" allowfullscreen class="video">
                                              <img src="images/gallery/video-img.jpg" class="img-responsive" />
                                              <div class="demo-gallery-poster">
                                            <img src="images/gallery/play-button.png">
                                          </div>
                                          </a>
                                          <h4>Video 2</h4>
                                        </div>
                                        <div class="gallery-grid video-bg">
                                            <a href="https://vimeo.com/1084537" frameborder="0" allowfullscreen class="video">
                                              <img src="images/gallery/video-img.jpg" class="img-responsive" />
                                              <div class="demo-gallery-poster">
                                            <img src="images/gallery/play-button.png">
                                          </div>
                                          </a>
                                          <h4>Video 3</h4>
                                        </div>
                                        
                                          <div class="gallery-grid video-bg">
                                            <a href="https://vimeo.com/1084537" frameborder="0" allowfullscreen class="video">
                                              <img src="images/gallery/video-img.jpg" class="img-responsive" />
                                              <div class="demo-gallery-poster">
                                            <img src="images/gallery/play-button.png">
                                          </div>
                                          </a>
                                          <h4>Video 4</h4>
                                        </div>
                                       
                                    </div> -->
                                    <div class="video-txt">
                                    	<h2>No Videos</h2>
                                    </div>
                                </div>
                            </div>
                            <!-- .tab_container -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>