 <!-- Main Wrapper Div Start -->
 <div class="body-content">
 <!-- Banner start -->
 <div class="active-banner">
     <h3>Farmers First</h3>
 </div>
 <!-- Banner End -->
 <!-- Main Div Start -->
 <div class="main-farmers-top">
     <!-- Intro Para section Start-->
     <div class="content-wrapper">
         <div>
             <h4>The World Orange Festival is the perfect platform to bridge the gap between the farmer and his consumer.</h4>
             <p>With a host of B2B activities, like seminars by Agri-Experts, talks by industry leaders, summits for traders, exhibitions by corporates, the World Orange Festival will be the epicentre of everything orange trade related. Nagpur, being the hub of Orange production in India, will also attract a host of international biotechnologists, who will shed light on the use of the newest technology to aid orange production, and thereby boost exports. The farmers will also display the fruit of their labour, and will have a chance to interact with all those who impact the supply, growth, distribution, processing, packaging and transport of the orange!</p>
         </div>
     </div>
     <div class="bg-wrapper">
     </div>
 </div>
 <!-- Intro Para section End-->
 <!-- Person Details Box Start -->
 <div class="main-farm-wrapper">
     <div class="grid-main farmergrids">
         <div class="grid-box">
             <div class="grid-inner">
                 <img src="<?= base_url('uploads/farmers/Jai-Shroff.jpg'); ?>" class="img-responsive" />
                 <div class="data-box">
                     <h4 class="name">Jai-Shroff</h4>
                     <h4>CEO, UPL Limited</h4>
                     <p>“The agricultural productivity and per capita income of farmers for the development of India is necessary. For this, farmers have to create a 'value chain'. Through the World Orange Festival, we are going to achieve this objective. The use of modern technology and 'value chain' will triple the orange production.”</p>
                 </div>
             </div>
         </div>
         <div class="grid-box">
             <div class="grid-inner">
                 <img src="<?= base_url('uploads/farmers/Vijay_Darda.png'); ?>" class="img-responsive" />
                 <div class="data-box">
                     <h4 class="name">Shri Vijay Darda</h4>
                     <h4>Chairman, Lokmat Media</h4>
                     <p>“Our vision behind The World Orange Festival is to bring the worlds spotlight on the Nagpur Easy Peel Orange and firmly establish the Nagpur Orange on the culinary food map of the world. We are confident that the increase in the worldwide demand for this fruit will generate value across the chain starting from the farmers all the way through to distributors, exporters and retailers.”</p>
                 </div>
             </div>
         </div>
         <div class="grid-box">
             <div class="grid-inner">
                 <img src="<?= base_url('uploads/farmers/anant-bajaj.png'); ?>" class="img-responsive" />
                 <div class="data-box">
                     <h4 class="name">Anant Bajaj</h4>
                     <h4>Joint Managing Director, Bajaj Electricals</h4>
                     <p>“World Orange Festival is a great initiative to promote Nagpur oranges on the global map. The festival is all about cherishing, celebrating, learning and networking. Our association with the festival is an opportunity for Bajaj Electricals Limited to strengthen ties with youth while they explore this beautiful canvas of art and culture. Our strong focus is to enhance consumer experience through such festivals which provides a perfect platform for a larger brand connect.”</p>
                 </div>
             </div>
         </div>
         </div>
         <!-- <div class="grid-box2">
         <div class="grid-inner">
             <img src="<?= base_url('uploads/farmers/Jai-Shroff.jpg'); ?>" class="img-responsive" />
             <div class="data-box">
                 <h4 class="name">Jai-Shroff</h4>
                 <h4>CEO, UPL Limited</h4>
                 <p>“The agricultural productivity and per capita income of farmers for the development of India is necessary. For this, farmers have to create a 'value chain'. Through the World Orange Festival, we are going to achieve this objective. The use of modern technology and 'value chain' will triple the orange production.”</p>
             </div>
         </div>
     </div>
     <div class="grid-box2">
         <div class="grid-inner">
             <img src="<?= base_url('uploads/farmers/Vijay_Darda.png'); ?>" class="img-responsive" />
             <div class="data-box">
                 <h4 class="name">Shri Vijay Darda</h4>
                 <h4>Chairman, Lokmat Media</h4>
                 <p>“Our vision behind The World Orange Festival is to bring the worlds spotlight on the Nagpur Easy Peel Orange and firmly establish the Nagpur Orange on the culinary food map of the world. We are confident that the increase in the worldwide demand for this fruit will generate value across the chain starting from the farmers all the way through to distributors, exporters and retailers.”</p>
             </div>
         </div>
     </div>

     <div class="grid-box2">
         <div class="grid-inner">
             <img src="<?= base_url('uploads/farmers/Vijay_Darda.png'); ?>" class="img-responsive" />
             <div class="data-box">
                 <h4 class="name">Anant Bajaj</h4>
                 <h4>Joint Managing Director, Bajaj Electricals</h4>
                 <p>“World Orange Festival is a great initiative to promote Nagpur oranges on the global map. The festival is all about cherishing, celebrating, learning and networking. Our association with the festival is an opportunity for Bajaj Electricals Limited to strengthen ties with youth while they explore this beautiful canvas of art and culture. Our strong focus is to enhance consumer experience through such festivals which provides a perfect platform for a larger brand connect.”</p>
             </div>
         </div>
     </div> -->
 </div>
</div>
<div class="farmers-cross">
<img class="famcross" src="<?= base_url('uploads/cross-bg.jpg'); ?>" alt="" />
<div class="divabovefarmersevent container-fluid">
     <div class="divabovefarmerseventinside">
     <h1 class="activitymainheading">Event Schedule</h1>
 <img src="<?= base_url('uploads/activities/leaf.png'); ?>">
</div>

<form id="vint" method="get">
<select name="eventselection" onchange="change_event_date_farmers(this.value)" class="eventselection">
    <option selected="selected" value="all">Select Date</option>
    <option <?= ($eventDate==16)?'selected=selected':''; ?> value="16">16 Dec</option>
    <option <?= ($eventDate==17)?'selected=selected':''; ?> value="17">17 Dec</option>
    <option <?= ($eventDate==18)?'selected=selected':''; ?> value="18">18 Dec</option>
</select>
</form>

  </div>
</div>
  


  <div class="event-schedule-content farmereveschedle" id="event-schedule-content">
     <div class="outter-c container-fluid">
         <div class="inner-c row">
         <div id="load_data">
         <input type="hidden" name="start_val" id="start_val" value="<?= $event_limit; ?>">
         <?php 
         $CI =& get_instance();
         $CI->load->model(PUBLIC_DIR.'/eventPage','event_modal');
         if(!empty($events))
         {
         foreach($events as $event) { 
             $event_registration_count = $CI->event_modal->registerCount($event->id);
         ?>
             <div class="item col-md-4 col-sm-6">
                 <div class="item-inner">
                     <div class="liv">
                         <span><?= $event->eventName; ?></span>
                         <img src="<?= base_url('uploads/event/farmers.png'); ?>">
                     </div>
                     <ul>
                         <li>
                             <i class="fa fa-calendar" aria-hidden="true"></i> <?= $event->eventDate; ?> 2017
                         </li>
                         <li>
                             <i class="fa fa-clock-o" aria-hidden="true"></i> <?= $event->eventStartTime; ?> - <?= $event->eventEndTime; ?>
                         </li>
                         <li>
                             <i class="fa fa-map-marker" aria-hidden="true"></i> <?= $event->location; ?>
                         </li>
                         <li>
                             <i class="fa fa-map-o" aria-hidden="true"></i> <a class="getdir" href="<?= $event->direction; ?>" target="_blank">Get Direction</a>
                         </li>
                     </ul>
                         <?php 
                         if($event->id>0)
                         //if($event->eventCapacity>0 and $event_registration_count>=$event->eventCapacity)
                         //if($event->eventCapacity>0 and $event_registration_count>=$event->eventCapacity)
                         {
                             $href = "javascript:void(0);";
                             $label = 'Registration Closed';
                             /*$registrationLink = '<a href="javascript:void(0);" >Registration Closed</a>';
                             echo $registrationLink;*/
                         }
                         else{
                             $registration_url = base64_encode($event->eventName.' & '.$event->eventDate.' & '.$event->id);
                             $href = base_url('registration/'.$registration_url);
                             /*$registrationLink = '<a href="'.base_url('registration/'.$registration_url).'>register now</a>';
                             echo ' <a href="javascript:void(0);" >Registration Closed</a>';*/
                             $label = 'Register Now';
                         }
                         ?>
                        <a href="<?= $href; ?>" class="regis"><?= $label; ?></a>
                 </div>
             </div>
         <?php } } else { ?>
           <center>No Events Found</center>
                        <?php } ?>
         </div>    
</div>
             
         </div>
     </div>
     <?php if(!empty($events)) { ?>
        <div id="load_data_message"> <a href="javascript:void(0);" onclick='load_more_farmers();' id='load_more_button' class="loadmore">Load More</a></div>
     <?php }  ?>

 </div>
  
</div>