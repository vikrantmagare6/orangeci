<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>404 | World Orange Festival</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <link rel="icon" type="image/png" href="<?= base_url('uploads/favicon.png'); ?>" />
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="<?= base_url('theme/front/css/styles.min.css'); ?>">
</head>

<body>
   <a class="orregister" href="<?= base_url('event_schedule'); ?>"><span>Register</span></a>
    <div class="full-header">
        <div class="orangecontactparentdiv">
            <div class="orangecontactdiv">
                <span class="mailid">
            <a href="mailto:worldorangefestival@lokmat.com" class="orangecontactanchors"><i class="fa fa-envelope-o" aria-hidden="true"></i> worldorangefestival@lokmat.com</a>
            <div class="gamesparent"><i class="fa fa-hashtag gamesicohead" aria-hidden="true"><a href="game.html" class="gamesicospan">PLAY</a></i></div>
            </span>
            </div>
        </div>
        <nav class="navparent">
            <div class="nav-bar">
                <div class="module left">
                    <a href="<?= base_url(); ?>"><img class="logoimgsvg" src="<?= base_url('uploads/orange-logo.png'); ?>"></a>
                </div>
                <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
                    <span class="hamburger"></span>
                    <span class="hamburger"></span>
                    <span class="hamburger"></span>
                </div>
                <div class="module-group right">
                    <div class="module left oranmobmenu">
                        <ul class="menu">
                            <li class=""> <a href="<?= base_url('farmers-first'); ?>">Farmers First</a>
                            </li>
                            <li class="has-dropdown"> <a href="<?= base_url('activities'); ?>">Activities</a>
                                <ul>
                                    <li class=""> <a href="<?= base_url('activities/?activities=art-and-culture'); ?>" target="">Art & Culture</a> </li>
                                    <li class=""> <a href="<?= base_url('activities/?activities=food'); ?>" target="">Food</a> </li>
                                    <li class=""> <a href="<?= base_url('activities/?activities=entertainement'); ?>" target="">Entertainment</a> </li>
                                    <li class=""> <a href="<?= base_url('activities/?activities=games'); ?>" target="">Games</a> </li>
                                    <li class=""> <a href="<?= base_url('activities/?activities=others'); ?>" target="">Others</a> </li>
                                </ul>
                            </li>
                            <li class=""> <a href="<?= base_url('event_schedule'); ?>"> Event Schedule </a> </li>
                            <li class=""> <a href="<?= base_url('venue'); ?>"> Venue </a></li>
                            <li class=""> <a href="<?= base_url('partners'); ?>"> Partners </a></li>
                            <li class=""> <a href="<?= base_url('gallery'); ?>"> Gallery </a></li>
                            <li class="gamesli"> <a href="<?= base_url('game'); ?>"> Games </a></li>        
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <div class="pagetoppad">
    </div>
    <!-- Main Wrapper Div Start -->
    <div class="body-content">
        <!-- Main Div Start -->
        <div class="viewportdiv block-404">
        <div class="outer-valign">
            <div class="inner-valign">
                <div class="container">
                    <div class="box-txt">
                    <img src="<?= base_url('uploads/404-error-animation.gif'); ?>" class="img-responsive" />
                    <p>Sorry, the page you are looking for could not be found.</p>
                    <div class="button-container">
                        <a href="<?= base_url(); ?>" class="button-class">Back to Homepage</a>
                    </div>
                </div>
                </div>
            </div>
        </div>
        </div>
        <!-- Main Div End -->
    </div>
    <!-- Main Wrapper Div End -->
    <footer class="orangesuperfooter">
        <div class="oranfooter clearfix">
            <div class="oranfooter1"><a  target="_blank" href="https://www.facebook.com/WorldOrangeFestival"><i class="oransocialfoot fa fa-facebook" aria-hidden="true"></i></a><a  target="_blank" href="https://twitter.com/WorldOrangeFest"><i class="oransocialfoot fa fa-twitter" aria-hidden="true"></i></a><a  target="_blank" href="https://www.instagram.com/worldorangefestival/"><i class="oransocialfoot fa fa-instagram" aria-hidden="true"></i></a></div>
            <div class="oranfooter2">@<?= date('Y'); ?> <?= SITE_DISPNAME; ?>. ALL RIGHTS RESERVED. CRAFTED BY <a href="http://www.togglehead.in" target="_blank" class="togglelinks">TOGGLEHEAD</a></div>
        </div>
    </footer>
     <script src="<?= base_url('theme/front/js/main.min.js'); ?>"></script>
    <script src="<?= base_url('theme/front/js/lib/lightgallery.min.js'); ?>"></script>
    <script src="<?= base_url('theme/front/js/lib/lg-video.min.js'); ?>"></script>
</body>

</html>