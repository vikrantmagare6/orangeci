-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2017 at 02:35 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `orangefest`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `base64_value` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `avatar` longtext NOT NULL,
  `status` enum('1','2','3') NOT NULL DEFAULT '1',
  `createdDate` varchar(32) NOT NULL,
  `updatedDate` varchar(32) NOT NULL,
  `ipAddress` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `fname`, `lname`, `uname`, `email`, `base64_value`, `password`, `avatar`, `status`, `createdDate`, `updatedDate`, `ipAddress`) VALUES
(1, 'Aliasgar', 'Vanak', 'admin', 'aliasgar@togglehead.in', 'YWJjMTIz', 'e99a18c428cb38d5f260853678922e03', 'avatar.jpg', '1', '1466240610', '1512043602', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `eventregistration`
--

CREATE TABLE `eventregistration` (
  `id` int(100) NOT NULL,
  `commonRegister` varchar(255) NOT NULL,
  `fullName` varchar(255) NOT NULL,
  `mobileNumber` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `eventId` int(10) NOT NULL,
  `registerDate` varchar(32) NOT NULL,
  `status` enum('Pending','Success','Exceeded') NOT NULL DEFAULT 'Pending',
  `updatedDate` varchar(32) NOT NULL,
  `ipAddress` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `eventCategory` varchar(100) NOT NULL,
  `eventName` varchar(255) NOT NULL,
  `eventDate` varchar(255) NOT NULL,
  `eventStartTime` varchar(255) NOT NULL,
  `eventEndTime` varchar(255) NOT NULL,
  `location` text NOT NULL,
  `direction` text NOT NULL,
  `appearance` text NOT NULL,
  `eventCapacity` int(10) NOT NULL DEFAULT '0',
  `status` enum('Active','Cancelled','Completed') NOT NULL DEFAULT 'Active',
  `createdDate` varchar(32) NOT NULL,
  `updatedDate` varchar(32) NOT NULL,
  `ipAddress` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `eventCategory`, `eventName`, `eventDate`, `eventStartTime`, `eventEndTime`, `location`, `direction`, `appearance`, `eventCapacity`, `status`, `createdDate`, `updatedDate`, `ipAddress`) VALUES
(2, 'Games', 'Jayshree Marraige', '17th December', '06:30 PM', '08:30 PM', 'Taj Hotel', 'Go to hell', 'VVS Laxman', 800, 'Active', '1512040297', '1512050636', '::1'),
(3, 'Art & Culture', 'Test Event', '17th December', '09:30 PM', '11:30 PM', 'Nagpara', 'Go to hell', 'Aliasgar Vanak', 1000, 'Active', '1512049517', '1512049517', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `id` int(100) NOT NULL,
  `userType` enum('Customer','Vendor','Admin') NOT NULL,
  `uid` int(11) NOT NULL,
  `loginDateTime` varchar(32) NOT NULL,
  `logoutDateTime` varchar(32) NOT NULL,
  `ipAddress` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`id`, `userType`, `uid`, `loginDateTime`, `logoutDateTime`, `ipAddress`) VALUES
(1, 'Admin', 1, '1512026569', '', '::1'),
(2, 'Admin', 1, '1512027020', '', '::1'),
(3, 'Admin', 1, '1512027215', '', '::1'),
(4, 'Admin', 1, '1512027771', '', '::1'),
(5, 'Admin', 1, '1512027809', '', '::1'),
(6, 'Admin', 1, '1512027833', '', '::1'),
(7, 'Admin', 1, '1512031568', '', '::1'),
(8, 'Admin', 1, '1512031678', '', '::1'),
(9, 'Admin', 1, '1512031782', '1512032682', '::1'),
(10, 'Admin', 1, '1512032696', '', '::1'),
(11, 'Admin', 1, '1512106399', '', '::1'),
(12, 'Admin', 1, '1512118857', '', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(1) NOT NULL,
  `logo` text NOT NULL,
  `favicon` text NOT NULL,
  `sitename` varchar(255) NOT NULL,
  `slogan` varchar(255) NOT NULL,
  `facebook_profile` text NOT NULL,
  `google_profile` text NOT NULL,
  `youtube_profile` text NOT NULL,
  `twitter_profile` text NOT NULL,
  `linkedin_profile` text NOT NULL,
  `contact_email` text NOT NULL,
  `no_reply_email` text NOT NULL,
  `paypal_email` text NOT NULL,
  `theme` varchar(255) NOT NULL,
  `base_address` text NOT NULL,
  `base_country` varchar(100) NOT NULL,
  `base_state` varchar(100) NOT NULL,
  `base_city` varchar(100) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `facebook_app_id` varchar(255) NOT NULL,
  `facebook_secret_key` varchar(255) NOT NULL,
  `google_app_id` varchar(255) NOT NULL,
  `google_secret_key` varchar(255) NOT NULL,
  `contact_address` text,
  `contact_email_address` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(255) NOT NULL,
  `contact_longitude` varchar(255) NOT NULL,
  `contact_latitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `logo`, `favicon`, `sitename`, `slogan`, `facebook_profile`, `google_profile`, `youtube_profile`, `twitter_profile`, `linkedin_profile`, `contact_email`, `no_reply_email`, `paypal_email`, `theme`, `base_address`, `base_country`, `base_state`, `base_city`, `currency`, `facebook_app_id`, `facebook_secret_key`, `google_app_id`, `google_secret_key`, `contact_address`, `contact_email_address`, `contact_phone`, `contact_longitude`, `contact_latitude`) VALUES
(1, 'IMG-20150307-WA0044.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Bedipara, Girnari Street, Nr. Patel Vadi, Bhavnagar Road, Rajkot (Gujarat)', 'hakimitin4@gmail.com', ' (0281)6999967 , (0281)2236052', '70.812343', '22.302768');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventregistration`
--
ALTER TABLE `eventregistration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `eventregistration`
--
ALTER TABLE `eventregistration`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
