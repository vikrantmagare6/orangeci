<!DOCTYPE html>
<html lang="en-US">
    <head>
        <title>Bootstrap Star Rating</title>
        <link href="bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="http://localhost/spaceskout/lib/theme/assets/css/font-awesome.css" media="screen" />

<link href="star-rating.min.css" rel="stylesheet">
<link href="select2.min.css" rel="stylesheet">
<link href="select2-addl.min.css" rel="stylesheet">
<link href="select2-krajee.min.css" rel="stylesheet">
</head>
    <body>
                    <div id="kv-gs-results-cont" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><i class="glyphicon glyphicon-search"></i> Search Results <small>Krajee jQuery Plugins</small></h4>
            </div>
            <div class="modal-body">
                <div id="kv-gs-results"><div class="kv-loading-big"></div></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->        <div class="kv-container">
			           
<div class="row">
    
    <div class="col-sm-9">
        <div class="demos">
<div class="bs-section">
    	<div class="page-header bs-header">
		<h1 id="advanced-example-1" class="text-warning">
            <a class="kv-anchor" title="Permalink" href="#advanced-example-1" data-toggle="tooltip"><span class="glyphicon glyphicon-link"></span></a>
                                
            
		</h1>
	</div>
    								<div class="bs-example">
				<label for="input-1" class="control-label">Rate This</label>
<input id="input-1" value="4.3" class="rating-loading">			</div>
			
			</div>        </div>
    </div>
</div>
        </div>
        
        
             <!--[if (gt IE 8)]><!-->
    
    <!--<![endif]-->
<script src="all-krajee.js"></script>
<script src="star-rating.min.js"></script>
<script src="select2.full.min.js"></script>
<script src="select2-krajee.min.js"></script>
<script src="star-rating_locale_de.js"></script>
<script type="text/javascript">jQuery(document).ready(function () {
!function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    p = /^http:/.test(d.location) ? 'http' : 'https';
    if (!d.getElementById(id)) {
        js = d.createElement(s);
        js.id = id;
        js.src = p + '://platform.twitter.com/widgets.js';
        fjs.parentNode.insertBefore(js, fjs);

    }
}
(document, "script", "twitter-wjs");
$('#input-1').rating({min: 0, max: 8, step: 0.1, stars: 8});
});</script>        
</body>
</html>
